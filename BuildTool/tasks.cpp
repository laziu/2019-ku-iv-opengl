#include <cstdlib>
#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

namespace fs = std::filesystem;

namespace gen_resource {
using isb_iterator = std::istreambuf_iterator<char>;
using osb_iterator = std::ostreambuf_iterator<char>;

std::string replace(std::string tmpl, const std::regex& re, const std::string& text)
{
    auto oss = std::ostringstream();
    std::regex_replace(osb_iterator(oss), tmpl.begin(), tmpl.end(), re, text);
    return oss.str();
}

std::string template_glsl(std::string tmpl, const fs::path& path)
{
    if (path.extension() != L".glsl") {
        std::wcerr << path.wstring() << L" is not glsl. skipped" << std::endl;
        return "";
    }

    auto ifs = std::ifstream(path);
    if (!ifs || !ifs.is_open()) {
        std::wcerr << path.wstring() << L" cannot be opened." << std::endl;
        return "";
    }
    std::wcout << path.wstring() << L" -> R::glsl" << std::endl;

    auto name = path.stem().string();
    std::replace(name.begin(), name.end(), '.', '_');

    auto content = std::string(isb_iterator(ifs), isb_iterator());

    tmpl = replace(tmpl, std::regex("_GLSL_NAME_"), name);
    tmpl = replace(tmpl, std::regex("_GLSL_CONTENT_"), content);

    return tmpl;
}

std::string template_binary(std::string tmpl, const fs::path& path)
{
    auto ifs = std::ifstream(path, std::ios_base::binary);
    if (!ifs || !ifs.is_open()) {
        std::wcerr << path.wstring() << L" cannot be opened." << std::endl;
        return "";
    }
    std::wcout << path.wstring() << L" -> R::binary" << std::endl;

    auto name = path.filename().string();
    std::replace(name.begin(), name.end(), '.', '_');

    auto data    = std::string(isb_iterator(ifs), isb_iterator());
    auto content = std::ostringstream();

    int i = 0;
    for (int byte : data) {
        content << ((i % 0x20) ? "" : "\n")
                << "0x" << std::uppercase << std::setfill('0') << std::setw(2) << std::hex << byte << ',';
        i = (i + 1) % 0x20;
    }

    tmpl = replace(tmpl, std::regex("_BINARY_NAME_"), name);
    tmpl = replace(tmpl, std::regex("_BINARY_CONTENT_"), content.str());

    return tmpl;
}

std::string template_text(std::string tmpl, const fs::path& path)
{
    auto ifs = std::ifstream(path);
    if (!ifs || !ifs.is_open()) {
        std::wcerr << path.wstring() << L" cannot be opened." << std::endl;
        return "";
    }
    std::wcout << path.wstring() << L" -> R::text" << std::endl;

    auto name = path.filename().string();
    std::replace(name.begin(), name.end(), '.', '_');

    auto content = std::string(isb_iterator(ifs), isb_iterator());

    tmpl = replace(tmpl, std::regex("_TEXT_NAME_"), name);
    tmpl = replace(tmpl, std::regex("_TEXT_CONTENT_"), content);

    return tmpl;
}

std::string template_image(std::string tmpl, const fs::path& path)
{
    int  w, h, channel;
    auto image = stbi_load(path.string().c_str(), &w, &h, &channel, 0);
    if (!image) {
        std::wcerr << path.wstring() << L" cannot be opened." << std::endl;
        return "";
    }
    std::wcout << path.wstring() << L" -> R::image" << std::endl;

    auto name = path.filename().string();
    std::replace(name.begin(), name.end(), '.', '_');

    auto content = std::ostringstream();
    content << '\n'
            << "        constexpr int width   = " << w << ";\n"
            << "        constexpr int height  = " << h << ";\n"
            << "        constexpr int channel = " << channel << ";\n"
            << "        constexpr unsigned char data[] = {";
    for (int i = 0, iE = w * h * channel; i < iE; ++i) {
        content << ((i % channel) ? "" : " ") << (i % (0x10 * channel) ? "" : "\n")
                << "0x" << std::uppercase << std::setfill('0') << std::setw(2) << std::hex << static_cast<int>(image[i]) << ',';
    }
    content << "\n        };\n   ";

    stbi_image_free(image);

    tmpl = replace(tmpl, std::regex("_IMAGE_NAME_"), name);
    tmpl = replace(tmpl, std::regex("_IMAGE_CONTENT_"), content.str());

    return tmpl;
}

void task(const fs::path& this_path, const fs::path& project_dir)
{
    std::wcout << L"Generate R.h..." << std::endl;

    auto of_path = project_dir / L"R.h";
    auto ofs     = std::ofstream(of_path, std::ios::trunc);
    if (!ofs || !ofs.is_open()) {
        std::wcerr << L"Target " << of_path.wstring() << L" cannot be opened." << std::endl;
        exit(EXIT_FAILURE);
    }

    auto if_path = this_path.parent_path().parent_path() / L"R.template.h";
    auto ifs     = std::ifstream(if_path);
    if (!ifs || !ifs.is_open()) {
        std::wcerr << L"Source " << if_path.wstring() << L" cannot be opened." << std::endl;
        exit(EXIT_FAILURE);
    }

    auto re_glsl   = std::regex("^.*_GLSL_NAME_.*$");
    auto re_binary = std::regex("^.*_BINARY_NAME_.*$");
    auto re_text   = std::regex("^.*_TEXT_NAME_.*$");
    auto re_image  = std::regex("^.*_IMAGE_NAME_.*$");

    static auto write_template = [&](std::string line, const fs::path& path, auto template_func) {
        if (!fs::is_directory(path))
            std::wcerr << L"Folder " << path.wstring() << L" cannot be opened."
                       << (fs::exists(path) ? L"" : L" Does it exists?") << std::endl;
        else
            for (const auto& entry : fs::directory_iterator(path))
                if (fs::is_regular_file(entry))
                    ofs << template_func(line, entry) << "\n\n";
    };

    for (std::string line; std::getline(ifs, line);) {
        if (std::regex_match(line, re_glsl))
            write_template(line, project_dir / L"shaders", template_glsl); // shaders template
        else if (std::regex_match(line, re_binary))
            write_template(line, project_dir / L"embed" / L"binary", template_binary); // embedded base64 mime template
        else if (std::regex_match(line, re_text))
            write_template(line, project_dir / L"embed" / L"text", template_text); // embedded text type template
        else if (std::regex_match(line, re_image))
            write_template(line, project_dir / L"embed" / L"image", template_image); // embedded image type template
        else
            ofs << line << std::endl; // plain code
    }
}
}

int wmain(int argc, wchar_t* argv[])
{
    const auto task_name = std::wstring(argv[1]);
    if (task_name == L"gen-resource") {
        const auto this_path   = std::wstring(argv[0]);
        const auto project_dir = std::wstring(argv[2]);
        gen_resource::task(this_path, project_dir);
    }
    else {
        std::wcerr << L"[" << task_name << L"]: Undefined task name. Do nothing" << std::endl;
        exit(EXIT_FAILURE);
    }
    return 0;
}
