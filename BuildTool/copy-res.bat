%echo off
set project_dir=%~1%
set solution_dir=%~2%
set configuration=%~3%
set platform=%~4%

set dst=Bin\%configuration%\%platform%

echo %project_dir%resources -^> %solution_dir%%dst%\resources

if not exist "%solution_dir%%dst%" mkdir "%solution_dir%%dst%"
if not exist "%solution_dir%%dst%\resources" mkdir "%solution_dir%%dst%\resources"
xcopy /s /e /c /y /i "%project_dir%resources" "%solution_dir%%dst%\resources"
if errorlevel 1 ( echo xcopy failed. ; exit /b 1 )
