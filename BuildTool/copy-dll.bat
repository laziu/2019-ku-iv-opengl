%echo off
set solution_dir=%~1%
set configuration=%~2%
set platform=%~3%

set src=Dependencies\bin\%platform%
set dst=Bin\%configuration%\%platform%

echo %src%\*.dll -^> %solution_dir%%dst%

if not exist "%solution_dir%%dst%" mkdir "%solution_dir%%dst%"
xcopy /e /c /y /i "%solution_dir%%src%\*.dll" "%solution_dir%%dst%"
if errorlevel 1 ( echo xcopy failed. ; exit /b 1 )
