// auto-generated R.h by build events; do not edit manually.
// clang-format off
#pragma once
namespace R {
namespace shader {
    constexpr const char* t_vert = "#define VERTEX_SHADER 1\n";
    constexpr const char* t_geom = "#define GEOMETRY_SHADER 1\n";
    constexpr const char* t_frag = "#define FRAGMENT_SHADER 1\n";

    constexpr const char* _GLSL_NAME_ = R"rawliteral(_GLSL_CONTENT_)rawliteral";
}
namespace image {
    namespace _IMAGE_NAME_ { _IMAGE_CONTENT_ }
}
namespace binary {
    constexpr unsigned char _BINARY_NAME_[] = { _BINARY_CONTENT_ };
}
namespace text {
    constexpr const char* _TEXT_NAME_ = R"rawliteral(_TEXT_CONTENT_)rawliteral";
}
}
// clang-format on
