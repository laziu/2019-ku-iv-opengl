#include "simulator.hpp"
#include "R.h"

double Simulator::piston_t    = 0;
bool   Simulator::piston_move = false;

namespace b_index {
enum { position = 0,
       velocity,
       force,
       density,
       flags,
       hash,
       spring_force };
}

constexpr size_t bp_size[] = {
    Simulator::C.particle_num * sizeof(glm::vec4), // position
    Simulator::C.particle_num * sizeof(glm::vec4), // velocity
    Simulator::C.particle_num * sizeof(glm::vec4), // force
    Simulator::C.particle_num * sizeof(GLfloat),   // density
    Simulator::C.particle_num * sizeof(GLuint),    // flags
};
constexpr size_t bhash_size = // hashtable grid
    sizeof(GLint) * (size_t(Simulator::C.particle_num) +
                     size_t(Simulator::C.cell_num.x) * size_t(Simulator::C.cell_num.y) * size_t(Simulator::C.cell_num.z));
constexpr size_t bspr_size = Simulator::cube_particle_num * sizeof(glm::vec4); // spring force

constexpr auto bp_row       = std::size(bp_size);
constexpr auto bp_offset    = ([]() constexpr {
    std::array<size_t, bp_row + 1> offset = { 0 };
    for (size_t i = 0; i < bp_row; ++i)
        offset[i + 1] = offset[i] + bp_size[i];
    return offset;
})();
constexpr auto bp_totalsize = bp_offset[bp_row];

Simulator::Simulator(): sph(C.particle_num), compute_spring_worker(*this)
{
    // create compute programs
    std::vector<glo::shader::sptr_t> shaders = {
        glo::shader::create(GL_COMPUTE_SHADER, { R::shader::simulation_header, R::shader::simulation_makeHashTable }),
        glo::shader::create(GL_COMPUTE_SHADER, { R::shader::simulation_header, R::shader::simulation_integrate }),
    };

    for (auto& shader : shaders)
        p_compute.push_back(glo::program::create({ shader }));

    // create buffers
    (b_particle = glo::buffer::create())
        ->bind(GL_SHADER_STORAGE_BUFFER)
        .bufferData(bp_totalsize, nullptr, GL_STATIC_DRAW);
    for (int i = 0; i < bp_row; ++i)
        b_particle->bindRange(GL_SHADER_STORAGE_BUFFER, i, bp_offset[i], bp_size[i]);

    (b_hashtable = glo::buffer::create())
        ->bind(GL_SHADER_STORAGE_BUFFER)
        .bufferData(bhash_size, nullptr, GL_STATIC_DRAW);
    b_hashtable->bindBase(GL_SHADER_STORAGE_BUFFER, b_index::hash);

    (b_spring_force = glo::buffer::create())
        ->bind(GL_SHADER_STORAGE_BUFFER)
        .bufferData(bspr_size, nullptr, GL_STATIC_DRAW);
    b_spring_force->bindBase(GL_SHADER_STORAGE_BUFFER, b_index::spring_force);

    (b_constant = glo::buffer::create())
        ->bind(GL_UNIFORM_BUFFER)
        .bufferData(sizeof(C), &C, GL_STATIC_DRAW);
    b_constant->bindBase(GL_UNIFORM_BUFFER, 0);

    (b_global = glo::buffer::create())
        ->bind(GL_UNIFORM_BUFFER)
        .bufferData(sizeof(global), &global, GL_STATIC_DRAW);
    b_global->bindBase(GL_UNIFORM_BUFFER, 1);

    // create VAOs
    (vao_water = glo::vertex_array::create())->bind();
    vao_water->hold(b_particle)
        ->bind(GL_ARRAY_BUFFER)
        .vertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void*>(bp_offset[b_index::position]))
        .vertexAttribIPointer(1, 1, GL_UNSIGNED_INT, 0, reinterpret_cast<void*>(bp_offset[b_index::flags]));

    (vao_cube = glo::vertex_array::create())->bind();
    vao_cube->hold(b_cube_pos = glo::buffer::create())
        ->bind(GL_ARRAY_BUFFER)
        .bufferData(14 * sizeof(glm::vec4), nullptr, GL_STATIC_DRAW)
        .vertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, nullptr);

    initialize();
    integrate(); // avoid weird rendering
}

void Simulator::initialize()
{
    b_particle->mapNamedBuffer<GLbyte>(GL_WRITE_ONLY, [&](GLbyte* buffer) {
        auto position = reinterpret_cast<glm::vec4*>(buffer + bp_offset[b_index::position]);
        auto velocity = reinterpret_cast<glm::vec4*>(buffer + bp_offset[b_index::velocity]);
        auto force    = reinterpret_cast<glm::vec4*>(buffer + bp_offset[b_index::force]);
        auto density  = reinterpret_cast<GLfloat*>(buffer + bp_offset[b_index::density]);
        auto flags    = reinterpret_cast<GLuint*>(buffer + bp_offset[b_index::flags]);

        // set cube particles
        for (int i = 0; i < cube_particle_num;)
            for (int x = 0; x < cube_num.x; ++x)
                for (int y = 0; y < cube_num.y; ++y)
                    for (int z = 0; z < cube_num.z; ++z) {
                        position[i] = glm::vec4(glm::vec3(10.f, 1.f, 0.f) + spring_length * glm::vec3(x, y, z), 1.f);
                        velocity[i] = glm::vec4(0, 0, 0, 0);
                        density[i]  = 1.f;
                        flags[i]    = C.f_t_object;
                        ++i;
                    }

        // set water particles
        for (GLuint i = cube_particle_num; i < C.particle_num;)
            for (int x = -4; x <= 4 && i < C.particle_num; ++x)
                for (int y = -4; y <= 4 && i < C.particle_num; ++y)
                    if (std::abs(x) + std::abs(y) < 7) {
                        position[i] = glm::vec4(x * 0.5f, 20.f + y * 0.5f, C.limit_min.z, 1.f);
                        velocity[i] = glm::vec4(0, -1.f, 4.f, 0);
                        flags[i]    = C.f_t_water | C.f_disabled;
                        ++i;
                    }

        // set mass spring
        static auto cpos = [&](GLuint x, GLuint y, GLuint z) { return (x * cube_num.y + y) * cube_num.z + z; };

        for (auto d : std::vector<glm::ivec3> { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } })
            for (int x = d.x; x < cube_num.x; ++x)
                for (int y = d.y; y < cube_num.y; ++y)
                    for (int z = d.z; z < cube_num.z; ++z) {
                        glm::ivec2 sp = { cpos(x, y, z), cpos(x - d.x, y - d.y, z - d.z) };
                        mss.join.push_back(sp);
                        mss.spring_length.push_back(glm::length(position[sp.x] - position[sp.y]));
                        mss.spring_coef.push_back(2500.f);
                    }

        for (auto d : std::vector<glm::ivec3> { { 1, 1, 0 }, { 1, 0, 1 }, { 0, 1, 1 } })
            for (int x = d.x; x < cube_num.x; ++x)
                for (int y = d.y; y < cube_num.y; ++y)
                    for (int z = d.z; z < cube_num.z; ++z) {
                        glm::ivec2 sps[2] = {
                            { cpos(x, y, z), cpos(x - d.x, y - d.y, z - d.z) },
                            { cpos(x - d.x * d.y, y - d.y * d.z, z - d.z * d.x),
                              cpos(x - d.x * d.z, y - d.y * d.x, z - d.z * d.y) }
                        };
                        for (auto& sp : sps) {
                            mss.join.push_back(sp);
                            mss.spring_length.push_back(glm::length(position[sp.x] - position[sp.y]));
                            mss.spring_coef.push_back(200.f);
                        }
                    }

        for (int x = 1; x < cube_num.x; ++x)
            for (int y = 1; y < cube_num.y; ++y)
                for (int z = 1; z < cube_num.z; ++z) {
                    glm::ivec2 sps[4] = {
                        { cpos(x, y, z), cpos(x - 1, y - 1, z - 1) },
                        { cpos(x - 1, y, z), cpos(x, y - 1, z - 1) },
                        { cpos(x, y - 1, z), cpos(x - 1, y, z - 1) },
                        { cpos(x, y, z - 1), cpos(x - 1, y - 1, z) },
                    };
                    for (auto& sp : sps) {
                        mss.join.push_back(sp);
                        mss.spring_length.push_back(glm::length(position[sp.x] - position[sp.y]));
                        mss.spring_coef.push_back(200.f);
                    }
                }

        // set cube vao index (triangle_strip)
        auto f        = cube_num - glm::ivec3(1, 1, 1);
        i_cube[0b000] = cpos(0, 0, 0);
        i_cube[0b001] = cpos(0, 0, f.z);
        i_cube[0b010] = cpos(0, f.y, 0);
        i_cube[0b011] = cpos(0, f.y, f.z);
        i_cube[0b100] = cpos(f.x, 0, 0);
        i_cube[0b101] = cpos(f.x, 0, f.z);
        i_cube[0b110] = cpos(f.x, f.y, 0);
        i_cube[0b111] = cpos(f.x, f.y, f.z);
    });

    updateCube();
}

void Simulator::update(double dt)
{
    // waterfall animation
    waterfall_t += global.time_step;
    if (waterfall_t > 0.01 && freeze_i < C.particle_num) {
        waterfall_t = 0;
        b_particle->bind(GL_SHADER_STORAGE_BUFFER)
            .mapBufferRange<GLuint>(bp_offset[b_index::flags], bp_size[b_index::flags], GL_MAP_WRITE_BIT, [&](GLuint* flags) {
                for (int i = 0; i < 69 && freeze_i < C.particle_num; ++i)
                    flags[freeze_i++] ^= C.f_disabled;
            });
    }

    // piston animation
    if (piston_move)
        piston_t += global.time_step * 0.75;
    float piston_npos = 20.f * (1.f - glm::abs(glm::sin(float(piston_t))));
    global.piston_vel = (piston_npos - global.piston_pos) / global.time_step;
    global.piston_pos = piston_npos;

    // physics calculation
    updateGlobal();
    for (int iteration = 0; iteration < iteration_n; ++iteration) {
        compute_spring_worker.start();
        makeHashTable();
        sph.computeDensity();
        sph.computeForce();
        compute_spring_worker.wait();
        integrate();
    }

    updateCube();
}

void Simulator::updateCube() // update vao buffer
{
    b_cube_pos->mapNamedBuffer<glm::vec4>(GL_WRITE_ONLY, [&](glm::vec4* b_pos) {
        b_particle->mapNamedBuffer<glm::vec4>(GL_READ_ONLY, [&](glm::vec4* position) {
            std::array<glm::vec4, 8> pos;
            for (size_t i = 0; i < 8; ++i)
                pos[i] = position[i_cube[i]];
            auto diff_x = glm::normalize(pos[0] + pos[1] + pos[2] + pos[3] - pos[4] - pos[5] - pos[6] - pos[7]) * (C.h / 2.f);
            auto diff_y = glm::normalize(pos[0] + pos[1] - pos[2] - pos[3] + pos[4] + pos[5] - pos[6] - pos[7]) * (C.h / 2.f);
            auto diff_z = glm::normalize(pos[0] - pos[1] + pos[2] - pos[3] + pos[4] - pos[5] + pos[6] - pos[7]) * (C.h / 2.f);
            pos[0] += diff_x + diff_y + diff_z;
            pos[1] += diff_x + diff_y - diff_z;
            pos[2] += diff_x - diff_y + diff_z;
            pos[3] += diff_x - diff_y - diff_z;
            pos[4] += -diff_x + diff_y + diff_z;
            pos[5] += -diff_x + diff_y - diff_z;
            pos[6] += -diff_x - diff_y + diff_z;
            pos[7] += -diff_x - diff_y - diff_z;

            static constexpr int i_seq[] = { 0b000, 0b001, 0b010, 0b011, 0b111, 0b001, 0b101,
                                             0b000, 0b100, 0b010, 0b110, 0b111, 0b100, 0b101 };
            for (size_t i = 0; i < 14; ++i)
                b_pos[i] = pos[i_seq[i]];
        });
    });
}

void Simulator::drawCube()
{
    vao_cube->bind().drawArrays(GL_TRIANGLE_STRIP, 0, 14);
}

void Simulator::drawWater()
{
    vao_water->bind().drawArrays(GL_POINTS, cube_particle_num, C.particle_num);
}

void Simulator::ComputeSpringThread::onStart()
{
    force    = static_cast<glm::vec4*>(glMapNamedBuffer(simulator.b_spring_force->id, GL_READ_WRITE));
    auto buf = static_cast<GLbyte*>(glMapNamedBuffer(simulator.b_particle->id, GL_READ_ONLY));
    position = reinterpret_cast<glm::vec4*>(buf + bp_offset[b_index::position]);
    velocity = reinterpret_cast<glm::vec4*>(buf + bp_offset[b_index::velocity]);
    flags    = reinterpret_cast<GLuint*>(buf + bp_offset[b_index::flags]);
}

void Simulator::ComputeSpringThread::onWork()
{
    memset(force, 0, bspr_size);
    simulator.mss.computeForce(position, velocity, force, flags);
}

void Simulator::ComputeSpringThread::onFinish()
{
    glUnmapNamedBuffer(simulator.b_particle->id);
    glUnmapNamedBuffer(simulator.b_spring_force->id);
}

void Simulator::updateGlobal()
{
    b_global->bind(GL_UNIFORM_BUFFER).bufferData(sizeof(global), &global, GL_STATIC_DRAW); // update uniform value
}

void Simulator::makeHashTable()
{
    b_hashtable->bind(GL_SHADER_STORAGE_BUFFER).mapBuffer<GLbyte>(GL_WRITE_ONLY, [&](GLbyte* buffer) {
        memset(buffer, 0xFF, bhash_size); // reset grid table and link
    });
    p_compute[0]->use();
    glDispatchCompute(C.particle_num / 128, 1, 1);
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}

void Simulator::integrate()
{
    p_compute[1]->use();
    glDispatchCompute(C.particle_num / 128, 1, 1);
    glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}
