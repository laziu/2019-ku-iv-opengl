#pragma once
#include <omp.h>
#include <glm/glm.hpp>

class MassSpringSimulation {
public:
    std::vector<glm::ivec2> join;
    std::vector<GLfloat>    spring_length;
    std::vector<GLfloat>    spring_coef;

    void computeForce(glm::vec4* position, glm::vec4* velocity, glm::vec4* force, GLuint* flags)
    {
#pragma omp parallel for shared(position, velocity, force, flags)
        for (int s = 0; s < join.size(); ++s) {
            int i = join[s].x;
            int j = join[s].y;

            auto  dir = position[i] - position[j];
            float len = glm::length(dir);

            float spring_force  = spring_coef[s] * (len - spring_length[s]);
            float damping_force = .5f * glm::dot(velocity[i] - velocity[j], dir);
            auto  force_sum     = (spring_force + damping_force) * dir;

#pragma omp atomic
            force[i].x -= force_sum.x;
#pragma omp atomic
            force[i].y -= force_sum.y;
#pragma omp atomic
            force[i].z -= force_sum.z;
#pragma omp atomic
            force[i].w -= force_sum.w;
#pragma omp atomic
            force[j].x += force_sum.x;
#pragma omp atomic
            force[j].y += force_sum.y;
#pragma omp atomic
            force[j].z += force_sum.z;
#pragma omp atomic
            force[j].w += force_sum.w;
        }
    }
};
