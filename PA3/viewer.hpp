#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>

namespace viewer {
constexpr auto time_step = 1. / 60.;

void init();
void update(double dt);
void render();
void resize(GLFWwindow*, int w, int h);
void mousepress(GLFWwindow*, int button, int action, int mods);
void mousemove(GLFWwindow*, double x, double y);
void mousescroll(GLFWwindow*, double x_offset, double y_offset);
void keypress(GLFWwindow*, int key, int scancode, int action, int mods);
};
