#include "viewer.hpp"
#include "viewer.water.hpp"

#include "R.h"
#include "glo/framebuffer.hpp"
#include "glo/program.hpp"
#include "glo/texture.hpp"
#include "simulator.hpp"
#include "util/image.hpp"
#include <cmath>
#include <algorithm>
#include <memory>
#include <glm/glm.hpp>
#include <glm/trigonometric.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace viewer {
std::unique_ptr<Simulator> simulator;

struct Camera {
    glm::dvec3 focus  = glm::dvec3(0, 0, 0);
    glm::dvec3 r_pos  = glm::dvec3(0, 30., 90.);
    double     radius = 40.;
    double     theta  = glm::radians(10.); // longitude (+z = 0 <= theta < 2PI)
    double     phi    = glm::radians(60.); // latitude  (+y = 0 <= phi <= PI = -y)

    glm::vec4 eye() const { return glm::vec4(focus + r_pos, 1.f); }
    glm::mat4 mat(); // calculate view matrix from camera info
} camera;

glm::dvec2 mouse_coord    = glm::dvec2(0, 0);
bool       mouse_state[3] = {};
bool       pause = true, one_frame = true;

glo::buffer::sptr_t b_draw_uniform;

struct DrawUniform {
    alignas(16) glm::mat4 p_mat        = {};
    alignas(16) glm::mat4 v_mat        = {};
    alignas(16) glm::mat4 pv_mat       = {};
    alignas(16) glm::mat4 lpv_mat      = {};
    alignas(16) glm::vec4 eye          = {};
    alignas(16) glm::vec4 dir_light[1] = {
        { glm::normalize(glm::vec3(1.f, 1.f, 2.f)), 0.f }
    };
    alignas(16) glm::vec4 point_light[1] = {
        { -17.f, 30.f, -7.f, 1.f }
    };
    alignas(8) glm::ivec2 w_size     = {};
    alignas(4) float particle_radius = .5f;
    alignas(4) float piston_pos      = 20.f;
} u_data;

glm::mat4 p_mat, v_mat, pv_mat;
glm::mat4 lp_mat = glm::ortho(-40.f, 40.f, -40.f, 40.f, 1.f, 500.f),
          lv_mat = glm::lookAt(glm::vec3(100.f, 100.f, 200.f), glm::vec3(-10, 0, 0), glm::vec3(0, 1, 0));

glo::texture::sptr_t     tex_screen, tex_screen_depth, tex_sky, tex_floor, tex_chain, tex_cube, tex_shadow, tex_shadow_depth;
glo::framebuffer::sptr_t fb_screen, fb_shadow;
glo::program::sptr_t     p_screen, p_sky, p_floor, p_cube, p_piston, p_container, p_shadow, p_outfloor;

inline void updateUniform()
{
    b_draw_uniform->bind(GL_UNIFORM_BUFFER).bufferData(sizeof(u_data), &u_data, GL_STATIC_DRAW);
}

void init()
{
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    simulator = std::make_unique<Simulator>();
    printf("Created SPH %d particles.\n", Simulator::C.particle_num);

    u_data.lpv_mat = lp_mat * lv_mat;
    (b_draw_uniform = glo::buffer::create())
        ->bind(GL_UNIFORM_BUFFER)
        .bufferData(sizeof(u_data), &u_data, GL_STATIC_DRAW);
    b_draw_uniform->bindBase(GL_UNIFORM_BUFFER, 2);

    // create sky cube
    {
        util::image::sptr_t img[6] = {
            util::image::fromFile("./resources/sky.px.jpg"),
            util::image::fromFile("./resources/sky.nx.jpg"),
            util::image::fromFile("./resources/sky.py.jpg"),
            util::image::fromFile("./resources/sky.ny.jpg"),
            util::image::fromFile("./resources/sky.pz.jpg"),
            util::image::fromFile("./resources/sky.nz.jpg"),
        };
        glActiveTexture(GL_TEXTURE2);
        tex_sky = glo::texture::create();
        tex_sky->bind(GL_TEXTURE_CUBE_MAP);
        for (int i = 0; i < 6; ++i)
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB,
                         img[i]->width, img[i]->height, 0, GL_RGB, GL_UNSIGNED_BYTE, img[i]->bytes.data());
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    }

    // create floating cube texture
    {
        namespace head = R::image::soil_head_png;
        namespace side = R::image::soil_side_png;
        namespace bot  = R::image::soil_bottom_png;
        glActiveTexture(GL_TEXTURE9);
        tex_cube = glo::texture::create();
        tex_cube->bind(GL_TEXTURE_CUBE_MAP);
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGB, side::width, side::height, 0, GL_RGB, GL_UNSIGNED_BYTE, side::data);
        glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGB, side::width, side::height, 0, GL_RGB, GL_UNSIGNED_BYTE, side::data);
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGB, head::width, head::height, 0, GL_RGB, GL_UNSIGNED_BYTE, head::data);
        glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGB, bot::width, bot::height, 0, GL_RGB, GL_UNSIGNED_BYTE, bot::data);
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGB, side::width, side::height, 0, GL_RGB, GL_UNSIGNED_BYTE, side::data);
        glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGB, side::width, side::height, 0, GL_RGB, GL_UNSIGNED_BYTE, side::data);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_MIRRORED_REPEAT);
    }

    // create tile texture
    {
        auto c_img = util::image::fromFile("./resources/tile.jpg");
        auto n_img = util::image::fromFile("./resources/tile.normal.jpg");
        (tex_floor = glo::texture::create())
            ->bind(GL_TEXTURE5, GL_TEXTURE_2D_ARRAY)
            .storage3D(1, GL_RGB8, c_img->width, c_img->height, 2)
            .subImage3D(0, 0, 0, 0, c_img->width, c_img->height, 1, GL_RGB, GL_UNSIGNED_BYTE, c_img->bytes.data())
            .subImage3D(0, 0, 0, 1, n_img->width, n_img->height, 1, GL_RGB, GL_UNSIGNED_BYTE, n_img->bytes.data())
            .parameteri(GL_TEXTURE_MAG_FILTER, GL_LINEAR)
            .parameteri(GL_TEXTURE_MIN_FILTER, GL_LINEAR)
            .parameteri(GL_TEXTURE_WRAP_S, GL_REPEAT)
            .parameteri(GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    // create chain texture
    {
        auto c_img = util::image::fromFile("./resources/chain.png");
        auto n_img = util::image::fromFile("./resources/chain.normal.png");
        (tex_chain = glo::texture::create())
            ->bind(GL_TEXTURE8, GL_TEXTURE_2D_ARRAY)
            .storage3D(1, GL_RGBA8, c_img->width, c_img->height, 2)
            .subImage3D(0, 0, 0, 0, c_img->width, c_img->height, 1, GL_RGBA, GL_UNSIGNED_BYTE, c_img->bytes.data())
            .subImage3D(0, 0, 0, 1, n_img->width, n_img->height, 1, GL_RGBA, GL_UNSIGNED_BYTE, n_img->bytes.data())
            .parameteri(GL_TEXTURE_MAG_FILTER, GL_LINEAR)
            .parameteri(GL_TEXTURE_MIN_FILTER, GL_LINEAR)
            .parameteri(GL_TEXTURE_WRAP_S, GL_REPEAT)
            .parameteri(GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    // create shadow framebuffer
    {
        (tex_shadow = glo::texture::create())
            ->bind(GL_TEXTURE6, GL_TEXTURE_2D)
            .image2D(0, GL_RGBA, 4096, 4096, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr)
            .parameteri(GL_TEXTURE_MIN_FILTER, GL_NEAREST)
            .parameteri(GL_TEXTURE_MAG_FILTER, GL_NEAREST)
            .parameteri(GL_TEXTURE_WRAP_S, GL_CLAMP)
            .parameteri(GL_TEXTURE_WRAP_T, GL_CLAMP);

        (tex_shadow_depth = glo::texture::create())
            ->bind(GL_TEXTURE7, GL_TEXTURE_2D)
            .image2D(0, GL_DEPTH_COMPONENT32, 4096, 4096, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr)
            .parameteri(GL_TEXTURE_MIN_FILTER, GL_NEAREST)
            .parameteri(GL_TEXTURE_MAG_FILTER, GL_NEAREST)
            .parameteri(GL_TEXTURE_WRAP_S, GL_CLAMP)
            .parameteri(GL_TEXTURE_WRAP_T, GL_CLAMP);

        (fb_shadow = glo::framebuffer::create())
            ->bind(GL_FRAMEBUFFER)
            .texture2D(GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex_shadow, 0)
            .texture2D(GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, tex_shadow_depth, 0)
            .checkStatus();
    }

    // create draw programs
    p_screen = glo::program::create(
        { glo::shader::create(GL_VERTEX_SHADER, { R::shader::render_header, R::shader::t_vert, R::shader::render_quad }),
          glo::shader::create(GL_FRAGMENT_SHADER, { R::shader::render_header, R::shader::t_frag, R::shader::render_quad }) });
    p_sky = glo::program::create(
        { glo::shader::create(GL_VERTEX_SHADER, { R::shader::render_header, R::shader::t_vert, R::shader::render_sky }),
          glo::shader::create(GL_FRAGMENT_SHADER, { R::shader::render_header, R::shader::t_frag, R::shader::render_sky }) });
    p_floor = glo::program::create(
        { glo::shader::create(GL_VERTEX_SHADER, { R::shader::render_header, R::shader::t_vert, R::shader::render_floor }),
          glo::shader::create(GL_FRAGMENT_SHADER, { R::shader::render_header, R::shader::t_frag, R::shader::render_floor }) });
    p_cube = glo::program::create(
        { glo::shader::create(GL_VERTEX_SHADER, { R::shader::render_header, R::shader::t_vert, R::shader::render_cube }),
          glo::shader::create(GL_GEOMETRY_SHADER, { R::shader::render_header, R::shader::t_geom, R::shader::render_cube }),
          glo::shader::create(GL_FRAGMENT_SHADER, { R::shader::render_header, R::shader::t_frag, R::shader::render_cube }) });
    p_piston = glo::program::create(
        { glo::shader::create(GL_VERTEX_SHADER, { R::shader::render_header, R::shader::t_vert, R::shader::render_piston }),
          glo::shader::create(GL_GEOMETRY_SHADER, { R::shader::render_header, R::shader::t_geom, R::shader::render_piston }),
          glo::shader::create(GL_FRAGMENT_SHADER, { R::shader::render_header, R::shader::t_frag, R::shader::render_piston }) });
    p_container = glo::program::create(
        { glo::shader::create(GL_VERTEX_SHADER, { R::shader::render_header, R::shader::t_vert, R::shader::render_container }),
          glo::shader::create(GL_GEOMETRY_SHADER, { R::shader::render_header, R::shader::t_geom, R::shader::render_container }),
          glo::shader::create(GL_FRAGMENT_SHADER, { R::shader::render_header, R::shader::t_frag, R::shader::render_container }) });
    p_outfloor = glo::program::create(
        { glo::shader::create(GL_VERTEX_SHADER, { R::shader::render_header, R::shader::t_vert, R::shader::render_outfloor }),
          glo::shader::create(GL_FRAGMENT_SHADER, { R::shader::render_header, R::shader::t_frag, R::shader::render_outfloor }) });

    water::init();

    // print input methods
    printf("\n"
           "------------- INPUT -------------\n"
           "Camera Rotate: Drag Mouse LEFT   \n"
           "Camera Zoom  : Scroll            \n"
           "Move Focus   : Drag Mouse MIDDLE \n"
           "---------------------------------\n"
           "Pause : SPACE   | Piston: W      \n"
           "Frame : F       |                \n"
           "Reset : R       |                \n"
           "---------------------------------\n");
}

void resize(GLFWwindow*, int w, int h)
{
    if (w == 0 || h == 0) return; // avoid crash
    u_data.w_size = glm::vec2(w, h);

    p_mat = glm::perspective(
        glm::radians(90.),     // FoV
        double(w) / double(h), // ratio
        1., 500.);             // zNear, zFar (affects to precision)

    (tex_screen = glo::texture::create())
        ->bind(GL_TEXTURE0, GL_TEXTURE_2D)
        .image2D(0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr)
        .parameteri(GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        .parameteri(GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    (tex_screen_depth = glo::texture::create())
        ->bind(GL_TEXTURE1, GL_TEXTURE_2D)
        .image2D(0, GL_DEPTH24_STENCIL8, w, h, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, nullptr)
        .parameteri(GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        .parameteri(GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    (fb_screen = glo::framebuffer::create())
        ->bind(GL_FRAMEBUFFER)
        .texture2D(GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex_screen, 0)
        .texture2D(GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, tex_screen_depth, 0)
        .checkStatus();

    water::resize(w, h);
}

void update(double dt)
{
    if (!pause || one_frame) {
        simulator->update(dt);
        one_frame = false;
    }
}

void render()
{
    v_mat             = camera.mat();
    u_data.eye        = camera.eye();
    u_data.piston_pos = simulator->global.piston_pos;

    u_data.v_mat  = lv_mat;
    u_data.p_mat  = lp_mat;
    u_data.pv_mat = lp_mat * lv_mat;
    updateUniform();

    if (auto _ = fb_shadow->bind(GL_FRAMEBUFFER)) {
        glViewport(0, 0, 4096, 4096);

        glClearColor(0, 0, 0, 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        p_piston->use();
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 14);

        p_cube->use();
        simulator->drawCube();

        glBlendFunc(GL_ONE, GL_ONE);
        glDisable(GL_DEPTH_TEST);
        water::pg[0]->use();
        simulator->drawWater();
        glEnable(GL_DEPTH_TEST);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    u_data.v_mat  = v_mat;
    u_data.p_mat  = p_mat;
    u_data.pv_mat = p_mat * v_mat;
    updateUniform();

    glViewport(0, 0, u_data.w_size.x, u_data.w_size.y);
    glClearColor(.3f, .3f, .3f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (auto _ = fb_screen->bind(GL_FRAMEBUFFER)) {
        glClearColor(.3f, .3f, .3f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        p_sky->use();
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 14);

        p_floor->use();
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        p_outfloor->use();
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 10);

        p_piston->use();
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 14);

        p_cube->use();
        simulator->drawCube();
    }

    glClearColor(.3f, .3f, .3f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    tex_shadow->bind(GL_TEXTURE6, GL_TEXTURE_2D);
    tex_screen->bind(GL_TEXTURE0, GL_TEXTURE_2D);
    tex_screen_depth->bind(GL_TEXTURE1, GL_TEXTURE_2D);
    p_screen->use();
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    static auto drawWater = [&] { simulator->drawWater(); };
    water::preRender(drawWater);
    water::render();

    glDepthFunc(GL_ALWAYS);
    p_container->use();
    glDrawArrays(GL_LINES, 0, 32);
    glDepthFunc(GL_LEQUAL);
}

void mousepress(GLFWwindow*, int button, int action, int)
{
    switch (button) {
    case GLFW_MOUSE_BUTTON_LEFT:
    case GLFW_MOUSE_BUTTON_RIGHT:
    case GLFW_MOUSE_BUTTON_MIDDLE:
        mouse_state[button] = action == GLFW_PRESS;
        break;
    default: break;
    }
}

void mousemove(GLFWwindow*, double x, double y)
{
    auto coord  = glm::dvec2(x, y);
    auto diff   = coord - mouse_coord;
    mouse_coord = coord;

    if (mouse_state[GLFW_MOUSE_BUTTON_LEFT]) { // interact with objects
    }
    else if (mouse_state[GLFW_MOUSE_BUTTON_RIGHT]) { // rotate camera
        camera.theta = std::fmod(camera.theta - 0.01 * diff.x, glm::two_pi<double>());
        camera.phi   = std::clamp(camera.phi - 0.01 * diff.y, 0.01, glm::pi<double>() - 0.01);
    }
    else if (mouse_state[GLFW_MOUSE_BUTTON_MIDDLE]) { // move focus
        camera.focus.x -= .1 * (diff.y * glm::sin(camera.theta) + diff.x * glm::cos(camera.theta));
        camera.focus.z -= .1 * (diff.y * glm::cos(camera.theta) - diff.x * glm::sin(camera.theta));
    }
}

void mousescroll(GLFWwindow*, double, double offset) // zoom camera
{
    camera.radius = std::clamp(camera.radius * (1. - 0.05 * offset), 0.5, 100.);
}

void keypress(GLFWwindow*, int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS) {
        switch (key) {
        case GLFW_KEY_SPACE: pause = !pause; break;
        case GLFW_KEY_F: one_frame = true; break;
        case GLFW_KEY_W: simulator->piston_move = !simulator->piston_move; break;
        case GLFW_KEY_R: simulator = std::make_unique<Simulator>(); break;
        default: break;
        }
    }
}

glm::mat4 Camera::mat()
{
    const auto n_pos = glm::dvec3(
        radius * glm::sin(phi) * glm::sin(theta),
        radius * glm::cos(phi),
        radius * glm::sin(phi) * glm::cos(theta));
    r_pos = glm::mix(r_pos, n_pos, 0.5);
    return glm::lookAt(focus + r_pos, focus, glm::dvec3(0, 1.f, 0));
}
}
