#pragma once
#include "R.h"
#include "glo/program.hpp"
#include "glo/vertex_array.hpp"

class SPH {
public:
    const GLuint particle_num;

    std::vector<std::shared_ptr<glo::program>> p_compute;

    SPH(GLuint particle_num): particle_num(particle_num)
    {
        std::vector<glo::shader::sptr_t> shaders = {
            glo::shader::create(GL_COMPUTE_SHADER, { R::shader::simulation_header, R::shader::SPH_computeDensity }),
            glo::shader::create(GL_COMPUTE_SHADER, { R::shader::simulation_header, R::shader::SPH_computeForce }),
        };

        for (auto& shader : shaders)
            p_compute.push_back(glo::program::create({ shader }));

        for (auto& program : p_compute) {
            glUniformBlockBinding(program->id, 0, 0);
            glUniformBlockBinding(program->id, 1, 1);
        }
    }

    void computeDensity()
    {
        p_compute[0]->use();
        glDispatchCompute(particle_num / 128, 1, 1);
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
    }

    void computeForce()
    {
        p_compute[1]->use();
        glDispatchCompute(particle_num / 128, 1, 1);
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
    }
};
