#pragma once
#include "glo/program.hpp"
#include "glo/vertex_array.hpp"
#include "util/thread_worker.hpp"

#include "SPH.hpp"
#include "mass_spring.hpp"

class Simulator {
public:
    static constexpr glm::ivec3 cube_num          = { 4, 4, 4 };
    static constexpr GLuint     cube_particle_num = cube_num.x * cube_num.y * cube_num.z; // particle num forming cubes
    static constexpr GLfloat    spring_length     = 1.0f;

    static constexpr struct {
        alignas(4) GLuint particle_num  = 384 * 128; //384 * 128; // total num of particles
        alignas(4) GLfloat rest_density = 4.2f;
        alignas(4) GLfloat mass         = 1.f;  // particle weight
        alignas(4) GLfloat h            = 1.f;  // effective radius
        alignas(4) GLfloat k            = 50.f; // pressure coefficient
        alignas(4) GLfloat mu           = 0.8f; // viscosity coefficient

        alignas(16) glm::vec3 limit_min = { -40.f, h / 2.f, -20.f };      // minimum space limit
        alignas(16) glm::vec3 limit_max = { 20.f, h / 2.f + 80.f, 20.f }; // maximum space limit
        alignas(16) glm::ivec3 cell_num = { 60, 80, 40 };                 // num of grid cell

        alignas(16) glm::vec4 gravity = { 0, -0.6f, 0, 0 };

        alignas(4) GLuint f_disabled = 0x1;  // 0 if no exist
        alignas(4) GLuint f_fixed    = 0x2;  // 0 if free
        alignas(4) GLuint f_t_water  = 0x10; // water particle type
        alignas(4) GLuint f_t_object = 0x20; // floating object type
    } C = {};

    struct {
        alignas(4) GLfloat time_step  = 0.01f; // dt
        alignas(4) GLfloat piston_pos = 20.f;  // moving wall offset
        alignas(4) GLfloat piston_vel = 0;     // moving wall velocity
    } global;

private:
    std::shared_ptr<glo::buffer> b_particle, b_hashtable, b_spring_force, b_constant, b_global;

    std::vector<std::shared_ptr<glo::program>> p_compute;

    static constexpr int iteration_n = 10;

    double waterfall_t = 0;
    GLuint freeze_i    = cube_particle_num;

    SPH                  sph;
    MassSpringSimulation mss;

    static double piston_t;

    void initialize();

    void updateGlobal();
    void makeHashTable();
    void integrate();
    void updateCube();

    class ComputeSpringThread : public util::thread_worker {
        Simulator& simulator;

        glm::vec4* position = nullptr;
        glm::vec4* velocity = nullptr;
        glm::vec4* force    = nullptr;
        GLuint*    flags    = nullptr;

        void onStart() override;
        void onWork() override;
        void onFinish() override;

    public:
        ComputeSpringThread(Simulator& s): simulator(s) {}
    } compute_spring_worker;

    glo::vertex_array::sptr_t vao_water, vao_cube;

    std::array<GLuint, 8> i_cube;
    glo::buffer::sptr_t   b_cube_pos;

public:
    Simulator();

    void update(double dt);
    void drawCube();
    void drawWater();

    static bool piston_move;
};
