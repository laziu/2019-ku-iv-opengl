#pragma once
#include "R.h"
#include "glo/framebuffer.hpp"
#include "glo/program.hpp"
#include "glo/texture.hpp"
#include <functional>

namespace viewer::water {
inline glo::texture::sptr_t     tex_thickness, tex_depth[2];
inline glo::framebuffer::sptr_t fb[3];
inline glo::program::sptr_t     pg[4];

inline void init()
{
    pg[0] = glo::program::create(
        { glo::shader::create(GL_VERTEX_SHADER, { R::shader::render_header, "#define PARTICLE_VERT\n", R::shader::render_water }),
          glo::shader::create(GL_GEOMETRY_SHADER, { R::shader::render_header, "#define PARTICLE_GEOM\n", R::shader::render_water }),
          glo::shader::create(GL_FRAGMENT_SHADER, { R::shader::render_header, "#define THICKNESS_FRAG\n", R::shader::render_water }) });
    pg[1] = glo::program::create(
        { glo::shader::create(GL_VERTEX_SHADER, { R::shader::render_header, "#define PARTICLE_VERT\n", R::shader::render_water }),
          glo::shader::create(GL_GEOMETRY_SHADER, { R::shader::render_header, "#define PARTICLE_GEOM\n", R::shader::render_water }),
          glo::shader::create(GL_FRAGMENT_SHADER, { R::shader::render_header, "#define DEPTH_FRAG\n", R::shader::render_water }) });
    pg[2] = glo::program::create(
        { glo::shader::create(GL_VERTEX_SHADER, { R::shader::render_header, R::shader::t_vert, R::shader::render_quad }),
          glo::shader::create(GL_FRAGMENT_SHADER, { R::shader::render_header, "#define SMOOTH_FRAG\n", R::shader::render_water }) });
    pg[3] = glo::program::create(
        { glo::shader::create(GL_VERTEX_SHADER, { R::shader::render_header, R::shader::t_vert, R::shader::render_quad }),
          glo::shader::create(GL_FRAGMENT_SHADER, { R::shader::render_header, "#define DRAW_NORMAL_FRAG\n", R::shader::render_water }) });
}

inline void resize(int w, int h)
{
    (tex_thickness = glo::texture::create())
        ->bind(GL_TEXTURE_2D)
        .image2D(0, GL_RGBA32F, w, h, 0, GL_RGBA, GL_FLOAT, nullptr)
        .parameteri(GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        .parameteri(GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    for (int i = 0; i < 2; ++i)
        (tex_depth[i] = glo::texture::create())
            ->bind(GL_TEXTURE_2D)
            .image2D(0, GL_DEPTH_COMPONENT32, w, h, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr)
            .parameteri(GL_TEXTURE_MIN_FILTER, GL_LINEAR)
            .parameteri(GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    (fb[0] = glo::framebuffer::create())
        ->bind(GL_FRAMEBUFFER)
        .texture2D(GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex_thickness, 0)
        .checkStatus();
    (fb[1] = glo::framebuffer::create())
        ->bind(GL_FRAMEBUFFER)
        .texture2D(GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, tex_depth[0], 0)
        .checkStatus();
    (fb[2] = glo::framebuffer::create())
        ->bind(GL_FRAMEBUFFER)
        .texture2D(GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, tex_depth[1], 0)
        .checkStatus();
}

inline void preRender(const std::function<void(void)>& drawWater)
{
    if (auto _ = fb[0]->bind(GL_FRAMEBUFFER)) {
        glDisable(GL_DEPTH_TEST);
        glBlendFunc(GL_ONE, GL_ONE);
        glClearColor(0, 0, 0, 0);
        glClear(GL_COLOR_BUFFER_BIT);
        pg[0]->use();
        drawWater();
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_DEPTH_TEST);
    }
    if (auto _ = fb[1]->bind(GL_FRAMEBUFFER)) {
        glClear(GL_DEPTH_BUFFER_BIT);
        pg[1]->use();
        drawWater();
    }
    if (auto _ = fb[2]->bind(GL_FRAMEBUFFER)) {
        tex_depth[0]->bind(GL_TEXTURE4, GL_TEXTURE_2D);
        glClear(GL_DEPTH_BUFFER_BIT);
        pg[2]->use();
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }
}

inline void render()
{
    tex_thickness->bind(GL_TEXTURE3, GL_TEXTURE_2D);
    tex_depth[1]->bind(GL_TEXTURE4, GL_TEXTURE_2D);
    pg[3]->use();
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}
}
