void collision_check(uint i, inout vec4 pos, inout vec4 vel)
{
    vec3 min_pos = vec3(limit_min.x + piston_pos, limit_min.yz);
    vec3 min_vel = vec3(max(piston_vel, 0), 0, 0);
    vec3 max_pos = limit_max.xyz;
    vec3 max_vel = vec3(0, 0, 0);

    vec3  min_rvel = vel.xyz - min_vel;
    vec3  max_rvel = vel.xyz - max_vel;
    bvec3 min_coll = lessThan(pos.xyz, min_pos);
    bvec3 max_coll = greaterThan(pos.xyz, max_pos);

    pos = vec4(max(min_pos + 0.001, min(max_pos - 0.001, pos.xyz)), pos.w);
    vel = vec4(min_coll.x ? min_vel.x + 0.6 * abs(min_rvel.x) : max_coll.x ? max_vel.x - 0.6 * abs(max_rvel.x) : vel.x,
               min_coll.y ? min_vel.y + 0.6 * abs(min_rvel.y) : max_coll.y ? max_vel.y - 0.6 * abs(max_rvel.y) : vel.y,
               min_coll.z ? min_vel.z + 0.6 * abs(min_rvel.z) : max_coll.z ? max_vel.z - 0.6 * abs(max_rvel.z) : vel.z,
               vel.w);
}

void main()
{
    uint i = gl_GlobalInvocationID.x;
    if (bool(flags[i] & f_disabled)) return; // not existing particle

    vec4 vel = velocity[i];
    vec4 pos = position[i];

    if (!bool(flags[i] & f_fixed)) { // integrate if not freeze
        vec4 acc = force[i] / density[i] + gravity;
        if (bool(flags[i] & f_t_object))
            acc += spring_force[i];

        vel += acc * time_step;
        pos += vel * time_step;
    }

    collision_check(i, pos, vel);

    velocity[i] = vel;
    position[i] = pos;
}
