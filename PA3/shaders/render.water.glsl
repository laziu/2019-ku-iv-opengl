#ifdef PARTICLE_VERT // get particle position and pass to geometry shader ---------------
layout(location = 0) in vec4 position;
layout(location = 1) in uint flags;

out uint g_flags;

void main()
{
    gl_Position = position;
    g_flags     = flags;
}
#endif
#ifdef PARTICLE_GEOM // get particle position and generate quads to draw sphere ---------
layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

in uint g_flags[];

out GEOM_OUT
{
    vec3 v_position;
    vec2 coordinate;
}
g_out;

void GenVertex(vec4 v_position, vec2 coordinate)
{
    gl_Position      = p_mat * v_position;
    g_out.v_position = v_position.xyz;
    g_out.coordinate = coordinate;
    EmitVertex();
}

void main()
{
    if (bool(g_flags[0] & 0x1)) return; // not existing particle

    vec4  center = v_mat * gl_in[0].gl_Position;
    float r      = particle_radius;

    GenVertex(center + vec4(+r, +r, 0., 0.), vec2(+1., +1.));
    GenVertex(center + vec4(+r, -r, 0., 0.), vec2(+1., -1.));
    GenVertex(center + vec4(-r, +r, 0., 0.), vec2(-1., +1.));
    GenVertex(center + vec4(-r, -r, 0., 0.), vec2(-1., -1.));

    EndPrimitive();
}
#endif
#ifdef DEPTH_FRAG // get quads and generate depth map -----------------------------------
in GEOM_OUT
{
    vec3 v_position;
    vec2 coordinate;
}
f_in;

void main()
{
    if (length(f_in.coordinate) > 1) discard;
    float theta  = acos(length(f_in.coordinate));
    vec3  v_pos  = f_in.v_position + vec3(0, 0, particle_radius * sin(theta));
    vec4  pv_pos = p_mat * vec4(v_pos, 1);
    gl_FragDepth = (pv_pos.z / pv_pos.w) / 2 + 0.5;
}
#endif
#ifdef THICKNESS_FRAG // get quads and generate thickness map ---------------------------
layout(location = 0) out vec4 FragColor;

in GEOM_OUT
{
    vec3 v_position;
    vec2 coordinate;
}
f_in;

void main()
{
    if (length(f_in.coordinate) > 1) discard;
    float theta = acos(length(f_in.coordinate));
    FragColor   = vec4(0.01 * sin(theta));
}
#endif
#ifdef SMOOTH_FRAG // smoothing depth texture -------------------------------------------
bool written(vec2 uv)
{
    return texture(tex_water_depth, uv).x < 0.999999;
}

vec3 eyepos(vec2 uv)
{
    float depth = texture(tex_water_depth, uv).x;
    vec4  p_pos = vec4(vec3(uv, depth) * 2 - 1, 1);
    vec4  v_pos = inverse(p_mat) * p_pos;
    return v_pos.xyz / v_pos.w;
}

void main()
{
    vec2 resolution = textureSize(tex_water_depth, 0);
    vec2 texcoords  = gl_FragCoord.xy / resolution;
    if (!written(texcoords)) discard;

    vec2 step   = vec2(1, 1) / resolution;
    vec2 offset = vec2(20, 20) / resolution;

    float depth = length(eyepos(texcoords));
    vec2  sum   = vec2(0, 0);
    for (float x = -offset.x; x <= offset.x; x += step.x) {
        for (float y = -offset.y; y <= offset.y; y += step.y) {
            vec2 uv = texcoords + vec2(x, y);
            if (written(uv)) {
                float d = length(eyepos(uv));

                float r = length(vec2(x, y)) * resolution.y / 8;
                float w = exp(-r * r);
                float g = exp(-(d - depth) * (d - depth) * 0.05);

                sum += vec2(d * w * g, w * g);
            }
        }
    }
    float smoothed_depth = sum.x / sum.y;

    vec3 v_pos   = eyepos(texcoords) / depth * smoothed_depth;
    vec4 pv_pos  = p_mat * vec4(v_pos, 1);
    gl_FragDepth = (pv_pos.z / pv_pos.w) / 2 + 0.5;
}
#endif
#ifdef DRAW_NORMAL_FRAG // draw depth texture with calculating normals ------------------
out vec4 FragColor;

bool written(vec2 uv)
{
    return texture(tex_water_depth, uv).x < 0.999999;
}

vec3 eyepos(vec2 uv)
{
    float depth = texture(tex_water_depth, uv).x;
    vec4  p_pos = vec4(vec3(uv, depth) * 2 - 1, 1);
    vec4  v_pos = inverse(p_mat) * p_pos;
    return v_pos.xyz / v_pos.w;
}

float shadow(vec2 uv)
{
    float depth   = texture(tex_screen_depth, uv).x;
    vec4  pv_pos  = vec4(vec3(uv, depth) * 2 - 1, 1);
    vec4  pos     = inverse(pv_mat) * pv_pos;
    vec4  lpv_pos = lpv_mat * pos;
    vec3  luv     = (lpv_pos.xyz / lpv_pos.w) / 2 + 0.5;
    float s_depth = texture(tex_shadow_depth, luv.xy).x;
    if (abs(s_depth - luv.z) < 0.00025 || depth > 0.999) return 0;
    return 0.5 * texture(tex_shadow, luv.xy).a;
}

void main()
{
    vec2 resolution = textureSize(tex_water_depth, 0);
    vec2 texcoords  = gl_FragCoord.xy / resolution;
    if (!written(texcoords)) discard;

    vec2 step = vec2(1, 1) / resolution;

    vec3 pos = eyepos(texcoords);

    vec3 ddx  = eyepos(texcoords + vec2(step.x, 0)) - pos;
    vec3 ddx2 = pos - eyepos(texcoords - vec2(step.x, 0));
    if (abs(ddx.z) > abs(ddx2.z)) ddx = ddx2;

    vec3 ddy  = eyepos(texcoords + vec2(0, step.y)) - pos;
    vec3 ddy2 = pos - eyepos(texcoords - vec2(0, step.y));
    if (abs(ddy.z) > abs(ddy2.z)) ddy = ddy2;

    vec3 normal = normalize(cross(ddx.xyz, ddy.xyz));

    vec3 light    = (v_mat * dir_light[0]).xyz;
    vec3 half_dir = normalize(light + vec3(0, 0, 1));

    float diffuse  = max(0, dot(normal, light));
    float specular = pow(max(dot(normal, half_dir), 0), 7);
    float fresnel  = pow(1 - abs(normal.z), 5);

    float thickness = texture(tex_water_thickness, texcoords).x;

    vec2 bg_uv   = texcoords + normal.xy * 0.05 * thickness;
    vec3 bgColor = texture(tex_screen, bg_uv).xyz - shadow(bg_uv);
    vec3 frColor = vec3(0.5, 0.8, 0.9) * (0.3 + 0.4 * diffuse + 1.2 * specular + 5.0 * fresnel);

    float alpha  = 2.5 * thickness;
    FragColor    = vec4(bgColor + alpha * frColor + vec3(0.05, 0.08, 0.09), 1);
    gl_FragDepth = texture(tex_water_depth, texcoords).x;
}
#endif
