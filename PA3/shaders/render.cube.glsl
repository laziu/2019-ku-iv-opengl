#ifdef VERTEX_SHADER
layout(location = 0) in vec4 position;

const vec3 uvs[14] = vec3[](
    vec3(-1, -1, -1),
    vec3(-1, -1, +1),
    vec3(-1, +1, -1),
    vec3(-1, +1, +1),
    vec3(+1, +1, +1),
    vec3(-1, -1, +1),
    vec3(+1, -1, +1),
    vec3(-1, -1, -1),
    vec3(+1, -1, -1),
    vec3(-1, +1, -1),
    vec3(+1, +1, -1),
    vec3(+1, +1, +1),
    vec3(+1, -1, -1),
    vec3(+1, -1, +1));

out vec3 texcoords;

void main()
{
    gl_Position = position;
    texcoords   = uvs[gl_VertexID];
}
#endif
#ifdef GEOMETRY_SHADER
layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec3 texcoords[];

out GEOM_OUT
{
    vec3 position;
    vec3 normal;
    vec3 texcoords;
}
g_out;

void GenVertex(vec4 position, vec3 normal, vec3 texcoord)
{
    gl_Position     = pv_mat * position;
    g_out.position  = position.xyz;
    g_out.normal    = normal;
    g_out.texcoords = texcoord;
    EmitVertex();
}

void main()
{
    vec3 normal = normalize(cross(gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz,
                                  gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz));
    GenVertex(gl_in[0].gl_Position, normal, texcoords[0]);
    GenVertex(gl_in[1].gl_Position, normal, texcoords[1]);
    GenVertex(gl_in[2].gl_Position, normal, texcoords[2]);
    EndPrimitive();
}
#endif
#ifdef FRAGMENT_SHADER
out vec4 FragColor;

in GEOM_OUT
{
    vec3 position;
    vec3 normal;
    vec3 texcoords;
}
f_in;

//const vec3 color = vec3(1.0, 0.2, 0.2);

void main()
{
    vec3  color   = texture(tex_cube, f_in.texcoords).rgb;
    float d_scale = dirLightScale(dir_light[0].xyz, f_in.position.xyz, f_in.normal, 0.5, 0.3, 5);
    float p_scale = pointLightScale(point_light[0].xyz, f_in.position.xyz, f_in.normal, 0.5, 0.3, 5);
    FragColor     = vec4(color * (0.3 + d_scale + p_scale), 1);
}
#endif
