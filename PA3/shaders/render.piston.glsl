#ifdef VERTEX_SHADER
const vec3 cube_position[14] = vec3[](
    vec3(-41.3, -0, -20.5),
    vec3(-41.3, -0, +20.5),
    vec3(-41.3, 10, -20.5),
    vec3(-41.3, 10, +20.5),
    vec3(-40.3, 10, +20.5),
    vec3(-41.3, -0, +20.5),
    vec3(-40.3, -0, +20.5),
    vec3(-41.3, -0, -20.5),
    vec3(-40.3, -0, -20.5),
    vec3(-41.3, 10, -20.5),
    vec3(-40.3, 10, -20.5),
    vec3(-40.3, 10, +20.5),
    vec3(-40.3, -0, -20.5),
    vec3(-40.3, -0, +20.5));

out vec2 texcoords;

void main()
{
    texcoords   = 0.25 * vec2(cube_position[gl_VertexID].x + cube_position[gl_VertexID].z, cube_position[gl_VertexID].y);
    gl_Position = vec4(cube_position[gl_VertexID] + vec3(piston_pos, 0, 0), 1);
}
#endif
#ifdef GEOMETRY_SHADER
layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec2 texcoords[];

out GEOM_OUT
{
    vec3 position;
    vec3 normal;
    vec2 texcoords;
}
g_out;

void GenVertex(vec4 position, vec3 normal, vec2 texcoord)
{
    gl_Position     = pv_mat * position;
    g_out.position  = position.xyz;
    g_out.normal    = normal;
    g_out.texcoords = texcoord;
    EmitVertex();
}

void main()
{
    vec3 normal = normalize(cross(gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz,
                                  gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz));
    GenVertex(gl_in[0].gl_Position, normal, texcoords[0]);
    GenVertex(gl_in[1].gl_Position, normal, texcoords[1]);
    GenVertex(gl_in[2].gl_Position, normal, texcoords[2]);
    EndPrimitive();
}
#endif
#ifdef FRAGMENT_SHADER
out vec4 FragColor;

in GEOM_OUT
{
    vec3 position;
    vec3 normal;
    vec2 texcoords;
}
f_in;

void main()
{
    vec4 color = texture(tex_chain, vec3(f_in.texcoords, 0));
    if (color.w < 0.1) discard;
    float d_scale = dirLightScale(dir_light[0].xyz, f_in.position.xyz, f_in.normal, 0.5, 0.3, 11);
    float p_scale = pointLightScale(point_light[0].xyz, f_in.position.xyz, f_in.normal, 0.5, 0.3, 11);
    FragColor     = vec4(color.rgb * (0.3 + 0.5 * d_scale + 0.9 * p_scale), 0.9);
}
#endif
