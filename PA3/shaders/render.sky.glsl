#ifdef VERTEX_SHADER
const vec3 cube_position[14] = vec3[](
    vec3(-1000, -1000, -1000),
    vec3(-1000, -1000, +1000),
    vec3(-1000, +1000, -1000),
    vec3(-1000, +1000, +1000),
    vec3(+1000, +1000, +1000),
    vec3(-1000, -1000, +1000),
    vec3(+1000, -1000, +1000),
    vec3(-1000, -1000, -1000),
    vec3(+1000, -1000, -1000),
    vec3(-1000, +1000, -1000),
    vec3(+1000, +1000, -1000),
    vec3(+1000, +1000, +1000),
    vec3(+1000, -1000, -1000),
    vec3(+1000, -1000, +1000));

out vec3 texcoords;

void main()
{
    texcoords   = cube_position[gl_VertexID];
    gl_Position = (pv_mat * vec4(texcoords, 1)).xyww;
}
#endif
#ifdef FRAGMENT_SHADER
out vec4 FragColor;

in vec3 texcoords;

void main()
{
    FragColor = texture(tex_sky, texcoords);
}
#endif
