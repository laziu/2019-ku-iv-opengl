float poly6Kernel(vec4 delta)
{
    float dist = length(delta);
    return 4.0 / (PI * pow(h, 8)) * pow(h * h - dist * dist, 3);
}

void main()
{
    uint i = gl_GlobalInvocationID.x;
    if (bool(flags[i] & f_disabled)) return; // not existing particle
    ivec3 gpos = gridPosition(position[i].xyz);

    float density_sum = 0;

    for (int d = 0; d < 27; ++d) {
        ivec3 npos = gpos + direction[d];
        if (!isGridIndexValid(npos)) continue;

        for (int j = hash[gridIndex(npos)]; j != -1; j = hash[j]) {
            vec4  delta = position[i] - position[j];
            float q     = length(delta) / h;
            if (0.0 <= q && q <= 1.0)
                density_sum += mass * poly6Kernel(delta);
        }
    }

    density[i] = density_sum > 0 ? density_sum : 0.001;
}
