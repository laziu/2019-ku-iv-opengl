void applySafe(inout vec4 dst, vec4 src)
{
    dst = vec4(isnan(src.x) || isinf(src.x) ? dst.x : src.x,
               isnan(src.y) || isinf(src.y) ? dst.y : src.y,
               isnan(src.z) || isinf(src.z) ? dst.z : src.z,
               dst.w);
}

vec4 spikygradientKernel(vec4 delta, float q)
{
    return (-30.0 / (PI * pow(h, 8))) * (pow(1.0 - q, 2) / q) * delta;
}

float viscositylaplacianKernel(vec4 delta, float q)
{
    return 40.0 / (PI * pow(h, 4)) * (1.0 - q);
}

void main()
{
    uint i = gl_GlobalInvocationID.x;
    if (bool(flags[i] & f_disabled)) return; // not existing particle
    ivec3 gpos = gridPosition(position[i].xyz);

    vec4 pressure_sum  = vec4(0, 0, 0, 0);
    vec4 viscosity_sum = vec4(0, 0, 0, 0);

    // regard object as light density particle
    float target_density = rest_density * (bool(flags[i] & f_t_object) ? 0.5 : 1);

    for (int d = 0; d < 27; ++d) {
        ivec3 npos = gpos + direction[d];
        if (!isGridIndexValid(npos)) continue;

        for (int j = hash[gridIndex(npos)]; j != -1; j = hash[j]) {
            if (i == j) continue;
            if (!bool((flags[i] | flags[j]) & f_t_water)) continue; // skip calculate between non-water
            vec4  delta = position[i] - position[j];
            float q     = length(delta) / h;
            if (0.0 < q && q <= 1.0) {
                float density_j     = bool(flags[j] & f_t_object) ? 10000.f : density[j]; // regard object as very big density particle
                float density_diff  = (density[i] + density_j) / 2.0 - target_density;
                vec4  velocity_diff = velocity[j] - velocity[i];
                applySafe(pressure_sum, pressure_sum + mass * (density_diff / density_j) * spikygradientKernel(delta, q));
                applySafe(viscosity_sum, viscosity_sum + mass * (velocity_diff / density_j) * viscositylaplacianKernel(delta, q));
            }
        }
    }
    pressure_sum  = -k * pressure_sum;
    viscosity_sum = mu * viscosity_sum;

    force[i] = pressure_sum + viscosity_sum;
}
