#ifdef VERTEX_SHADER
const vec2 quads[4] = vec2[](
    vec2(-1, -1),
    vec2(+1, -1),
    vec2(-1, +1),
    vec2(+1, +1));

void main()
{
    gl_Position = vec4(quads[gl_VertexID], 0, 1);
}
#endif
#ifdef FRAGMENT_SHADER
out vec4 FragColor;

float shadow(vec2 uv)
{
    float depth   = texture(tex_screen_depth, uv).x;
    vec4  pv_pos  = vec4(vec3(uv, depth) * 2 - 1, 1);
    vec4  pos     = inverse(pv_mat) * pv_pos;
    vec4  lpv_pos = lpv_mat * pos;
    vec3  luv     = (lpv_pos.xyz / lpv_pos.w) / 2 + 0.5;
    float s_depth = texture(tex_shadow_depth, luv.xy).x;
    if (abs(s_depth - luv.z) < 0.00025 || depth > 0.999) return 0;
    return 0.5 * texture(tex_shadow, luv.xy).a;
}

void main()
{
    vec2 uv = gl_FragCoord.xy / textureSize(tex_screen, 0);

    vec4 color   = texture(tex_screen, uv);
    FragColor    = vec4(color.rgb - shadow(uv), 1);
    gl_FragDepth = texture(tex_screen_depth, uv).x;
}
#endif
