#ifdef VERTEX_SHADER
const vec4 fixed_position[10] = vec4[](
    vec4(-25.5, -0.01, -25.5, 1),
    vec4(+50.5, -0.01, -50.5, 1),
    vec4(+25.5, -0.01, -25.5, 1),
    vec4(+50.5, -0.01, +50.5, 1),
    vec4(+25.5, -0.01, +25.5, 1),
    vec4(-50.5, -0.01, +50.5, 1),
    vec4(-25.5, -0.01, +25.5, 1),
    vec4(-50.5, -0.01, -50.5, 1),
    vec4(-25.5, -0.01, -25.5, 1),
    vec4(+50.5, -0.01, -50.5, 1));

out vec4 position;
out vec2 texcoords;

void main()
{
    position    = fixed_position[gl_VertexID];
    texcoords   = fixed_position[gl_VertexID].xz / 16;
    gl_Position = pv_mat * position;
}
#endif
#ifdef FRAGMENT_SHADER
out vec4 FragColor;

in vec4 position;
in vec2 texcoords;

void main()
{
    vec3  color   = 0.5 * texture(tex_floor, vec3(texcoords.xy, 0)).rgb;
    vec3  normal  = normalize(texture(tex_floor, vec3(texcoords.xy, 1)).yzx);
    float d_scale = dirLightScale(dir_light[0].xyz, position.xyz, normal, 0.5, 0.9, 11);
    float p_scale = 5 * pointLightScale(point_light[0].xyz, position.xyz, normal, 1.5, 1.0, 7);
    FragColor     = vec4(color * (0.1 + d_scale + p_scale), 0.6);
}
#endif
