void main()
{
    uint i = gl_GlobalInvocationID.x;
    if (bool(flags[i] & f_disabled)) return; // not existing particle
    ivec3 gpos = gridPosition(position[i].xyz);

    ivec3 gidx = ivec3(gpos.x < 0 ? 0 : gpos.x >= cell_num.x ? cell_num.x - 1 : gpos.x,
                       gpos.y < 0 ? 0 : gpos.y >= cell_num.y ? cell_num.y - 1 : gpos.y,
                       gpos.z < 0 ? 0 : gpos.z >= cell_num.z ? cell_num.z - 1 : gpos.z);

    hash[i] = atomicExchange(hash[gridIndex(gidx)], int(i));
}
