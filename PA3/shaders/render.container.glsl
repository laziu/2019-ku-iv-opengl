#ifdef VERTEX_SHADER
layout(location = 0) in vec4 position;

const vec3  p_min   = vec3(-40.5, +0, -20);
const vec3  p_max   = vec3(+20.5, 81, +20);
const float p_mid_x = -20.5;

const vec3 lines[32] = vec3[](
    vec3(p_min.x, p_min.y, p_min.z), vec3(p_min.x, p_min.y, p_max.z),
    vec3(p_min.x, p_min.y, p_min.z), vec3(p_min.x, p_max.y, p_min.z),
    vec3(p_min.x, p_max.y, p_min.z), vec3(p_min.x, p_max.y, p_max.z),
    vec3(p_min.x, p_min.y, p_max.z), vec3(p_min.x, p_max.y, p_max.z),
    vec3(p_mid_x, p_min.y, p_min.z), vec3(p_mid_x, p_min.y, p_max.z),
    vec3(p_mid_x, p_min.y, p_min.z), vec3(p_mid_x, p_max.y, p_min.z),
    vec3(p_mid_x, p_max.y, p_min.z), vec3(p_mid_x, p_max.y, p_max.z),
    vec3(p_mid_x, p_min.y, p_max.z), vec3(p_mid_x, p_max.y, p_max.z),
    vec3(p_max.x, p_min.y, p_min.z), vec3(p_max.x, p_min.y, p_max.z),
    vec3(p_max.x, p_min.y, p_min.z), vec3(p_max.x, p_max.y, p_min.z),
    vec3(p_max.x, p_max.y, p_min.z), vec3(p_max.x, p_max.y, p_max.z),
    vec3(p_max.x, p_min.y, p_max.z), vec3(p_max.x, p_max.y, p_max.z),
    vec3(p_min.x, p_min.y, p_min.z), vec3(p_max.x, p_min.y, p_min.z),
    vec3(p_min.x, p_min.y, p_max.z), vec3(p_max.x, p_min.y, p_max.z),
    vec3(p_min.x, p_max.y, p_min.z), vec3(p_max.x, p_max.y, p_min.z),
    vec3(p_min.x, p_max.y, p_max.z), vec3(p_max.x, p_max.y, p_max.z));

void main()
{
    gl_Position = vec4(lines[gl_VertexID], 1);
}
#endif
#ifdef GEOMETRY_SHADER
layout(lines) in;
layout(triangle_strip, max_vertices = 10) out;

void GenVertex(vec4 position)
{
    gl_Position = pv_mat * position;
    EmitVertex();
}

void main()
{
    vec3 direction = normalize(gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz);
    GenVertex(gl_in[0].gl_Position + vec4(0.05 * direction.yzx, 0));
    GenVertex(gl_in[1].gl_Position + vec4(0.05 * direction.yzx, 0));
    GenVertex(gl_in[0].gl_Position + vec4(0.05 * direction.zxy, 0));
    GenVertex(gl_in[1].gl_Position + vec4(0.05 * direction.zxy, 0));
    GenVertex(gl_in[0].gl_Position - vec4(0.05 * direction.yzx, 0));
    GenVertex(gl_in[1].gl_Position - vec4(0.05 * direction.yzx, 0));
    GenVertex(gl_in[0].gl_Position - vec4(0.05 * direction.zxy, 0));
    GenVertex(gl_in[1].gl_Position - vec4(0.05 * direction.zxy, 0));
    GenVertex(gl_in[0].gl_Position + vec4(0.05 * direction.yzx, 0));
    GenVertex(gl_in[1].gl_Position + vec4(0.05 * direction.yzx, 0));
    EndPrimitive();
}
#endif
#ifdef FRAGMENT_SHADER
out vec4 FragColor;

void main()
{
    FragColor = vec4(0, 0, 0, 0.1);
}
#endif
