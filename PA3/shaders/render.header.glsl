#version 430

#define DIR_LIGHT_COUNT   1
#define POINT_LIGHT_COUNT 1

layout(std140, binding = 2) uniform DrawUniforms
{
    mat4  p_mat;
    mat4  v_mat;
    mat4  pv_mat;
    mat4  lpv_mat;
    vec4  eye;
    vec4  dir_light[DIR_LIGHT_COUNT];
    vec4  point_light[POINT_LIGHT_COUNT];
    ivec2 w_size;
    float particle_radius;
    float piston_pos;
};

layout(binding = 0) uniform sampler2D tex_screen;
layout(binding = 1) uniform sampler2D tex_screen_depth;
layout(binding = 2) uniform samplerCube tex_sky;
layout(binding = 3) uniform sampler2D tex_water_thickness;
layout(binding = 4) uniform sampler2D tex_water_depth;
layout(binding = 5) uniform sampler2DArray tex_floor;
layout(binding = 6) uniform sampler2D tex_shadow;
layout(binding = 7) uniform sampler2D tex_shadow_depth;
layout(binding = 8) uniform sampler2DArray tex_chain;
layout(binding = 9) uniform samplerCube tex_cube;

#define PI 3.14159265358979323846264338328

float dirLightScale(vec3 light_dir, vec3 position, vec3 normal,
                    float diffuse_coef, float specular_coef, float shininess)
{
    vec3 eye_dir  = normalize(eye.xyz - position);
    vec3 half_dir = normalize(light_dir + eye_dir);

    float diffuse_scale  = diffuse_coef * max(dot(light_dir, normal), 0);
    float specular_scale = specular_coef * pow(max(dot(normal, half_dir), 0), shininess);

    return diffuse_scale + specular_scale;
}

float pointLightScale(vec3 light_pos, vec3 position, vec3 normal,
                      float diffuse_coef, float specular_coef, float shininess)
{
    vec3  light_dir = normalize(light_pos - position);
    float light_len = length(light_pos - position);
    vec3  eye_dir   = normalize(eye.xyz - position);
    vec3  half_dir  = normalize(light_dir + eye_dir);

    float diffuse_scale  = diffuse_coef * max(dot(light_dir, normal), 0);
    float specular_scale = specular_coef * pow(max(dot(normal, half_dir), 0), shininess);

    return (diffuse_scale + specular_scale) / (light_len * light_len);
}
