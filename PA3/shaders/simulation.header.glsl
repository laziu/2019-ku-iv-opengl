#version 430
#extension GL_ARB_compute_shader : enable
#extension GL_ARB_shader_storage_buffer_object : enable

layout(local_size_x = 128, local_size_y = 1, local_size_z = 1) in;

layout(std140, binding = 0) uniform Constants
{
    uint  particle_num;
    float rest_density;
    float mass;
    float h;
    float k;
    float mu;

    vec4  limit_min;
    vec4  limit_max;
    ivec4 cell_num;

    vec4 gravity;

    uint f_disabled;
    uint f_fixed;
    uint f_t_water;
    uint f_t_object;
};

layout(std140, binding = 1) uniform Globals
{
    float time_step;
    float piston_pos;
    float piston_vel;
};

layout(std430, binding = 0) buffer Pbuf { vec4 position[]; };
layout(std430, binding = 1) buffer Vbuf { vec4 velocity[]; };
layout(std430, binding = 2) buffer Fbuf { vec4 force[]; };
layout(std430, binding = 3) buffer Dbuf { float density[]; };
layout(std430, binding = 4) buffer Flag { uint flags[]; };

layout(std430, binding = 5) buffer Hash { int hash[]; }; // link[particle_num] + grid[cell_num.xyz]

layout(std430, binding = 6) buffer SprB { vec4 spring_force[]; };

#define PI 3.14159265358979323846264338328

ivec3 gridPosition(vec3 pos)
{
    return ivec3(int(cell_num.x * ((pos.x - limit_min.x) / (limit_max.x - limit_min.x))),
                 int(cell_num.y * ((pos.y - limit_min.y) / (limit_max.y - limit_min.y))),
                 int(cell_num.z * ((pos.z - limit_min.z) / (limit_max.z - limit_min.z))));
}

bool isGridIndexValid(ivec3 gpos)
{
    return (0 <= gpos.x && gpos.x < cell_num.x &&
            0 <= gpos.y && gpos.y < cell_num.y &&
            0 <= gpos.z && gpos.z < cell_num.z);
}

int gridIndex(ivec3 gpos)
{
    return int(particle_num) + (gpos.x * cell_num.y + gpos.y) * cell_num.z + gpos.z;
}

uniform ivec3 direction[27] = ivec3[27](
    ivec3(-1, -1, -1), ivec3(-1, -1, +0), ivec3(-1, -1, +1),
    ivec3(-1, +0, -1), ivec3(-1, +0, +0), ivec3(-1, +0, +1),
    ivec3(-1, +1, -1), ivec3(-1, +1, +0), ivec3(-1, +1, +1),
    ivec3(+0, -1, -1), ivec3(+0, -1, +0), ivec3(+0, -1, +1),
    ivec3(+0, +0, -1), ivec3(+0, +0, +0), ivec3(+0, +0, +1),
    ivec3(+0, +1, -1), ivec3(+0, +1, +0), ivec3(+0, +1, +1),
    ivec3(+1, -1, -1), ivec3(+1, -1, +0), ivec3(+1, -1, +1),
    ivec3(+1, +0, -1), ivec3(+1, +0, +0), ivec3(+1, +0, +1),
    ivec3(+1, +1, -1), ivec3(+1, +1, +0), ivec3(+1, +1, +1));
