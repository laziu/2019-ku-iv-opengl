#pragma once
#ifndef _UTIL_IMAGE_
#define _UTIL_IMAGE_
#include <memory>
#include <vector>

namespace util {
struct image {
    using sptr_t = std::shared_ptr<image>;

    int width   = 0;
    int height  = 0;
    int channel = 0;

    std::vector<unsigned char> bytes;

    static sptr_t fromFile(const char* filename);

private:
    image() = default;
};
}
#endif
