#pragma once
#ifndef _UTIL_THREAD_WORKER_
#define _UTIL_THREAD_WORKER_
#include <condition_variable>
#include <functional>
#include <mutex>
#include <thread>

namespace util {
class thread_worker {
    std::mutex              mutex;
    std::condition_variable cv;
    bool                    working_state = false;

    std::thread thread;
    bool        kill_state = false;

protected:
    virtual void onStart()  = 0;
    virtual void onFinish() = 0;
    virtual void onWork()   = 0;

    thread_worker()
    {
        thread = std::thread([&] {
            while (true) {
                std::unique_lock<std::mutex> lock(mutex);
                cv.wait(lock, [&] { return working_state || kill_state; });
                if (kill_state) return;

                onWork();

                working_state = false;
                lock.unlock();
                cv.notify_one();
            }
        });
    }

public:
    virtual ~thread_worker()
    {
        kill_state = true;
        cv.notify_one();
        thread.join();
    }

    void start()
    {
        onStart();
        {
            std::lock_guard<std::mutex> lock(mutex);
            working_state = true;
        }
        cv.notify_one();
    }

    void wait()
    {
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return !working_state; });
        }
        onFinish();
    }
};
}
#endif