#pragma once
#ifndef _UTIL_ITERABLE_
#define _UTIL_ITERABLE_
#include <iterator>
#include <stdexcept>

namespace util {
template <typename T>
class arraylike {
    const T *p_begin, *p_end;

public:
    arraylike(T* begin, T* end): p_begin(begin), p_end(end) {}
    arraylike(T* data, size_t size): p_begin(data), p_end(data + size) {}

    T* begin() { return p_begin; }
    T* end() { return p_end; }

    T& operator*() { return *p_begin; }
    T& operator[](size_t n) { return p_begin[n]; }
    T* data() { return p_begin; }

    size_t size() { return p_end - p_begin; }
};

template <typename T>
class range {
    T n_begin, n_end, n_step;

public:
    class iterator : public std::iterator<std::input_iterator_tag, T, T, const T*, T> {
        T n_current, n_end, n_step;

    public:
        iterator(T current, T end, T step): n_current(current), n_end(end), n_step(step) {}

        const T& operator*() const { return n_current; }

        iterator& operator++()
        {
            if (n_current != n_end)
                n_current += n_step;
            return *this;
        }

        iterator operator++(int)
        {
            iterator ret = *this;
            ++*this;
            return ret;
        }

        bool operator==(const iterator& it) const { return n_current == it.n_current; }
        bool operator!=(const iterator& it) const { return n_current != it.n_current; }
    };

    [[nodiscard]] iterator begin() const { return iterator(n_begin, n_end, n_step); }
    [[nodiscard]] iterator end() const { return iterator(n_end, n_end, n_step); }

    constexpr range(const T& begin, const T& end, const T& step = 1)
        : n_begin(begin), n_end(end), n_step(step)
    {
        if (step == 0)
            throw std::invalid_argument("step should not be 0");
        else if (step > 0 && n_begin > n_end)
            throw std::invalid_argument("begin > end but step > 0");
        else if (step < 0 && n_begin < n_end)
            throw std::invalid_argument("begin < end but step < 0");

        const int offset = n_step > 0 ? 1 : -1;
        n_end += (step - offset) - ((end - begin - offset) % step);
    }

    explicit constexpr range(const T& end): range(0, end, 1) {}
};
}
#endif
