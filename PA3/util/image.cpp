#include "image.hpp"
#include <cstdio>

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

namespace util {
image::sptr_t image::fromFile(const char* filename)
{
    auto img = sptr_t(new image);
    printf("opening image '%s' ... ", filename);
    if (auto raw = stbi_load(filename, &img->width, &img->height, &img->channel, 0)) {
        img->bytes = std::vector<unsigned char>(raw, raw + size_t(img->width) * size_t(img->height) * size_t(img->channel));
        stbi_image_free(raw);
        printf("done.\n");
        return img;
    }
    printf("failed.\n");
    fprintf(stderr, "image file %s cannot be opened.\n", filename);
    return nullptr;
}
}
