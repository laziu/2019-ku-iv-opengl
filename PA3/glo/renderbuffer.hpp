#pragma once
#include <algorithm>
#include <array>
#include <functional>
#include <memory>
#include <glad/glad.h>

#ifdef DEBUG_GL_OBJECTS
#include <cstdio>
#define DEBUG_GL_LOG(...) printf(__VA_ARGS__)
#else
#define DEBUG_GL_LOG(...)
#endif

namespace glo {
struct renderbuffer {
    using sptr_t = std::shared_ptr<renderbuffer>;

    const GLuint id;

    template <GLsizei N>
    static std::array<sptr_t, N> create()
    {
        std::array<GLuint, N> ids;
        std::array<sptr_t, N> bfs;

        glGenRenderbuffers(N, ids.data());
        std::transform(ids.begin(), ids.end(), bfs.begin(), [](auto id) {
            DEBUG_GL_LOG("[Debug] ++   renderbuffer/%d\n", id);
            return sptr_t(new renderbuffer(id));
        });
        return bfs;
    }

    static sptr_t create()
    {
        GLuint id;
        glGenRenderbuffers(1, &id);
        DEBUG_GL_LOG("[Debug] ++   renderbuffer/%d\n", id);
        return sptr_t(new renderbuffer(id));
    }

    ~renderbuffer()
    {
        DEBUG_GL_LOG("[Debug]   -- renderbuffer/%d\n", id);
        glDeleteRenderbuffers(1, &id);
    }

    struct binded {
        binded&& bufferStorage(GLenum internal_format, GLsizei width, GLsizei height)
        {
            glRenderbufferStorage(target, internal_format, width, height);
            return std::move(*this);
        }

        void unbind() const
        {
            glBindRenderbuffer(target, 0);
        }

    private:
        const renderbuffer& o;
        const GLenum        target;

        binded(renderbuffer& o, GLenum target): o(o), target(target) { glBindRenderbuffer(target, o.id); }

        friend renderbuffer;
    };

    binded bind(GLenum target) { return binded(*this, target); }

private:
    renderbuffer(GLuint id): id(id) {}
};
}

#undef DEBUG_GL_LOG
