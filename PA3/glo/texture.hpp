#pragma once
#include <array>
#include <memory>
#include <glad/glad.h>

#ifdef DEBUG_GL_OBJECTS
#include <cstdio>
#define DEBUG_GL_LOG(...) printf(__VA_ARGS__)
#else
#define DEBUG_GL_LOG(...)
#endif

namespace glo {
struct texture {
    using sptr_t = std::shared_ptr<texture>;

    const GLuint id;

    template <GLsizei N>
    static std::array<sptr_t, N> create()
    {
        std::array<GLuint, N> ids;
        std::array<sptr_t, N> txs;

        glGenTextures(N, ids.data());
        std::transform(ids.begin(), ids.end(), txs.begin(), [](auto id) {
            DEBUG_GL_LOG("[Debug] ++   texture/%d\n", id);
            return sptr_t(new texture(id));
        });
        return txs;
    }

    static sptr_t create()
    {
        GLuint id;
        glGenTextures(1, &id);
        DEBUG_GL_LOG("[Debug] ++   texture/%d\n", id);
        return sptr_t(new texture(id));
    }

    ~texture()
    {
        DEBUG_GL_LOG("[Debug]   -- texture/%d\n", id);
        glDeleteTextures(1, &id);
    }

    struct binded {
        binded&& image2D(GLint level, GLint internal_format, GLsizei width, GLsizei height,
                         GLint border, GLenum format, GLenum type, const void* pixels)
        {
            glTexImage2D(target, level, internal_format, width, height,
                         border, format, type, pixels);
            return std::move(*this);
        }

        binded&& image2DMultisample(GLsizei samples, GLenum internal_format,
                                    GLsizei width, GLsizei height, GLboolean fixed_sample_locations)
        {
            glTexImage2DMultisample(target, samples, internal_format,
                                    width, height, fixed_sample_locations);
            return std::move(*this);
        }

        binded&& storage3D(GLsizei levels, GLenum internal_format, GLsizei width, GLsizei height, GLsizei depth)
        {
            glTexStorage3D(target, levels, internal_format, width, height, depth);
            return std::move(*this);
        }

        binded&& subImage3D(GLint level, GLint x_offset, GLint y_offset, GLint z_offset,
                            GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void* pixels)
        {
            glTexSubImage3D(target, level, x_offset, y_offset, z_offset, width, height, depth, format, type, pixels);
            return std::move(*this);
        }

        binded&& parameteri(GLenum p_name, GLint param)
        {
            glTexParameteri(target, p_name, param);
            return std::move(*this);
        }

        void unbind() const { glBindTexture(target, 0); }

    private:
        const texture& o;
        const GLenum   target;
        binded(texture& o, GLenum target): o(o), target(target) { glBindTexture(target, o.id); }

        friend texture;
    };

    binded bind(GLenum target) { return binded(*this, target); }
    binded bind(GLenum tex_index, GLenum target)
    {
        glActiveTexture(tex_index);
        return bind(target);
    }

private:
    texture(GLuint id): id(id) {}
};
}

#undef DEBUG_GL_LOG
