#pragma once
#include <cstdio>
#include <functional>
#include <initializer_list>
#include <memory>
#include <string>
#include <unordered_map>
#include <glad/glad.h>

#ifdef DEBUG_GL_OBJECTS
#define DEBUG_GL_LOG(...) printf(__VA_ARGS__)
#else
#define DEBUG_GL_LOG(...)
#endif

namespace glo {
struct shader {
    using sptr_t = std::shared_ptr<shader>;

    const GLuint id;
    const size_t hash;

    static sptr_t create(GLenum type, std::initializer_list<const GLchar*> sources)
    {
        const auto hash = create_hash(type, sources);
        if (const auto find = instances.find(hash); find != instances.end())
            if (auto ptr = find->second.lock()) {
                DEBUG_GL_LOG("[Debug] LOAD shader/%d\n", ptr->id);
                return ptr;
            }

        auto inst       = sptr_t(new shader(type, sources));
        instances[hash] = std::weak_ptr(inst);
        return inst;
    }

    static sptr_t create(GLenum type, const GLchar* source)
    {
        return create(type, { source });
    }

    static void lock(std::initializer_list<sptr_t> shaders)
    {
        for (auto& shader : shaders)
            locks[shader->hash] = shader;
    }

    static void free()
    {
        instances.clear();
        locks.clear();
    }

    ~shader()
    {
        DEBUG_GL_LOG("[Debug]   -- shader/%d\n", id);
        glDeleteShader(id);
    }

private:
    static inline size_t create_hash(GLenum type, std::initializer_list<const GLchar*> sources)
    {
        auto hash = std::hash<GLenum>()(type);
        for (auto& source : sources)
            hash = (hash + 0x9e3779b9) ^ std::hash<const GLchar*>()(source);
        return hash;
    }

    shader(GLenum type, std::initializer_list<const GLchar*> sources)
        : id(glCreateShader(type)), hash(create_hash(type, sources))
    {
        std::vector<const GLchar*> s { sources };
        glShaderSource(id, static_cast<GLsizei>(s.size()), s.data(), nullptr);
        glCompileShader(id);

        GLint success;
        glGetShaderiv(id, GL_COMPILE_STATUS, &success);
        if (!success) {
            GLint log_size;
            glGetShaderiv(id, GL_INFO_LOG_LENGTH, &log_size);

            std::string log;
            log.resize(log_size);
            glGetShaderInfoLog(id, log_size, nullptr, log.data());

            fprintf(stderr, "shader compile error: %s\n", log.c_str());
        }

        DEBUG_GL_LOG("[Debug] ++   shader/%d\n", id);
    }

    shader(GLenum type, const GLchar* source): shader(type, { source }) {}

    static std::unordered_map<std::size_t, std::weak_ptr<shader>> instances;
    static std::unordered_map<std::size_t, sptr_t>                locks;
};
}

#undef DEBUG_GL_LOG
