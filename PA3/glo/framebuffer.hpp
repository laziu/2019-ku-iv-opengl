#pragma once
#include "renderbuffer.hpp"
#include "texture.hpp"
#include <cstdio>
#include <algorithm>
#include <array>
#include <functional>
#include <memory>
#include <glad/glad.h>

#ifdef DEBUG_GL_OBJECTS
#define DEBUG_GL_LOG(...) printf(__VA_ARGS__)
#else
#define DEBUG_GL_LOG(...)
#endif

namespace glo {
struct framebuffer {
    using sptr_t = std::shared_ptr<framebuffer>;

    const GLuint id;

    template <GLsizei N>
    static std::array<sptr_t, N> create()
    {
        std::array<GLuint, N> ids;
        std::array<sptr_t, N> fbs;

        glGenFramebuffers(N, ids.data());
        std::transform(ids.begin(), ids.end(), fbs.begin(), [](auto id) {
            DEBUG_GL_LOG("[Debug] ++   framebuffer/%d\n", id);
            return sptr_t(new framebuffer(id));
        });
        return fbs;
    }

    static sptr_t create()
    {
        GLuint id;
        glGenFramebuffers(1, &id);
        DEBUG_GL_LOG("[Debug] ++   framebuffer/%d\n", id);
        return sptr_t(new framebuffer(id));
    }

    ~framebuffer()
    {
        DEBUG_GL_LOG("[Debug]   -- framebuffer/%d\n", id);
        glDeleteFramebuffers(1, &id);
    }

    struct binded {
        binded&& texture2D(GLenum attachment, GLenum tex_target, texture::sptr_t& texture, GLuint level)
        {
            glFramebufferTexture2D(target, attachment, tex_target, texture->id, level);
            return std::move(*this);
        }

        binded&& renderbuffer(GLenum attachment, GLenum renderbuffer_target, renderbuffer::sptr_t& renderbuffer)
        {
            glFramebufferRenderbuffer(target, attachment, renderbuffer_target, renderbuffer->id);
            return std::move(*this);
        }

        binded&& checkStatus()
        {
            if (const auto status = glCheckFramebufferStatus(target); status != GL_FRAMEBUFFER_COMPLETE) {
                auto error = "???";
                switch (status) {
#define ERROR_CASE(C) \
    case C: error = #C; break
                    ERROR_CASE(GL_FRAMEBUFFER_UNDEFINED);
                    ERROR_CASE(GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT);
                    ERROR_CASE(GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT);
                    ERROR_CASE(GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER);
                    ERROR_CASE(GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER);
                    ERROR_CASE(GL_FRAMEBUFFER_UNSUPPORTED);
                    ERROR_CASE(GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE);
                    ERROR_CASE(GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS);
                    ERROR_CASE(0);
#undef ERROR_CASE
                }
                fprintf(stderr, "Error: glCheckFramebufferStatus(%d) returned %s\n", o.id, error);
            }
            return std::move(*this);
        }

        binded&& use(std::function<void()>& f)
        {
            f();
            return std::move(*this);
        }

        // do nothing but ignore warning C26444: unnamed instance
        void end() {}

        ~binded() { glBindFramebuffer(target, 0); }

        operator bool() const { return o.id; }

    private:
        const framebuffer& o;
        const GLenum       target;
        binded(framebuffer& o, GLenum target): o(o), target(target) { glBindFramebuffer(target, o.id); }

        friend framebuffer;
    };

    binded bind(GLenum target) { return binded(*this, target); }

private:
    framebuffer(GLuint id): id(id) {}

    std::vector<texture::sptr_t> textures;
};
}

#undef DEBUG_GL_LOG
