#pragma once
#include "shader.hpp"
#include <cstdio>
#include <initializer_list>
#include <memory>
#include <string>
#include <unordered_map>
#include <glad/glad.h>

#ifdef DEBUG_GL_OBJECTS
#define DEBUG_GL_LOG(...) printf(__VA_ARGS__)
#else
#define DEBUG_GL_LOG(...)
#endif

namespace glo {
struct program {
    using sptr_t = std::shared_ptr<program>;

    const GLuint id;
    const size_t hash;

    static sptr_t create(std::initializer_list<shader::sptr_t> shaders)
    {
        const auto hash = create_hash(shaders);
        if (const auto find = instances.find(hash); find != instances.end())
            if (auto ptr = find->second.lock()) {
                DEBUG_GL_LOG("[Debug] LOAD program/%d\n", ptr->id);
                return ptr;
            }

        auto inst       = sptr_t(new program(shaders));
        instances[hash] = std::weak_ptr(inst);
        return inst;
    }

    static void free() { instances.clear(); }

    ~program()
    {
        DEBUG_GL_LOG("[Debug]   -- program/%d\n", id);
        glDeleteProgram(id);
    }

    inline void use() const { glUseProgram(id); }

    template <typename T>
    struct uniform {
        GLuint id;

        uniform(GLuint id): id(id) {}
        uniform(): id(0) {}

        void setData(const T& data) {}
    };

    template <typename T>
    inline uniform<T> getUniform(const GLchar* const name)
    {
        if (auto loc = glGetUniformLocation(id, name); loc != -1)
            return uniform<T>(loc);
        fprintf(stderr, "glGetUniformLocation error: program/%d \"%s\" not found\n", id, name);
        return uniform<T>();
    }

private:
    static inline size_t create_hash(std::initializer_list<shader::sptr_t> shaders)
    {
        size_t hash = 0;
        for (auto& sh : shaders)
            hash = (hash * 31) ^ sh->hash;
        return hash;
    }

    program(std::initializer_list<shader::sptr_t> shaders)
        : id(glCreateProgram()), hash(create_hash(shaders))
    {
        for (auto& shader : shaders)
            glAttachShader(id, shader->id);
        glLinkProgram(id);

        GLint success;
        glGetProgramiv(id, GL_LINK_STATUS, &success);
        if (!success) {
            GLint log_size;
            glGetProgramiv(id, GL_INFO_LOG_LENGTH, &log_size);

            std::string log;
            log.resize(log_size);
            glGetProgramInfoLog(id, log_size, nullptr, log.data());

            fprintf(stderr, "glprogram compile error: %s\n", log.c_str());
        }

        DEBUG_GL_LOG("[Debug] ++   program/%d\n", id);
    }

    static std::unordered_map<std::size_t, std::weak_ptr<program>> instances;
};
}

#undef DEBUG_GL_LOG

#include "program.inl"
