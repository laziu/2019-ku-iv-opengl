#pragma once
#include "program.hpp"
#include <glm/mat2x2.hpp>
#include <glm/mat2x3.hpp>
#include <glm/mat2x4.hpp>
#include <glm/mat3x2.hpp>
#include <glm/mat3x3.hpp>
#include <glm/mat3x4.hpp>
#include <glm/mat4x2.hpp>
#include <glm/mat4x3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace glo {
#ifndef PROGRAM_UNIFORM_SET_IMPL
#define PROGRAM_UNIFORM_SET_IMPL(T, suffix)                             \
    template <> inline void program::uniform<T>::setData(const T& data) \
    {                                                                   \
        glUniform##suffix(id, data);                                    \
    }
PROGRAM_UNIFORM_SET_IMPL(GLfloat, 1f)
PROGRAM_UNIFORM_SET_IMPL(GLdouble, 1d)
PROGRAM_UNIFORM_SET_IMPL(GLint, 1i)
PROGRAM_UNIFORM_SET_IMPL(GLuint, 1ui)
#undef PROGRAM_UNIFORM_SET_IMPL
#define PROGRAM_UNIFORM_SET_IMPL(T, suffix)                             \
    template <> inline void program::uniform<T>::setData(const T& data) \
    {                                                                   \
        glUniform##suffix(id, 1, glm::value_ptr(data));                 \
    }
PROGRAM_UNIFORM_SET_IMPL(glm::vec2, 2fv)
PROGRAM_UNIFORM_SET_IMPL(glm::vec3, 3fv)
PROGRAM_UNIFORM_SET_IMPL(glm::vec4, 4fv)
PROGRAM_UNIFORM_SET_IMPL(glm::dvec2, 2dv)
PROGRAM_UNIFORM_SET_IMPL(glm::dvec3, 3dv)
PROGRAM_UNIFORM_SET_IMPL(glm::dvec4, 4dv)
PROGRAM_UNIFORM_SET_IMPL(glm::ivec2, 2iv)
PROGRAM_UNIFORM_SET_IMPL(glm::ivec3, 3iv)
PROGRAM_UNIFORM_SET_IMPL(glm::ivec4, 4iv)
PROGRAM_UNIFORM_SET_IMPL(glm::uvec2, 2uiv)
PROGRAM_UNIFORM_SET_IMPL(glm::uvec3, 3uiv)
PROGRAM_UNIFORM_SET_IMPL(glm::uvec4, 4uiv)
#undef PROGRAM_UNIFORM_SET_IMPL
#define PROGRAM_UNIFORM_SET_IMPL(T, suffix)                             \
    template <> inline void program::uniform<T>::setData(const T& data) \
    {                                                                   \
        glUniformMatrix##suffix(id, 1, GL_FALSE, glm::value_ptr(data)); \
    }
PROGRAM_UNIFORM_SET_IMPL(glm::mat2, 2fv)
PROGRAM_UNIFORM_SET_IMPL(glm::mat3, 3fv)
PROGRAM_UNIFORM_SET_IMPL(glm::mat4, 4fv)
PROGRAM_UNIFORM_SET_IMPL(glm::mat2x3, 2x3fv)
PROGRAM_UNIFORM_SET_IMPL(glm::mat3x2, 3x2fv)
PROGRAM_UNIFORM_SET_IMPL(glm::mat2x4, 2x4fv)
PROGRAM_UNIFORM_SET_IMPL(glm::mat4x2, 4x2fv)
PROGRAM_UNIFORM_SET_IMPL(glm::mat3x4, 3x4fv)
PROGRAM_UNIFORM_SET_IMPL(glm::mat4x3, 4x3fv)
PROGRAM_UNIFORM_SET_IMPL(glm::dmat2, 2dv)
PROGRAM_UNIFORM_SET_IMPL(glm::dmat3, 3dv)
PROGRAM_UNIFORM_SET_IMPL(glm::dmat4, 4dv)
PROGRAM_UNIFORM_SET_IMPL(glm::dmat2x3, 2x3dv)
PROGRAM_UNIFORM_SET_IMPL(glm::dmat3x2, 3x2dv)
PROGRAM_UNIFORM_SET_IMPL(glm::dmat2x4, 2x4dv)
PROGRAM_UNIFORM_SET_IMPL(glm::dmat4x2, 4x2dv)
PROGRAM_UNIFORM_SET_IMPL(glm::dmat3x4, 3x4dv)
PROGRAM_UNIFORM_SET_IMPL(glm::dmat4x3, 4x3dv)
#undef PROGRAM_UNIFORM_SET_IMPL
#endif
}
