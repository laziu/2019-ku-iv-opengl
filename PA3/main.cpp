﻿#include <Windows.h>
#include "viewer.hpp"
#include <cstdio>
#include <cstdlib>

constexpr int  width  = 1024;
constexpr int  height = 640;
constexpr auto title  = "PA #3 - Fluid Simulation";

// run as dGPU rather than iGPU if nvidia optimus or amd enduro
extern "C" {
_declspec(dllexport) DWORD NvOptimusEnablement                = 0x00000001;
_declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;
}

int main(int argc, char* argv[])
{
    try {
        glfwSetErrorCallback([](int error, const char* description) {
            fprintf(stderr, "GLFW error %d: %s\n", error, description);
        });

        if (!glfwInit()) throw "Failed to initialize GLFW.";

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

        auto* const window = glfwCreateWindow(width, height, title, nullptr, nullptr);
        if (!window) throw "Failed to open GLFW window.";
        glfwMakeContextCurrent(window);

        if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress)))
            throw "Failed to initialize GLAD.\n";

        printf("-------------------- Hardware Info --------------------\n");
        printf("%s / %s\n", glGetString(GL_VENDOR), glGetString(GL_RENDERER));
        printf("Context: GL %s (GLSL %s)\n", glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));

        int dim[3];
        for (int i = 0; i < 3; ++i)
            glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, i, &dim[i]);
        printf("Max Workgroup Count: [%d, %d, %d]\n", dim[0], dim[1], dim[2]);
        for (int i = 0; i < 3; ++i)
            glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, i, &dim[i]);
        printf("Max Workgroup Size:  [%d, %d, %d]\n", dim[0], dim[1], dim[2]);
        printf("-------------------------------------------------------\n");

        if (!GLAD_GL_VERSION_4_3) throw "OpenGL 4.3 is not supported.";
        if (!glMapNamedBuffer) throw "OpenGL 4.3 glMapNamedBuffer is not supported on this driver.";

        glfwSetFramebufferSizeCallback(window, viewer::resize);
        glfwSetMouseButtonCallback(window, viewer::mousepress);
        glfwSetCursorPosCallback(window, viewer::mousemove);
        glfwSetScrollCallback(window, viewer::mousescroll);
        glfwSetKeyCallback(window, viewer::keypress);

        // Event flow
        viewer::init();
        viewer::resize(window, width, height);

        while (!glfwWindowShouldClose(window)) {
            static auto last_time    = glfwGetTime();
            const auto  current_time = glfwGetTime();
            const auto  delta_time   = current_time - last_time;
            static auto fps_wait     = 0.0;

            if (delta_time >= viewer::time_step) {
                viewer::update(delta_time);
                viewer::render();
                glfwSwapBuffers(window);
                last_time = current_time;

                fps_wait += delta_time;
                if (fps_wait > 1.0) {
                    fps_wait = 0;
                    printf(" fps: %lf\r", 1.0 / delta_time);
                }
            }

            glfwPollEvents();
        }

        glfwTerminate();
    }
    catch (const char* s) {
        fprintf(stderr, "ERROR: %s\n", s);
        system("pause");
        exit(EXIT_FAILURE);
    }
    return 0;
}
