#pragma once
#include "vector.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <GL/freeglut.h>

struct obj {
    struct face {
        vec3 &v1, &v2, &v3;
        vec3  normal;

        face(vec3& v1, vec3& v2, vec3& v3): v1(v1), v2(v2), v3(v3) {}
    };

    std::vector<vec3> vertices;
    std::vector<face> faces;

    obj(const char* path, double scale) {
        std::ifstream file(path);
        if (!file) {
            std::cerr << "cannot open " << path << std::endl;
            return;
        }

        for (std::string line; std::getline(file, line);) {
            std::istringstream iss(line);
            if (std::string h; iss >> h) {
                if (h[0] == 'v') {
                    vec3 v;
                    iss >> v.x >> v.y >> v.z;
                    v *= scale;
                    vertices.push_back(v);
                }
                else if (h[0] == 'f') {
                    int idx[3];
                    iss >> idx[0] >> idx[1] >> idx[2];
                    face f { vertices[idx[0] - 1], vertices[idx[1] - 1], vertices[idx[2] - 1] };
                    f.normal = ((f.v1 - f.v2).normalized()).cross((f.v2 - f.v3).normalized());
                    faces.push_back(f);
                }
            }
        }
    }
};

struct obj_entity {
    obj mesh;

    vec3   position;
    double scale = 0.;

    obj_entity(const char* path, double scale): mesh(path, scale) {}

    void draw() {
        glPushMatrix();
        glBegin(GL_TRIANGLES);
        glColor3f(1.0, 1.0, 1.0);
        for (auto& f : mesh.faces) {
            glNormal3dv(&f.normal.x);
            glVertex3dv(&f.v1.x);
            glVertex3dv(&f.v2.x);
            glVertex3dv(&f.v3.x);
        }
        glEnd();
        glPopMatrix();
    }
};