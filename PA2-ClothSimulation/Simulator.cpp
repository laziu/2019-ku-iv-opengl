#include "Simulator.h"

Simulator::Simulator() {
    Initialize();
    Lighting();
}

void Simulator::Initialize() {
    timsStep = 0.01;
    ground   = vec3(0.0, -15, 0.0);

    cloth.structural_coef = 1500;
    cloth.shear_coef      = 50;
    cloth.bending_coef    = 1000;
    cloth.iteration_n     = 10;

    cloth.init();
}

void Simulator::Update() {
    vec3 gravity(0.0, -9.8 / cloth.iteration_n, 0.0);

    for (int iter = 0; iter < cloth.iteration_n; iter++) {
        // RK2 계산식을 위해 step을 0, 1로 나눠 힘을 두 번 계산한다.
        for (int i = 0; i < 2; ++i) {
            cloth.compute_force(timsStep, gravity); // mass_spring::internal_force 호출
            cloth.integrate(i, timsStep);           // Node::integrate 호출
        }
        // 충돌 계산한다.
        cloth.collision_response(ground);
        cloth.collision_with_sphere(sphere_position, sphere_radius);
    }

    cloth.computeNormal();
}

void Simulator::Render() {
    // Lighting();
    DrawGround();
    DrawSphere();
    cloth.draw();
}

void Simulator::Lighting() {
    glShadeModel(GL_SMOOTH);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);

    float light_pos[]      = { 150.f, 150.f, 0, 1.f };
    float light_dir[]      = { -1.f, -1.f, 0, 0 };
    float light_ambient[]  = { 0.3f, 0.3f, 0.3f, 1.0f };
    float light_diffuse[]  = { 0.8f, 0.8f, 0.8f, 1.0f };
    float light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    float frontColor[]     = { 0.8f, 0.8f, 0.8f, 1.0f };

    float matShininess = 20.f;
    float noMat[]      = { 0, 0, 0, 1.f };

    float matSpec[] = { 1.f, 1.f, 1.f, 1.f };
    glMaterialfv(GL_FRONT, GL_EMISSION, noMat);
    glMaterialfv(GL_FRONT, GL_SPECULAR, matSpec);
    glMaterialfv(GL_FRONT, GL_AMBIENT, frontColor); // Set ambient property frontColor.
    glMaterialfv(GL_FRONT, GL_DIFFUSE, frontColor); // Set diffuse property frontColor.
    glMaterialf(GL_FRONT, GL_SHININESS, matShininess);

    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 80.0f);   // 80도 원뿔
    glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, 80.0f); // 초점 설정
    glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
    glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, light_dir);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
}

void Simulator::DrawGround(void) {

    glBegin(GL_QUADS);
    glColor3f(1.0, 1.0, 1.0);

    for (int x = 0; x < 128; x++) {
        for (int y = 0; y < 128; y++) {
            glNormal3d(0.0, 1.0, 0.0);
            glVertex3d(-250. + 250. / 64 * (x + 0.), ground.y, -250. + 250. / 64 * (y + 0.));
            glVertex3d(-250. + 250. / 64 * (x + 1.), ground.y, -250. + 250. / 64 * (y + 0.));
            glVertex3d(-250. + 250. / 64 * (x + 1.), ground.y, -250. + 250. / 64 * (y + 1.));
            glVertex3d(-250. + 250. / 64 * (x + 0.), ground.y, -250. + 250. / 64 * (y + 1.));
        }
    }
    glEnd();
}

void Simulator::DrawSphere(void) {
    glPushMatrix();
    glTranslated(sphere_position.x, sphere_position.y, sphere_position.z);
    glutSolidSphere(sphere_radius, 20, 20);
    glPopMatrix();
}
