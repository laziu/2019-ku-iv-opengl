#pragma once
#include <cmath>
#include <iostream>

class vec2 {
public:
    double x, y;

    vec2(): x(0), y(0) {}
    vec2(double x0, double y0): x(x0), y(y0) {}

    vec2 operator+(vec2 v) { return vec2(x + v.x, y + v.y); }
    vec2 operator-(vec2 v) { return vec2(x - v.x, y - v.y); }
    vec2 operator*(double v) { return vec2(x * v, y * v); }
    vec2 operator/(double v) { return vec2(x / v, y / v); }

    vec2& operator=(const vec2& v) {
        x = v.x;
        y = v.y;
        return *this;
    }
    vec2& operator+=(vec2 v) {
        x += v.x;
        y += v.y;
        return *this;
    }
    vec2& operator-=(vec2 v) {
        x -= v.x;
        y -= v.y;
        return *this;
    }

    double length() { return sqrt(x * x + y * y); }
    double dist() { return sqrt(x * x + y * y); }
    double dist(vec2 p) { return sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y)); }
};

class vec3 {
public:
    double x, y, z;

    vec3(): x(0), y(0), z(0) {}
    vec3(double x, double y, double z): x(x), y(y), z(z) {}

    vec3 operator+() const { return *this; }
    vec3 operator-() const { return vec3(-x, -y, -z); }

    vec3 operator+(vec3 v) { return vec3(x + v.x, y + v.y, z + v.z); }
    vec3 operator-(vec3 v) { return vec3(x - v.x, y - v.y, z - v.z); }
    vec3 operator*(double v) { return vec3(x * v, y * v, z * v); }
    vec3 operator/(double v) { return vec3(x / v, y / v, z / v); }

    friend vec3 operator*(double val, vec3 v) { return vec3(v.x * val, v.y * val, v.z * val); }
    friend vec3 operator/(double val, vec3 v) { return vec3(v.x / val, v.y / val, v.z / val); }

    vec3& operator+=(double v) {
        x += v;
        y += v;
        z += v;
        return *this;
    }
    vec3& operator-=(double v) {
        x -= v;
        y -= v;
        z -= v;
        return *this;
    }
    vec3& operator*=(double v) {
        x *= v;
        y *= v;
        z *= v;
        return *this;
    }
    vec3& operator/=(double v) {
        x /= v;
        y /= v;
        z /= v;
        return *this;
    }
    vec3& operator+=(vec3 v) {
        x += v.x;
        y += v.y;
        z += v.z;
        return *this;
    }
    vec3& operator-=(vec3 v) {
        x -= v.x;
        y -= v.y;
        z -= v.z;
        return *this;
    }
    vec3& operator*=(vec3 v) {
        x *= v.x;
        y *= v.y;
        z *= v.z;
        return *this;
    }
    vec3& operator/=(vec3 v) {
        x /= v.x;
        y /= v.y;
        z /= v.y;
        return *this;
    }

    vec3   cross(vec3 v) { return vec3(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x); }
    double length() { return sqrt(x * x + y * y + z * z); }
    vec3   normalized() {
        double w = length();
        return vec3(x / w, y / w, z / w);
    }
    void Normalize() {
        double w = length();
        if (w < 0.00001)
            return;
        x /= w;
        y /= w;
        z /= w;
    }
    double dot(vec3 v) { return (x * v.x + y * v.y + z * v.z); }
    double dist(vec3 v) { return sqrt(pow(x - v.x, 2) + pow(y - v.y, 2) + pow(z - v.z, 2)); }
    bool   operator==(vec3& v) { return x == v.x && y == v.y && z == v.z; }
    bool   operator!=(vec3& v) { return x != v.x || y != v.y || z != v.z; }
    void   setZero() { x = y = z = 0.0f; }

    vec3 rotateX(double theta) {
        auto c = cos(theta);
        auto s = sin(theta);
        return vec3(x, y * c - z * s, y * s + z * c);
    }

    vec3 rotateY(double theta) {
        auto c = cos(theta);
        auto s = sin(theta);
        return vec3(z * s + x * c, y, z * c - x * s);
    }

    vec3 rotateZ(double theta) {
        auto c = cos(theta);
        auto s = sin(theta);
        return vec3(x * c - y * s, x * s + y * c, z);
    }

    friend std::ostream& operator<<(std::ostream& os, const vec3& v) {
        char str[100];
        sprintf(str, "(%8.3f,%8.3f,%8.3f)", v.x, v.y, v.z);
        os << str;
        return os;
    }
};

struct ivec2 {
    int x, y;

    ivec2(): x(0), y(0) {}
    ivec2(int x, int y): x(x), y(y) {}
};

struct ivec3 {
    int x, y, z;

    ivec3(): x(0), y(0), z(0) {}
    ivec3(int x, int y, int z): x(x), y(y), z(z) {}
};

struct bvec3 {
    bool x, y, z;

    bvec3(): x(false), y(false), z(false) {}
    bvec3(bool x, bool y, bool z): x(x), y(y), z(z) {}
};
