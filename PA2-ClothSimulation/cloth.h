#pragma once
#include "Node.h"
#include "spring.h"
#include <algorithm>
#include <execution>
#include <vector>

class mass_cloth {
public:
    enum DrawModeEnum { DRAW_MASS_NODES, DRAW_SPRINGS, DRAW_FACES };
    DrawModeEnum drawMode = DRAW_MASS_NODES;

    std::vector<Node>        nodes;
    std::vector<mass_spring> spring;
    std::vector<Node*>       faces;

    std::vector<ivec3> indices;

    ivec3 size = { 50, 1, 50 };
    vec3  d    = { 1, 1, 1 };

    double structural_coef = 1500;
    double shear_coef      = 50;
    double bending_coef    = 1000;

    int iteration_n = 20;

public:
    inline Node& getNode(int x, int y, int z) { return nodes[(x * size.y + y) * size.z + z]; }

    void init() {
        for (int x = 0; x < size.x; ++x)
            for (int y = 0; y < size.y; ++y)
                for (int z = 0; z < size.z; ++z)
                    indices.push_back({ x, y, z });

        // Basic Implements 1. Init Nodes and Shear and Structural Springs
        // x, y, z축 각각의 위치에 맞는 node를 생성한다.
        for (int x = 0; x < size.x; ++x)
            for (int y = 0; y < size.y; ++y)
                for (int z = 0; z < size.z; ++z)
                    nodes.push_back(vec3(d.x * (x - size.x / 2),
                                         d.y * (y - size.y / 2) + d.x * (size.x / 2),
                                         d.z * (z - size.z / 2)));

        // 가장 위 두 꼭지점에 있는 node를 고정시킨다.
        getNode(0, 0, 0).isFixed          = true;
        getNode(0, 0, size.z - 1).isFixed = true;

        // structural 스프링을 생성한다. dx, dy, dz로 각 방향으로의 스프링 생성을 하나의 for문으로
        // 처리하였다.
        for (auto [dx, dy, dz] : std::vector<ivec3> { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } })
            for (int x = dx; x < size.x; ++x)
                for (int y = dy; y < size.y; ++y)
                    for (int z = dz; z < size.z; ++z) {
                        mass_spring sp = { getNode(x, y, z), getNode(x - dx, y - dy, z - dz) };
                        sp.spring_coef = structural_coef;
                        spring.push_back(sp);
                    }

        // shear 스프링을 생성한다. 각 평면에 존재하는 두개의 스프링을 for문을 돌면서 처리하였다.
        for (auto [dx, dy, dz] : std::vector<ivec3> { { 1, 1, 0 }, { 1, 0, 1 }, { 0, 1, 1 } })
            for (int x = dx; x < size.x; ++x)
                for (int y = dy; y < size.y; ++y)
                    for (int z = dz; z < size.z; ++z) {
                        mass_spring sp[2] = { { getNode(x, y, z), getNode(x - dx, y - dy, z - dz) },
                                              { getNode(x - dx * dy, y - dy * dz, z - dz * dx),
                                                getNode(x - dx * dz, y - dy * dx, z - dz * dy) } };
                        sp[0].spring_coef = sp[1].spring_coef = shear_coef;
                        spring.push_back(sp[0]);
                        spring.push_back(sp[1]);
                    }

        // Additional Implements 1. Init Bending Spring
        // bending 스프링을 생성한다. structural과 유사하게 처리하였다.
        for (auto [dx, dy, dz] : std::vector<ivec3> { { 2, 0, 0 }, { 0, 2, 0 }, { 0, 0, 2 } })
            for (int x = dx; x < size.x; ++x)
                for (int y = dy; y < size.y; ++y)
                    for (int z = dz; z < size.z; ++z) {
                        mass_spring sp = { getNode(x, y, z), getNode(x - dx, y - dy, z - dz) };
                        sp.spring_coef = bending_coef;
                        spring.push_back(sp);
                    }

        // Basic Implements 3-1. Generate Faces
        // 한 평면을 구성하는 4개의 node를 faces에 push한다.
        for (int x = 0; x < size.x; ++x)
            for (int y = 0; y < size.y; ++y)
                for (int z = 0; z < size.z; ++z) {
                    // 두께가 1 이상일 경우 육면체 외각 평면을 모두 구한다.
                    if ((x == 0 || x == size.x - 1) && y != 0 && z != 0) { // yz 평면
                        faces.push_back(&getNode(x, y - 1, z - 1));
                        faces.push_back(&getNode(x, y - 1, z - 0));
                        faces.push_back(&getNode(x, y - 0, z - 0));
                        faces.push_back(&getNode(x, y - 0, z - 1));
                    }
                    if ((y == 0 || y == size.y - 1) && z != 0 && x != 0) { // zx 평면
                        faces.push_back(&getNode(x - 1, y, z - 1));
                        faces.push_back(&getNode(x - 0, y, z - 1));
                        faces.push_back(&getNode(x - 0, y, z - 0));
                        faces.push_back(&getNode(x - 1, y, z - 0));
                    }
                    if ((z == 0 || z == size.z - 1) && x != 0 && y != 0) { // xy 평면
                        faces.push_back(&getNode(x - 1, y - 1, z));
                        faces.push_back(&getNode(x - 1, y - 0, z));
                        faces.push_back(&getNode(x - 0, y - 0, z));
                        faces.push_back(&getNode(x - 0, y - 1, z));
                    }
                }

        // Additional Implements 4-2. Initialize Texture Coordinates
        // 텍스쳐의 좌표는 [0, 1] 사이이므로 정점의 위치에 맞게 계산하여 넣어준다.
        for (int x = 0; x < size.x; ++x)
            for (int y = 0; y < size.y; ++y)
                for (int z = 0; z < size.z; ++z)
                    getNode(x, y, z).texCoord =
                            vec2(1. - (z / (size.z - 1.)), 1. - (x / (size.x - 1.)));
    }

    void computeNormal() {
        // Basic Implements 3-2. Compute Vertex Normal
        // 각 node마다 인접한 2개의 face normal 평균을 node의 normal로 지정한다.
        std::for_each(std::execution::par, indices.begin(), indices.end(), [&](ivec3 i) {
            // 두께가 1 이상일 경우 각 face의 방향에 맞춰 normal을 계산한다.
            std::vector<ivec3> dir;
            if (i.y == size.y - 1)
                dir = std::vector<ivec3> { { 1, 0, 0 }, { 0, 0, -1 }, { -1, 0, 0 }, { 0, 0, 1 } };
            else if (i.y == 0)
                dir = std::vector<ivec3> { { 1, 0, 0 }, { 0, 0, 1 }, { -1, 0, 0 }, { 0, 0, -1 } };
            else if (i.z == size.z - 1)
                dir = std::vector<ivec3> { { 0, 1, 0 }, { -1, 0, 0 }, { 0, -1, 0 }, { 1, 0, 0 } };
            else if (i.z == 0)
                dir = std::vector<ivec3> { { 0, 1, 0 }, { 1, 0, 0 }, { 0, -1, 0 }, { -1, 0, 0 } };
            else if (i.x == size.x - 1)
                dir = std::vector<ivec3> { { 0, 0, 1 }, { 0, -1, 0 }, { 0, 0, -1 }, { 0, 1, 0 } };
            else if (i.x == 0)
                dir = std::vector<ivec3> { { 0, 0, 1 }, { 0, 1, 0 }, { 0, 0, -1 }, { 0, -1, 0 } };
            else
                return; // 외각이 아닌 경우 face가 안보이므로 skip한다.

            auto& node = getNode(i.x, i.y, i.z);

            // 각 node에 인접한 4개의 face의 normal은 node와 structural spring으로 연결된
            // 4개의 정점으로 향하는 방향 벡터를 2개씩 외적한 것과 같다.
            vec3 lines[4];
            std::transform(dir.begin(), dir.end(), std::begin(lines), [&](ivec3 d) {
                // node가 모서리에 위치한 경우 인접한 node가 없는 방향벡터는 0이 되어 결과에 영향이
                // 없다.
                auto n = ivec3(std::min(size.x - 1, std::max(0, i.x + d.x)),
                               std::min(size.y - 1, std::max(0, i.y + d.y)),
                               std::min(size.z - 1, std::max(0, i.z + d.z)));
                return getNode(n.x, n.y, n.z).position - node.position;
            });
            node.normal = lines[0].cross(lines[1]) + lines[2].cross(lines[3]);
            node.normal.Normalize();
        });
    }

    void add_force(vec3 additional_force) {
        std::for_each(std::execution::par, nodes.begin(), nodes.end(), [&](Node& node) { // parallel
            node.add_force(additional_force);
        });
    }

    void compute_force(double dt, vec3 gravity) {
        std::for_each(std::execution::par, nodes.begin(), nodes.end(), [&](Node& node) { // parallel
            node.add_force(gravity * node.mass);
        });
        /* Compute Force for all springs */
        std::for_each(std::execution::par, spring.begin(), spring.end(), [&](mass_spring& sp) { //
            sp.internal_force(dt);
        });
    }

    void integrate(int step, double dt) {
        /* integrate Nodes*/
        std::for_each(std::execution::par, nodes.begin(), nodes.end(), [&](Node& node) { // parallel
            node.integrate(step, dt);
        });
    }

    void collision_response(vec3 ground) {
        // Basic Implements 4. Collision Check with ground
        // 바닥이 xz평면이므로 각 node의 y축 position과 velocity를 보고 충돌 판단 시 반사시킨다.
        std::for_each(std::execution::par, nodes.begin(), nodes.end(), [&](Node& node) {
            if (node.position.y <= ground.y && node.velocity.y < 0) { // ground.xy is pointless
                node.velocity.y = -node.velocity.y;
                node.position.y = ground.y + 5e-3;
            }
        });
    }

    void collision_with_sphere(vec3 center, float radius) {
        // Additional Implements 2. Collision Check with Sphere
        // 구의 중심과 각 node 사이의 거리를 보고 충돌 판단 시 구의 표면을 기준으로 반사시킨다.
        std::for_each(std::execution::par, nodes.begin(), nodes.end(), [&](Node& node) {
            auto diff = node.position - center;
            if (diff.length() < radius) {
                diff.Normalize();
                node.velocity -= 2. * diff.dot(node.velocity) * diff;
                node.position = center + radius * diff;
            }
        });
    }

    void draw();
};
