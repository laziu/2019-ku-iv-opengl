#pragma once
#include "Viewer.h"
#include <fstream>
#include <vector>

constexpr auto M_PI = 3.14159265358979323846;

using str_iterator = std::istreambuf_iterator<char>;

void Viewer::Initialize(void) {
    S_Simulator.reset(new Simulator);

    // Additional Implements 4-1. OpenGL Initialization for Texture Mapping
    glEnable(GL_TEXTURE_2D);

    tex = texture::create();
    glBindTexture(GL_TEXTURE_2D, tex->id);

    // bmp 파일을 열어서 header에서 크기를 읽고, 픽셀 데이터를 텍스처 데이터로 복사한다.
    if (auto tex_file = std::ifstream("resources/texture.bmp", std::ios_base::binary)) {
        std::vector<char> data((str_iterator(tex_file)), str_iterator());
        auto [w, h] = std::tuple(*(int*)&data[18], *(int*)&data[22]);
        data.erase(data.begin(), data.begin() + 54);

        // bmp 파일은 BGR이고, OpenGL은 RGB 텍스처를 사용하므로 위치를 맞춰준다.
        for (size_t i = 0; i < 3 * w * h; i += 3)
            std::swap(data[i], data[i + 2]);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, data.data());
    }
    else
        std::cerr << "texture file not exists." << std::endl;

    // 텍스처 옵션을 정해준다.
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // texCoord < 0일때 전체 반복
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // texCoord > 1일때 전체 반복
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); // texel < pixel일때 동작
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); // texel > pixel일때 동작
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE); // texture와 color를 곱해 값 적용

    glDisable(GL_TEXTURE_2D);
}

void Viewer::Render(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslated(0.0, 0.0, -m_Zoom);

    glTranslated(m_Translate.x, m_Translate.y, 0.0);
    glPushMatrix();
    glRotated(m_Rotate.x, 1.0, 0.0, 0.0);
    glRotated(m_Rotate.y, 0.0, 1.0, 0.0);
    S_Simulator->Render();
    glPopMatrix();
    float light_pos[] = { 150.f, 150.f, 0.f, 1.f };
    glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
    glutSwapBuffers();
    glutPostRedisplay();
}

void Viewer::Reshape(int w, int h) {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45., (double)w / h, 0.1, 500);
    glViewport(0, 0, w, h);
    window_size = ivec2(w, h);
}

void Viewer::Mouse(int mouse_event, int state, int x, int y) {
    m_Mouse_Coord.x = x;
    m_Mouse_Coord.y = y;
    switch (mouse_event) {
    case GLUT_LEFT_BUTTON:
    case GLUT_MIDDLE_BUTTON:
    case GLUT_RIGHT_BUTTON: m_Mouse_Event[mouse_event] = (GLUT_DOWN == state); break;
    default: break;
    }
    glutPostRedisplay();
}

void Viewer::Motion(int x, int y) {
    int diffx = x - m_Mouse_Coord.x;
    int diffy = y - m_Mouse_Coord.y;

    m_Mouse_Coord.x = x;
    m_Mouse_Coord.y = y;

    // 카메라의 위치에 맞춰 힘을 가하는 방향을 맞춰준다.
    auto mouse_ray = vec3(diffx, -diffy, 0)
                             .rotateX(-m_Rotate.x * (M_PI / 180.))
                             .rotateY(-m_Rotate.y * (M_PI / 180.));

    if (m_Mouse_Event[GLUT_LEFT_BUTTON]) {
        if (!interationMode) {
            m_Rotate.x += 0.1 * diffy;
            m_Rotate.y += 0.1 * diffx;
        }
        else {
            // Basic Implements 5. User Interaction
            // node 전체에 힘을 가한다.
            S_Simulator->cloth.add_force(2. * mouse_ray);
        }
    }
    else if (m_Mouse_Event[GLUT_MIDDLE_BUTTON]) {
        m_Translate.x += 0.1 * diffx;
        m_Translate.y -= 0.1 * diffy;
    }
    else if (m_Mouse_Event[GLUT_RIGHT_BUTTON]) {
        if (!interationMode)
            m_Zoom += 0.1 * diffy;
        else
            S_Simulator->sphere_position += .001 * m_Zoom * mouse_ray;
    }
    glutPostRedisplay();
}

void Viewer::Keyboard(unsigned char key, int x, int y) {
    switch (key) {
    case '1': S_Simulator->cloth.drawMode = mass_cloth::DRAW_MASS_NODES; break;
    case '2': S_Simulator->cloth.drawMode = mass_cloth::DRAW_SPRINGS; break;
    case '3': S_Simulator->cloth.drawMode = mass_cloth::DRAW_FACES; break;
    case 27:
    case 'q':
    case 'Q': exit(0); break;
    case ' ': m_start = !m_start; break;
    case 'r':
    case 'R': {
        auto prevMode = S_Simulator->cloth.drawMode;
        S_Simulator.reset(new Simulator);
        S_Simulator->cloth.drawMode = prevMode;
    } break;
    case 'f':
    case 'F': interationMode = !interationMode; break;
    }
    glutPostRedisplay();
}

void Viewer::Update() {
    if (m_start == true) {
        S_Simulator->Update();
    }
}
