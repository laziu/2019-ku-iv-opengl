#pragma once
#include "cloth.h"
#include <GL/freeglut.h>

class Simulator {
public:
    Simulator();

    void Initialize(void);
    void Update();
    void Render();
    void Lighting(void);
    void DrawGround(void);
    void DrawSphere(void);

public:
    mass_cloth cloth;
    vec3       ground;
    double     timsStep;

    // sphere의 크기와 위치
    vec3  sphere_position;
    float sphere_radius = 10.f;
};
