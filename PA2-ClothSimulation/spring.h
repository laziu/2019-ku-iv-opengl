#pragma once
#include "Node.h"
#include <memory>

class mass_spring {
public:
    double spring_coef;
    double damping_coef = 5.;
    Node&  p1;
    Node&  p2;
    double initial_length;

public:
    mass_spring(Node& p1, Node& p2): p1(p1), p2(p2) { init(); }

    void init() {
        vec3 S_length  = (p2.position - p1.position);
        initial_length = S_length.length();
    }

    void internal_force(double dt) {
        // Basic Implements 2-1. Compute Spring Force
        // Hooke의 법칙을 스프링마다 적용하고, 진동을 줄이기 위해 관성 적용
        vec3   direction = p2.position - p1.position;
        double length    = direction.length();
        direction.Normalize();

        double spring_force  = spring_coef * (length - initial_length);
        double damping_force = damping_coef * (p2.velocity - p1.velocity).dot(direction);
        vec3   force         = (spring_force + damping_force) * direction;

        // add hook_force and damping force
        p1.add_force(+force);
        p2.add_force(-force);
    }
    void draw();
};
