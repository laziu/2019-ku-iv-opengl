#pragma once
#include <memory>
#include <GL/freeglut.h>

// texture를 memory safe하게 관리하는 구조체
struct texture {
    using sptr_t = std::shared_ptr<texture>;

    const GLuint id;

    static sptr_t create() {
        GLuint id;
        glGenTextures(1, &id);
        return sptr_t(new texture(id));
    }

    ~texture() { glDeleteTextures(1, &id); }

private:
    texture(GLuint id): id(id) {}
};
