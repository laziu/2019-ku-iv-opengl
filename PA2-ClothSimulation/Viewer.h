#pragma once
#include "Simulator.h"
#include "texture.h"
#include <memory>

class Viewer {
public:
    std::unique_ptr<Simulator> S_Simulator;

public:
    double m_Zoom = 120.f;        // view zoom
    vec2   m_Rotate;              // x,y
    vec2   m_Translate;           // x,y
    ivec2  m_Mouse_Coord;         // previous mouse coordinates
    bool   m_Mouse_Event[3] = {}; // mouse event handler
    bool   m_start          = false;
    bool   interationMode   = false;
    ivec2  window_size;

    texture::sptr_t tex;

public:
    void Initialize(void);
    void Render(void);
    void Reshape(int w, int h);
    void Mouse(int mouse_event, int state, int x, int y);
    void Motion(int x, int y);
    void Keyboard(unsigned char key, int x, int y);
    void Update();
};
