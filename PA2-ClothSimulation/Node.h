#pragma once
#include "vector.h"

class Node {
public:
    double mass = 1.;
    vec3   force;
    vec3   position;
    vec3   velocity;
    vec3   acceleration;
    vec3   normal;
    bool   isFixed = false;
    vec2   texCoord;

public:
    Node(vec3 init_pos): position(init_pos) {}

    void add_force(vec3 additional_force) { force += additional_force; }

    vec3 ppos, pvel, pacc[2];
    void integrate(int step, double dt) {
        if (!isFixed) {
            // Basic Implements 2-2. Integration
            // Additional Implements 5. Numerical Method Integration
            // step 0과 1로 나눠 RK2 계산식을 수행한다.
            // 원래의 RK2 수식을 적용 시 시뮬레이션이 정상 동작하지 않아
            // position은 velocity에 종속적이도록 간소화했다.
            switch (step) {
            case 0:
                ppos = position;
                pvel = velocity;

                pacc[step] = force / mass;
                velocity   = pvel + dt * pacc[step];
                break;
            case 1:
                pacc[step] = force / mass;
                velocity   = pvel + dt * (pacc[0] + pacc[1]) / 2.;
                break;
            }
            position = ppos + dt * velocity;
        }
        // initialize Force
        force.setZero();
    }

    void draw();
};
