#include "Node.h"
#include "cloth.h"
#include <GL/freeglut.h>

void Node::draw() {
    glDisable(GL_LIGHTING);
    glColor3d(0.97, 0.95, 0.15);
    glPointSize(2.0f);

    glBegin(GL_POINTS);
    glVertex3d(position.x, position.y, position.z);
    glEnd();
    glEnable(GL_LIGHTING);
}

void mass_spring::draw() {
    glDisable(GL_LIGHTING);
    glColor3d(1.0, 1.0, 1.0);
    glLineWidth(2.0f);

    glBegin(GL_LINES);
    glVertex3d(p1.position.x, p1.position.y, p1.position.z);
    glVertex3d(p2.position.x, p2.position.y, p2.position.z);
    glEnd();
    glEnable(GL_LIGHTING);
}

void mass_cloth::draw() {
    switch (drawMode) {
    case DRAW_MASS_NODES:
        glDisable(GL_LIGHTING);
        for (int i = 0; i < nodes.size(); i++)
            nodes[i].draw();
        glEnable(GL_LIGHTING);
        break;
    case DRAW_SPRINGS:
        glDisable(GL_LIGHTING);
        for (int i = 0; i < spring.size(); i++)
            spring[i].draw();
        glEnable(GL_LIGHTING);
        break;
    case DRAW_FACES:
        // Basic Implements 3-3. Draw Call for Cloth
        // Additional Implements 4-3. Texture Coordinate Mapping
        // 정점 4개를 QUAD로 그린다.
        // 각 node의 normal, texture coordinate, position을 opengl에 전달한다.
        glColor3d(1.0, 1.0, 1.0);
        glEnable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
        for (auto& node : faces) {
            glNormal3dv(&node->normal.x);
            glTexCoord2dv(&node->texCoord.x);
            glVertex3dv(&node->position.x);
        }
        glEnd();
        glDisable(GL_TEXTURE_2D);
        break;
    default: break;
    }
    glPopMatrix();
}
