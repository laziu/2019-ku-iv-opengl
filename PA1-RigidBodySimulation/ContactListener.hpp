#pragma once
#include <functional>
#include <vector>
#include <Box2D/Dynamics/b2Body.h>
#include <Box2D/Dynamics/b2World.h>
#include <Box2D/Dynamics/Contacts/b2Contact.h>

struct ContactListener : public b2ContactListener {
    std::vector<std::function<void()>> resetCallback;

    std::vector<std::function<void(b2Contact*)>> beginContactCallback;
    std::vector<std::function<void(b2Contact*)>> endContactCallback;

    std::vector<std::function<void(b2Contact*, const b2Manifold*)>>       preSolveCallback;
    std::vector<std::function<void(b2Contact*, const b2ContactImpulse*)>> postSolveCallback;

    void BeginContact(b2Contact* contact) override {
        for (auto& f : beginContactCallback)
            f(contact);
    }

    void EndContact(b2Contact* contact) override {
        for (auto& f : endContactCallback)
            f(contact);
    }

    void PreSolve(b2Contact* contact, const b2Manifold* oldManifold) override {
        for (auto& f : preSolveCallback)
            f(contact, oldManifold);
    }

    void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse) override {
        for (auto& f : postSolveCallback)
            f(contact, impulse);
    }
};
