﻿#include <cstdio>
#include <memory>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "WindowManager.hpp"
#include "scene/Scene.hpp"

constexpr int  width  = 1024;
constexpr int  height = 640;
constexpr auto title  = "PA #1 - Rigid Body Simulation";

static std::unique_ptr<Scene> scene;

void print_key_info(GLFWwindow* window) {
    auto input = WindowManager::input();
    printf("\rkey input: ");
    printf("%c ", input.W ? 'W' : ' ');
    printf("%c ", input.S ? 'S' : ' ');
    printf("%c ", input.A ? 'A' : ' ');
    printf("%c ", input.D ? 'D' : ' ');
    printf("%c ", input.F ? 'F' : ' ');
    printf("/ ");
    printf("%c ", glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS ? 'R' : ' ');
}

int main(int argc, char* argv[]) {
    glfwSetErrorCallback([](int error, const char* description) {
        fprintf(stderr, "GLFW error %d: %s\n", error, description);
    });

    try {
        if (!glfwInit())
            throw "Failed to initialize GLFW\n";

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

        glfwWindowHint(GLFW_SAMPLES, 4);

        auto* const window = glfwCreateWindow(width, height, title, nullptr, nullptr);
        if (!window)
            throw "Failed to open GLFW window.\n";

        glfwMakeContextCurrent(window);

        glfwSetFramebufferSizeCallback(window, [](auto window, int width, int height) {
            scene->resize(window, width, height);
            scene->pause = true;
        });

        glfwSetWindowPosCallback(window, [](auto window, int x, int y) { scene->pause = true; });

        glfwSetKeyCallback(window, [](auto window, int key, int scancode, int action, int mods) {
            if (action == GLFW_PRESS) {
                static int level = 0;
                switch (key) {
                case GLFW_KEY_R:
                    scene.reset(new Scene);
                    scene->moveStage(level);
                    break;
                case GLFW_KEY_1: scene->moveStage(level = 0); break;
                case GLFW_KEY_2: scene->moveStage(level = 1); break;
                case GLFW_KEY_3: scene->moveStage(level = 2); break;
                case GLFW_KEY_4: scene->moveStage(level = 3); break;
                case GLFW_KEY_5: scene->moveStage(level = 4); break;
                case GLFW_KEY_6: scene->moveStage(level = 5); break;
                case GLFW_KEY_7: scene->moveStage(level = 6); break;
                }
            }
        });

        glfwSwapInterval(1); // v-sync

        if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
            throw "Failed to initialize GLAD.\n";

        printf("-------------------- Hardware Info --------------------\n");
        printf("%s / %s\n", glGetString(GL_VENDOR), glGetString(GL_RENDERER));
        printf("Context: GL %s (GLSL %s)\n", glGetString(GL_VERSION),
               glGetString(GL_SHADING_LANGUAGE_VERSION));
        printf("-------------------------------------------------------\n");

        if (!GLAD_GL_VERSION_1_5)
            throw "OpenGL 1.5 is not supported.\n";
        if (!GLAD_GL_VERSION_3_3)
            throw "OpenGL 3.3 is not supported.\n";

        glEnable(GL_LINE_SMOOTH);
        glEnable(GL_BLEND);
        glEnable(GL_DEPTH_TEST);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        // Event flow
        WindowManager::initialize(window);
        scene = std::make_unique<Scene>();
        scene->resize(window, width, height);

        while (!glfwWindowShouldClose(window)) {
            static double lastTime = 0;
            double        currentTime, deltaTime;

            currentTime = glfwGetTime();
            deltaTime   = currentTime - lastTime;
            lastTime    = currentTime;

            WindowManager::update(window);
            scene->update(window, currentTime, deltaTime);
            scene->render(window, currentTime, deltaTime);

            // print_key_info(window);

            glfwPollEvents();
        }

        glfwTerminate();
    }
    catch (const char* s) {
        fprintf(stderr, s);
        system("pause");
        exit(EXIT_FAILURE);
    }
    return 0;
}

GLFWwindow*                WindowManager::window = nullptr;
WindowManager::InputStatus WindowManager::stat   = {};
WindowManager::SizeStatus  WindowManager::sz     = { width, height };
