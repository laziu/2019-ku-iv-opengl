#pragma once
#include "IDrawable.hpp"
#include "R.h"
#include "data.hpp"
#include <iterator>
#include <Box2D/Box2D.h>

class StaticBox : public IDrawable {
    std::shared_ptr<glo::program>      program;
    std::shared_ptr<glo::vertex_array> vao;

    glo::program::uniform<glm::mat4> u_pMat;
    glo::program::uniform<glm::vec4> u_color;

    glm::mat4& pMat;

public:
    StaticBox(glm::mat4& pMat, b2World& world): pMat(pMat) {
        auto vshader = glo::shader::create(GL_VERTEX_SHADER, R::shader::box_vert);
        auto gshader = glo::shader::create(GL_GEOMETRY_SHADER, R::shader::box_geom);
        auto fshader = glo::shader::create(GL_FRAGMENT_SHADER, R::shader::default_frag);
        glo::shader::lock({ vshader, gshader, fshader });

        program = glo::program::create({ vshader, gshader, fshader });

        u_pMat  = program->getUniform<glm::mat4>("p_mat");
        u_color = program->getUniform<glm::vec4>("color");

        vao = glo::vertex_array::create();
        vao->bind();

        auto buffer = vao->hold(glo::buffer::create());

        buffer->bind(GL_ARRAY_BUFFER)
                .bufferData(sizeof(R::map::static_box), R::map::static_box, GL_STATIC_DRAW)
                .vertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(R::map::static_box[0]), 0)
                .vertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(R::map::static_box[0]),
                                     (void*)sizeof(b2Vec2));

        b2BodyDef bodyDef;
        auto      body = world.CreateBody(&bodyDef);

        for (auto [scale, position] : R::map::static_box) {
            b2PolygonShape shape;
            shape.SetAsBox(scale.x / 2, scale.y / 2, position, 0);
            body->CreateFixture(&shape, 1.f);
        }
    }

    void draw() override {
        program->use();
        u_pMat.setData(pMat);
        u_color.setData(R::map::color.field);

        vao->bind().drawArrays(GL_POINTS, 0, (GLsizei)std::size(R::map::static_box));
    }
};
