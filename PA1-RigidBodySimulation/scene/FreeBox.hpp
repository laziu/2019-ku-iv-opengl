#pragma once
#include "IDrawable.hpp"
#include "R.h"
#include "data.hpp"
#include <algorithm>
#include <iterator>
#include <tuple>
#include <vector>
#include <Box2D/Box2D.h>

class FreeBox : public IDrawable {
    std::shared_ptr<glo::program>      program;
    std::shared_ptr<glo::vertex_array> vao;
    std::shared_ptr<glo::buffer>       b_position, b_angle;

    glo::program::uniform<glm::mat4> u_pMat;
    glo::program::uniform<glm::vec4> u_color;

    glm::mat4& pMat;

    std::vector<b2Body*> body;

public:
    FreeBox(glm::mat4& pMat, b2World& world): pMat(pMat) {
        using R::map::free_box;

        auto vshader = glo::shader::create(GL_VERTEX_SHADER, R::shader::box_vert);
        auto gshader = glo::shader::create(GL_GEOMETRY_SHADER, R::shader::box_geom);
        auto fshader = glo::shader::create(GL_FRAGMENT_SHADER, R::shader::default_frag);
        glo::shader::lock({ vshader, gshader, fshader });

        program = glo::program::create({ vshader, gshader, fshader });

        u_pMat  = program->getUniform<glm::mat4>("p_mat");
        u_color = program->getUniform<glm::vec4>("color");

        vao = glo::vertex_array::create();
        vao->bind();

        vao->hold(glo::buffer::create())
                ->bind(GL_ARRAY_BUFFER)
                .bufferData(sizeof(free_box), free_box, GL_STATIC_DRAW)
                .vertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(free_box[0]),
                                     (void*)(sizeof(float) * 2 + sizeof(b2Vec2)));

        auto buffers = vao->hold(glo::buffer::create<2>());

        (b_position = buffers[0])
                ->bind(GL_ARRAY_BUFFER)
                .bufferData(std::size(free_box) * sizeof(b2Vec2), nullptr, GL_STREAM_DRAW)
                .vertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

        (b_angle = buffers[1])
                ->bind(GL_ARRAY_BUFFER)
                .bufferData(std::size(free_box) * sizeof(float), nullptr, GL_STREAM_DRAW)
                .vertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 0, 0);

        for (auto [scale, position, angle, density] : free_box) {
            b2PolygonShape shape;
            shape.SetAsBox(scale.x / 2, scale.y / 2);

            b2BodyDef bodyDef;
            bodyDef.type           = b2_dynamicBody;
            bodyDef.position       = position;
            bodyDef.angle          = angle;
            bodyDef.linearDamping  = .99f;
            bodyDef.angularDamping = .99999f;

            b2FixtureDef fixDef;
            fixDef.shape    = &shape;
            fixDef.density  = density;
            fixDef.friction = .75f;

            auto boxBody = world.CreateBody(&bodyDef);
            boxBody->CreateFixture(&fixDef);
            body.push_back(boxBody);
        }
    }

    void update() override {
        b_position->bind(GL_ARRAY_BUFFER).mapBuffer<b2Vec2>(GL_WRITE_ONLY, [&](auto buffer) {
            std::transform(body.begin(), body.end(), buffer,
                           [](auto& bd) { return bd->GetPosition(); });
        });

        b_angle->bind(GL_ARRAY_BUFFER).mapBuffer<GLfloat>(GL_WRITE_ONLY, [&](auto buffer) {
            std::transform(body.begin(), body.end(), buffer,
                           [](auto& bd) { return bd->GetAngle(); });
        });
    }

    void draw() override {
        program->use();
        u_pMat.setData(pMat);
        u_color.setData(R::map::color.entity);

        vao->bind().drawArrays(GL_POINTS, 0, (GLsizei)std::size(R::map::free_box));
    }
};
