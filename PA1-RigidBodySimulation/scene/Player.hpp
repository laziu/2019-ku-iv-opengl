#pragma once
#include "ContactListener.hpp"
#include "IDrawable.hpp"
#include "R.h"
#include "WindowManager.hpp"
#include "glo/program.hpp"
#include "glo/vertex_array.hpp"
#include "util/color.hpp"
#include <algorithm>
#include <array>
#include <unordered_map>
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <Box2D/Box2D.h>

class Player : public IDrawable {
    static constexpr auto radius    = 0.33f;
    static constexpr auto xForce    = 0.14f;
    static constexpr auto yForce    = 2.f;
    static constexpr auto dampCoef  = 1.f;
    static constexpr auto dragCoef  = 0.015f;
    static constexpr auto faceColor = util::rgb_code("ffffff");
    static constexpr auto lineColor = util::rgb_code("000000");

    static constexpr auto v_data() {
        std::array<glm::vec2, 360 / 5 + 1> vert {};
        for (int i = 0; i <= 360; i += 5)
            vert[i / 5] = radius * glm::vec2(glm::cos(glm::radians(float(i))),
                                             glm::sin(glm::radians(float(i))));
        return vert;
    }

    std::shared_ptr<glo::program>      program;
    std::shared_ptr<glo::vertex_array> vao;

    glo::program::uniform<glm::mat4> u_mMat;
    glo::program::uniform<glm::mat4> u_pMat;
    glo::program::uniform<glm::vec3> u_color;

    glm::mat4  mMat;
    glm::mat4& pMat;

    b2World& world;
    b2Body*  body = nullptr;

    // ------ jump checker ------------------------------------
    float jumpForce = 0;

    std::unordered_map<b2Fixture*, bool> contacts;

public:
    Player(b2World& world, glm::mat4& pMat, ContactListener& cl):
            world(world), pMat(pMat), mMat(1.f) {

        auto vshader = glo::shader::create(GL_VERTEX_SHADER, R::shader::Player_vert);
        auto fshader = glo::shader::create(GL_FRAGMENT_SHADER, R::shader::Player_frag);
        glo::shader::lock({ vshader, fshader });

        program = glo::program::create({ vshader, fshader });

        u_pMat  = program->getUniform<glm::mat4>("pMat");
        u_mMat  = program->getUniform<glm::mat4>("mMat");
        u_color = program->getUniform<glm::vec3>("color");

        vao = glo::vertex_array::create();
        vao->bind();

        vao->hold(glo::buffer::create())
                ->bind(GL_ARRAY_BUFFER)
                .bufferData(std::size(v_data()) * sizeof(glm::vec2), v_data().data(),
                            GL_STATIC_DRAW)
                .vertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

        b2CircleShape shape;
        shape.m_radius = radius;

        b2FixtureDef fixDef;
        fixDef.shape       = &shape;
        fixDef.density     = 1.f;
        fixDef.restitution = .7f;

        b2BodyDef bodyDef;
        bodyDef.type   = b2_dynamicBody;
        bodyDef.bullet = true;
        bodyDef.position.Set(0, 0);

        body = world.CreateBody(&bodyDef);
        body->CreateFixture(&fixDef)->SetUserData((void*)1); // 1 = player

        auto playerAndObject = [](b2Fixture*& a, b2Fixture*& b) {
            if (1 != (size_t)a->GetUserData())
                std::swap(a, b);
            return 1 == (size_t)a->GetUserData() && !b->IsSensor(); // ignore water
        };

        cl.beginContactCallback.push_back([&](b2Contact* contact) {
            auto player = contact->GetFixtureA();
            auto target = contact->GetFixtureB();
            if (playerAndObject(player, target))
                contacts[target] = true;
        });

        cl.endContactCallback.push_back([&](b2Contact* contact) {
            auto player = contact->GetFixtureA();
            auto target = contact->GetFixtureB();
            if (playerAndObject(player, target)) {
                if (auto find = contacts.find(target); find != contacts.end())
                    contacts.erase(find);
            }
        });

        cl.postSolveCallback.push_back([&](b2Contact* contact, auto) {
            auto player = contact->GetFixtureA();
            auto target = contact->GetFixtureB();
            if (playerAndObject(player, target)) {
                b2WorldManifold manifold;
                contact->GetWorldManifold(&manifold);
                b2Vec2 direction = player->GetBody()->GetPosition() - manifold.points[0];
                direction.Normalize();
                // add jump force when W pressed
                if (direction.y > 0 && WindowManager::input().W &&
                    contacts.find(target) != contacts.end() && contacts[target]) {
                    if (jumpForce < direction.y * yForce)
                        jumpForce = direction.y * yForce;
                    contacts[target] = false;
                }
            }
        });

        cl.resetCallback.push_back([&]() { contacts.clear(); });
    }

    void update() override {
        auto   velocity = body->GetLinearVelocity();
        b2Vec2 force    = { 0, 0 };

        // apply jump and move force
        force.x = xForce * WindowManager::input().axisX;
        if (body->GetLinearVelocity().y > 0) {
            force.y   = jumpForce;
            jumpForce = 0;
        }

        // apply big drag if S pressed
        if (WindowManager::input().S) {
            auto velocity = body->GetLinearVelocity();
            velocity.y    = glm::max(velocity.y, 0.f);
            body->ApplyForceToCenter(dampCoef * -velocity, true);
            auto angVelocity = body->GetAngularVelocity();
            body->ApplyTorque(dampCoef * -angVelocity, true);
        }

        body->ApplyLinearImpulseToCenter(force, true);

        // apply linear air resistance
        auto direction = body->GetLinearVelocity();
        auto speed     = direction.Normalize();

        auto dragMagnitude = dragCoef * speed * speed / 2.f;
        auto dragForce     = dragMagnitude * -direction;
        body->ApplyForceToCenter(dragForce, true);

        // apply angular air resistance
        auto angDirection = body->GetAngularVelocity();
        auto angSpeed     = glm::abs(angDirection);
        angDirection      = angDirection >= 0 ? 1.f : -1.f;

        auto angDragMagnitude = dragCoef * angSpeed * angSpeed / 2.f;
        auto angDragForce     = angDragMagnitude * -angDirection;
        body->ApplyTorque(angDragForce, true);
    }

    void draw() override {
        auto position = body->GetPosition();

        mMat = glm::translate(glm::mat4(1.f), glm::vec3(position.x, position.y, 0.f));

        program->use();
        u_pMat.setData(pMat);
        u_mMat.setData(mMat);

        u_color.setData(faceColor);
        vao->bind().drawArrays(GL_TRIANGLE_FAN, 0, (GLsizei)v_data().size());

        glLineWidth(1.f);
        u_color.setData(lineColor);
        vao->bind().drawArrays(GL_LINE_LOOP, 0, (GLsizei)v_data().size());
    }

    const b2Vec2& position() const { return body->GetPosition(); }

    void position(const b2Vec2& pos) { body->SetTransform(pos, body->GetAngle()); }
};
