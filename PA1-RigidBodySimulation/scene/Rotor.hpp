#pragma once
#include "IDrawable.hpp"
#include "R.h"
#include "data.hpp"
#include <algorithm>
#include <iterator>
#include <tuple>
#include <vector>
#include <Box2D/Box2D.h>

class Rotor : public IDrawable {
    static constexpr auto density = 1.f;

    std::shared_ptr<glo::program>      program;
    std::shared_ptr<glo::vertex_array> vao;
    std::shared_ptr<glo::buffer>       b_angle;

    glo::program::uniform<glm::mat4> u_pMat;
    glo::program::uniform<glm::vec4> u_color;

    glm::mat4& pMat;

    std::vector<b2Body*> body;

public:
    Rotor(glm::mat4& pMat, b2World& world): pMat(pMat) {
        auto vshader = glo::shader::create(GL_VERTEX_SHADER, R::shader::box_vert);
        auto gshader = glo::shader::create(GL_GEOMETRY_SHADER, R::shader::box_geom);
        auto fshader = glo::shader::create(GL_FRAGMENT_SHADER, R::shader::default_frag);
        glo::shader::lock({ vshader, gshader, fshader });

        program = glo::program::create({ vshader, gshader, fshader });

        u_pMat  = program->getUniform<glm::mat4>("p_mat");
        u_color = program->getUniform<glm::vec4>("color");

        vao = glo::vertex_array::create();
        vao->bind();

        auto [buffer, _abuf] = vao->hold(glo::buffer::create<2>());

        buffer->bind(GL_ARRAY_BUFFER)
                .bufferData(sizeof(R::map::rotor), R::map::rotor, GL_STATIC_DRAW)
                .vertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(R::map::rotor[0]),
                                     (void*)sizeof(float))
                .vertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(R::map::rotor[0]),
                                     (void*)(sizeof(b2Vec2) + sizeof(float)));

        (b_angle = _abuf)
                ->bind(GL_ARRAY_BUFFER)
                .bufferData(std::size(R::map::rotor) * sizeof(float), nullptr, GL_STREAM_DRAW)
                .vertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 0, 0);

        b2BodyDef groundbd;
        auto      ground = world.CreateBody(&groundbd);

        for (auto [scale, position, speed] : R::map::rotor) {
            b2PolygonShape shape;
            shape.SetAsBox(scale.x / 2, scale.y / 2);

            b2BodyDef bodyDef;
            bodyDef.type           = b2_dynamicBody;
            bodyDef.position       = position;
            bodyDef.angularDamping = 0.8f;

            auto boxBody = world.CreateBody(&bodyDef);
            boxBody->CreateFixture(&shape, density);
            body.push_back(boxBody);

            b2RevoluteJointDef jointDef;
            jointDef.Initialize(ground, boxBody, position);
            jointDef.motorSpeed     = speed;
            jointDef.maxMotorTorque = 100.f;
            jointDef.enableMotor    = true;
            world.CreateJoint(&jointDef);
        }
    }

    void update() override {
        b_angle->bind(GL_ARRAY_BUFFER).mapBuffer<GLfloat>(GL_WRITE_ONLY, [&](auto buffer) {
            std::transform(body.begin(), body.end(), buffer,
                           [](auto& bd) { return bd->GetAngle(); });
        });
    }

    void draw() override {
        program->use();
        u_pMat.setData(pMat);
        u_color.setData(R::map::color.entity);

        vao->bind().drawArrays(GL_POINTS, 0, (GLsizei)std::size(R::map::rotor));
    }
};
