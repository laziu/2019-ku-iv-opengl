#pragma once
#include "IDrawable.hpp"
#include "R.h"
#include "data.hpp"
#include <Box2D/Box2D.h>

class Pulley : public IDrawable {
    static constexpr auto lineWidth = .1f;
    static constexpr auto density   = 1.f;

    static constexpr auto pulley = R::map::pulley;

    static constexpr b2Vec2 boxScaleData[] = { pulley.scale, pulley.scale };
    static constexpr b2Vec2 boxPosData[]   = { pulley.box1position, pulley.box2position };
    static constexpr b2Vec2 ropeVertData[] = {
        { pulley.box1position.x, pulley.box1position.y + pulley.scale.y / 2.f },
        { pulley.box1position.x, pulley.jointHeight },
        { pulley.box2position.x, pulley.jointHeight },
        { pulley.box2position.x, pulley.box2position.y + pulley.scale.y / 2.f },
    };

    struct {
        std::shared_ptr<glo::program>      program;
        std::shared_ptr<glo::vertex_array> vao;
        std::shared_ptr<glo::buffer>       b_position, b_angle;
        glo::program::uniform<glm::mat4>   u_pMat;
        glo::program::uniform<glm::vec4>   u_color;
    } box;

    struct {
        std::shared_ptr<glo::program>      program;
        std::shared_ptr<glo::vertex_array> vao;
        std::shared_ptr<glo::buffer>       b_position, b_normal;
        glo::program::uniform<glm::mat4>   u_pMat;
        glo::program::uniform<GLfloat>     u_lineWidth;
        glo::program::uniform<glm::vec4>   u_color;
    } rope;

    glm::mat4& pMat;

    b2Body*        body[2];
    b2PulleyJoint* joint;

public:
    Pulley(glm::mat4& pMat, b2World& world): pMat(pMat) {
        {
            auto vshader = glo::shader::create(GL_VERTEX_SHADER, R::shader::box_vert);
            auto gshader = glo::shader::create(GL_GEOMETRY_SHADER, R::shader::box_geom);
            auto fshader = glo::shader::create(GL_FRAGMENT_SHADER, R::shader::default_frag);
            glo::shader::lock({ vshader, gshader, fshader });

            box.program = glo::program::create({ vshader, gshader, fshader });

            box.u_pMat  = box.program->getUniform<glm::mat4>("p_mat");
            box.u_color = box.program->getUniform<glm::vec4>("color");

            box.vao = glo::vertex_array::create();
            box.vao->bind();

            box.vao->hold(glo::buffer::create())
                    ->bind(GL_ARRAY_BUFFER)
                    .bufferData(sizeof(boxScaleData), boxScaleData, GL_STATIC_DRAW)
                    .vertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

            auto buffers = box.vao->hold(glo::buffer::create<2>());

            (box.b_position = buffers[0])
                    ->bind(GL_ARRAY_BUFFER)
                    .bufferData(sizeof(boxPosData), nullptr, GL_STREAM_DRAW)
                    .vertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

            (box.b_angle = buffers[1])
                    ->bind(GL_ARRAY_BUFFER)
                    .bufferData(std::size(boxPosData) * sizeof(float), nullptr, GL_STREAM_DRAW)
                    .vertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 0, 0);
        }
        {
            auto vshader = glo::shader::create(GL_VERTEX_SHADER, R::shader::chain_vert);
            auto gshader = glo::shader::create(GL_GEOMETRY_SHADER, R::shader::rope_geom);
            auto fshader = glo::shader::create(GL_FRAGMENT_SHADER, R::shader::default_frag);
            glo::shader::lock({ vshader, gshader, fshader });

            rope.program = glo::program::create({ vshader, gshader, fshader });

            rope.u_pMat      = rope.program->getUniform<glm::mat4>("p_mat");
            rope.u_lineWidth = rope.program->getUniform<GLfloat>("line_width");
            rope.u_color     = rope.program->getUniform<glm::vec4>("color");

            rope.vao = glo::vertex_array::create();
            rope.vao->bind();

            auto buffers = rope.vao->hold(glo::buffer::create<2>());

            (rope.b_position = buffers[0])
                    ->bind(GL_ARRAY_BUFFER)
                    .bufferData(sizeof(ropeVertData), nullptr, GL_STREAM_DRAW)
                    .vertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

            (rope.b_normal = buffers[1])
                    ->bind(GL_ARRAY_BUFFER)
                    .bufferData(sizeof(ropeVertData), nullptr, GL_STREAM_DRAW)
                    .vertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
        }

        b2BodyDef bodyDef;
        bodyDef.type           = b2_dynamicBody;
        bodyDef.linearDamping  = .9999f;
        bodyDef.angularDamping = .9999f;

        b2PolygonShape shape;
        b2FixtureDef   fixDef;
        fixDef.shape    = &shape;
        fixDef.density  = density;
        fixDef.friction = 0;

        shape.SetAsBox(boxScaleData[0].x / 2.f, boxScaleData[0].y / 2.f);
        bodyDef.position = boxPosData[0];

        body[0] = world.CreateBody(&bodyDef);
        body[0]->CreateFixture(&fixDef);

        shape.SetAsBox(boxScaleData[1].x / 2.f, boxScaleData[1].y / 2.f);
        bodyDef.position = boxPosData[1];

        body[1] = world.CreateBody(&bodyDef);
        body[1]->CreateFixture(&fixDef);

        b2PulleyJointDef pulleyDef;
        pulleyDef.Initialize(body[0], body[1], ropeVertData[1], ropeVertData[2], ropeVertData[0],
                             ropeVertData[3], 1.f);
        joint = static_cast<b2PulleyJoint*>(world.CreateJoint(&pulleyDef));
    }

    void update() override {
        box.b_position->bind(GL_ARRAY_BUFFER).mapBuffer<b2Vec2>(GL_WRITE_ONLY, [&](b2Vec2* buffer) {
            buffer[0] = body[0]->GetPosition();
            buffer[1] = body[1]->GetPosition();
        });

        box.b_angle->bind(GL_ARRAY_BUFFER).mapBuffer<GLfloat>(GL_WRITE_ONLY, [&](GLfloat* buffer) {
            buffer[0] = body[0]->GetAngle();
            buffer[1] = body[1]->GetAngle();
        });

        b2Vec2 anchors[4] = {
            joint->GetAnchorA(),
            joint->GetGroundAnchorA(),
            joint->GetGroundAnchorB(),
            joint->GetAnchorB(),
        };

        rope.b_position->bind(GL_ARRAY_BUFFER)
                .mapBuffer<b2Vec2>(GL_WRITE_ONLY, [&](b2Vec2* buffer) {
                    for (int i = 0; i < 4; ++i)
                        buffer[i] = anchors[i];
                });

        rope.b_normal->bind(GL_ARRAY_BUFFER).mapBuffer<b2Vec2>(GL_WRITE_ONLY, [&](b2Vec2* buffer) {
            for (int i = 0; i < 3; ++i)
                buffer[i] = anchors[i + 1] - anchors[i];
            buffer[3] = buffer[2];

            for (int i = 0; i < 4; ++i)
                buffer[i].Set(-buffer[i].y, buffer[i].x);
        });
    }

    void draw() override {
        rope.program->use();
        rope.u_pMat.setData(pMat);
        rope.u_lineWidth.setData(lineWidth);
        rope.u_color.setData(R::map::color.rope);
        rope.vao->bind().drawArrays(GL_LINE_STRIP, 0, (GLsizei)std::size(ropeVertData));

        box.program->use();
        box.u_pMat.setData(pMat);
        box.u_color.setData(R::map::color.entity);
        box.vao->bind().drawArrays(GL_POINTS, 0, (GLsizei)std::size(boxPosData));
    }
};
