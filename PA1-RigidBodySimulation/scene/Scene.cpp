#include "Scene.hpp"
#include "scene/data.hpp"

#include "WindowManager.hpp"
#include <glm/glm.hpp>
#include <glm/ext/matrix_clip_space.hpp>

#include "FreeBox.hpp"
#include "Pulley.hpp"
#include "Rotor.hpp"
#include "Seesaw.hpp"
#include "StaticBox.hpp"
#include "StaticChain.hpp"
#include "Swing.hpp"
#include "WaterArea.hpp"

Scene::Scene() {
    entities.push_back(std::make_unique<Pulley>(pMat, world));
    entities.push_back(std::make_unique<Swing>(pMat, world));
    entities.push_back(std::make_unique<StaticBox>(pMat, world));
    entities.push_back(std::make_unique<StaticChain>(pMat, world));
    entities.push_back(std::make_unique<Rotor>(pMat, world));
    entities.push_back(std::make_unique<Seesaw>(pMat, world));
    entities.push_back(std::make_unique<FreeBox>(pMat, world));
    entities.push_back(std::make_unique<WaterArea>(pMat, world, contactListener));

    glo::shader::free();
    glo::program::free();
    world.SetContactListener(&contactListener);
    glActiveTexture(GL_TEXTURE0);
}

void Scene::update(GLFWwindow* window, double time, double deltaTime) {
    if (pause) {
        pause = false;
        return;
    }
    deltaTime = glm::min(deltaTime, .1) * timeScale;

    if (WindowManager::input().F)
        world.SetGravity(b2Vec2_zero);
    else
        world.SetGravity(gravity);

    world.Step((float)deltaTime, 24, 9);
    world.ClearForces();

    for (auto& e : entities)
        e->update();

    player.update();
    shadow.update();

    auto ratio = (float)WindowManager::size().width / WindowManager::size().height;

    auto target = player.position();
    auto view   = b2Vec2(ratio * viewScale, viewScale);

#define FOLLOW_IF_TARGET_EXCEEDS_POS_LIMIT(axis, c, s)       \
    if (target.axis - camera.axis c s(posLimit * view.axis)) \
        camera.axis = target.axis - s(posLimit * view.axis);
    FOLLOW_IF_TARGET_EXCEEDS_POS_LIMIT(x, >, +)
    FOLLOW_IF_TARGET_EXCEEDS_POS_LIMIT(x, <, -)
    FOLLOW_IF_TARGET_EXCEEDS_POS_LIMIT(y, >, +)
    FOLLOW_IF_TARGET_EXCEEDS_POS_LIMIT(y, <, -)
#undef FOLLOW_IF_TARGET_EXCEEDS_VIEW_LIMIT

#define STAY_IF_VIEW_EXCEEDS_VIEW_LIMIT(axis, s, c, m) \
    if (camera.axis s view.axis c R::map::area.m.axis) \
        camera.axis = R::map::area.m.axis - s(view.axis);
    STAY_IF_VIEW_EXCEEDS_VIEW_LIMIT(x, +, >, max)
    STAY_IF_VIEW_EXCEEDS_VIEW_LIMIT(x, -, <, min)
    STAY_IF_VIEW_EXCEEDS_VIEW_LIMIT(y, +, >, max)
    STAY_IF_VIEW_EXCEEDS_VIEW_LIMIT(y, -, <, min)
#undef STAY_IF_VIEW_EXCEEDS_VIEW_LIMIT

    pMat = glm::ortho(camera.x - view.x, camera.x + view.x, camera.y - view.y, camera.y + view.y);
}

void Scene::render(GLFWwindow* window, double time, double deltaTime) {
    auto originalPMat = pMat;
    pMat *= glm::translate(glm::mat4(1.f), glm::vec3(.5f, -.5f, 0.f));

    shadow.drawform([&] {
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        player.draw();
        for (auto& e : entities)
            e->draw();
    });

    pMat = originalPMat;

    glClearColor(bg_color.r, bg_color.g, bg_color.b, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    shadow.draw();
    player.draw();
    for (auto& e : entities)
        e->draw();

    glfwSwapBuffers(window);
}

void Scene::moveStage(int level) {
    if (level < 0 || std::size(R::map::start_position) <= level) {
        fprintf(stderr, "Level %d not exists.\n", level);
        return;
    }

    for (auto& f : contactListener.resetCallback)
        f();

    player.position(R::map::start_position[level]);
}
