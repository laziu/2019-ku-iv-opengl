#pragma once
#include "IDrawable.hpp"
#include "R.h"
#include "data.hpp"
#include <iterator>
#include <tuple>
#include <Box2D/Box2D.h>

class StaticChain : public IDrawable {
    static constexpr float lineWidth = std::get<0>(R::map::static_box[0]).y;

    std::shared_ptr<glo::program> program;

    std::array<glo::vertex_array::sptr_t, 2> vao;

    glo::program::uniform<glm::mat4> u_pMat;
    glo::program::uniform<GLfloat>   u_lineWidth;
    glo::program::uniform<glm::vec4> u_color;

    glm::mat4& pMat;

    template <size_t N>
    auto gen_normal(const b2Vec2* positions) {
        std::array<b2Vec2, N> normals {};

        normals[0] = positions[1] - positions[0];
        normals[0].Set(normals[0].y, -normals[0].x);
        normals[0].Normalize();

        normals[N - 1] = positions[N - 1] - positions[N - 2];
        normals[N - 1].Set(normals[N - 1].y, -normals[N - 1].x);
        normals[N - 1].Normalize();

        for (size_t i = 1; i <= N - 2; ++i) {
            normals[i] = positions[i + 1] - positions[i - 1];
            normals[i].Set(normals[i].y, -normals[i].x);
            normals[i].Normalize();
        }
        return normals;
    }

    template <size_t N>
    auto with_normal_vertex(const b2Vec2* positions, std::array<b2Vec2, N>& normals) {
        std::array<b2Vec2, 2 * N> vertices;
        for (size_t i = 0; i < N; ++i)
            vertices[i] = positions[i];
        for (size_t i = 0; i < N; ++i)
            vertices[N + i] = positions[N - 1 - i] + lineWidth * normals[N - 1 - i];
        return vertices;
    }

public:
    StaticChain(glm::mat4& pMat, b2World& world): pMat(pMat) {
        using R::map::static_chain_ceil;
        using R::map::static_chain_floor;

        auto vshader = glo::shader::create(GL_VERTEX_SHADER, R::shader::chain_vert);
        auto gshader = glo::shader::create(GL_GEOMETRY_SHADER, R::shader::chain_geom);
        auto fshader = glo::shader::create(GL_FRAGMENT_SHADER, R::shader::default_frag);
        glo::shader::lock({ vshader, gshader, fshader });

        program = glo::program::create({ vshader, gshader, fshader });

        u_pMat      = program->getUniform<glm::mat4>("p_mat");
        u_lineWidth = program->getUniform<GLfloat>("line_width");
        u_color     = program->getUniform<glm::vec4>("color");

        program->use();
        u_lineWidth.setData(lineWidth);

        auto normal_floor = gen_normal<std::size(static_chain_floor)>(static_chain_floor);
        auto normal_ceil  = gen_normal<std::size(static_chain_ceil)>(static_chain_ceil);

        vao = glo::vertex_array::create<2>();

        vao[0]->bind();
        auto [b_floor_position, b_floor_normal] = vao[0]->hold(glo::buffer::create<2>());
        b_floor_position->bind(GL_ARRAY_BUFFER)
                .bufferData(sizeof(static_chain_floor), static_chain_floor, GL_STATIC_DRAW)
                .vertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
        b_floor_normal->bind(GL_ARRAY_BUFFER)
                .bufferData(sizeof(static_chain_floor), normal_floor.data(), GL_STATIC_DRAW)
                .vertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

        vao[1]->bind();
        auto [b_ceil_position, b_ceil_normal] = vao[1]->hold(glo::buffer::create<2>());
        b_ceil_position->bind(GL_ARRAY_BUFFER)
                .bufferData(sizeof(static_chain_ceil), static_chain_ceil, GL_STATIC_DRAW)
                .vertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
        b_ceil_normal->bind(GL_ARRAY_BUFFER)
                .bufferData(sizeof(static_chain_ceil), normal_ceil.data(), GL_STATIC_DRAW)
                .vertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

        b2BodyDef bodyDef;
        auto      body = world.CreateBody(&bodyDef);

        auto vert_floor = with_normal_vertex<normal_floor.size()>(static_chain_floor, normal_floor);
        auto vert_ceil  = with_normal_vertex<normal_ceil.size()>(static_chain_ceil, normal_ceil);

        b2ChainShape shape[2];
        shape[0].CreateLoop(vert_floor.data(), (int32)vert_floor.size());
        shape[1].CreateLoop(vert_ceil.data(), (int32)vert_ceil.size());
        body->CreateFixture(&shape[0], 1.f);
        body->CreateFixture(&shape[1], 1.f);
    }

    void draw() override {
        program->use();
        u_pMat.setData(pMat);
        u_color.setData(R::map::color.field);

        vao[0]->bind().drawArrays(GL_LINE_STRIP, 0, (GLsizei)std::size(R::map::static_chain_floor));
        vao[1]->bind().drawArrays(GL_LINE_STRIP, 0, (GLsizei)std::size(R::map::static_chain_ceil));
    }
};
