#pragma once
#include "IDrawable.hpp"
#include "R.h"
#include "data.hpp"

#include "ContactListener.hpp"
#include "WindowManager.hpp"
#include <algorithm>
#include <iterator>
#include <set>
#include <tuple>
#include <glm/glm.hpp>
#include <Box2D/Box2D.h>

class WaterArea : public IDrawable {
    static constexpr auto density = 1.5f;
    static constexpr auto pl_coef = .8f;
    static constexpr auto pl_swim = 5.f;

    std::shared_ptr<glo::program>      program;
    std::shared_ptr<glo::vertex_array> vao;

    glo::program::uniform<glm::mat4> u_pMat;
    glo::program::uniform<glm::vec4> u_color;

    glm::mat4& pMat;

    b2World& world;

    std::set<std::tuple<b2Fixture*, b2Fixture*>> contacts;

    auto calculatePolygonAreaAndCentroid(b2Fixture* fixWater, b2Fixture* fixTarget)
            -> std::tuple<float32, b2Vec2> {

        auto polyWater  = (b2PolygonShape*)fixWater->GetShape();
        auto polyTarget = (b2PolygonShape*)fixTarget->GetShape();

        static auto inside = [](b2Vec2 cp1, b2Vec2 cp2, b2Vec2 p) {
            return (cp2.x - cp1.x) * (p.y - cp1.y) > (cp2.y - cp1.y) * (p.x - cp1.x);
        };

        static auto intersection = [](b2Vec2 cp1, b2Vec2 cp2, b2Vec2 s, b2Vec2 e) {
            b2Vec2 dc { cp1.x - cp2.x, cp1.y - cp2.y };
            b2Vec2 dp { s.x - e.x, s.y - e.y };
            float  n1 = cp1.x * cp2.y - cp1.y * cp2.x;
            float  n2 = s.x * e.y - s.y * e.x;
            float  n3 = 1.f / (dc.x * dp.y - dc.y * dp.x);
            return b2Vec2((n1 * dp.x - n2 * dc.x) * n3, (n1 * dp.y - n2 * dc.y) * n3);
        };

        // find intersection
        std::vector<b2Vec2> vertWater, vertTarget;
        for (size_t i = 0; i < polyWater->m_count; ++i)
            vertWater.push_back(fixWater->GetBody()->GetWorldPoint(polyWater->m_vertices[i]));
        for (size_t i = 0; i < polyTarget->m_count; ++i)
            vertTarget.push_back(fixTarget->GetBody()->GetWorldPoint(polyTarget->m_vertices[i]));

        auto cp1 = vertWater[vertWater.size() - 1];
        for (auto cp2 : vertWater) {
            std::vector<b2Vec2> vertices = vertTarget;
            vertTarget.clear();

            b2Vec2 s = vertices[vertices.size() - 1];
            for (auto e : vertices) {
                if (inside(cp1, cp2, e)) {
                    if (!inside(cp1, cp2, s))
                        vertTarget.push_back(intersection(cp1, cp2, s, e));
                    vertTarget.push_back(e);
                }
                else if (inside(cp1, cp2, s))
                    vertTarget.push_back(intersection(cp1, cp2, s, e));
                s = e;
            }
            cp1 = cp2;
        }

        if (vertTarget.size() >= 3) {
            // find centroid and intersection area
            float  totalArea = 0;
            b2Vec2 centroid { 0, 0 };

            for (size_t i = 0; i < vertTarget.size(); ++i) {
                b2Vec2 p1 { 0, 0 };
                b2Vec2 p2 = vertTarget[i];
                b2Vec2 p3 = i + 1 < vertTarget.size() ? vertTarget[i + 1] : vertTarget[0];

                b2Vec2 e1 = p2 - p1;
                b2Vec2 e2 = p3 - p1;

                float area = b2Cross(e1, e2) / 2.f;
                totalArea += area;

                centroid += (area / 3.f) * (p1 + p2 + p3);
            }

            if (totalArea > b2_epsilon)
                centroid *= 1.f / totalArea;
            else
                totalArea = 0;

            return std::tuple(totalArea, centroid);
        }
        else
            return std::tuple(0.f, b2Vec2_zero);
    }

    auto calculateCircleArea(b2Fixture* fixWater, b2Fixture* fixTarget) -> float32 {
        auto polyWater = (b2PolygonShape*)fixWater->GetShape();

        float waterLevel = -100.f;
        for (size_t i = 0; i < polyWater->m_count; ++i)
            if (waterLevel < polyWater->m_vertices[i].y)
                waterLevel = polyWater->m_vertices[i].y;

        float targetY = fixTarget->GetBody()->GetPosition().y;
        float radius  = static_cast<b2CircleShape*>(fixTarget->GetShape())->m_radius;

        float diff  = glm::min(glm::max(waterLevel - targetY, -radius), radius);
        float theta = glm::acos(diff / radius);

        float area = (glm::pi<float>() - theta) * radius * radius + diff * diff * glm::tan(theta);

        return area;
    }

public:
    WaterArea(glm::mat4& pMat, b2World& world, ContactListener& cl): pMat(pMat), world(world) {
        auto vshader = glo::shader::create(GL_VERTEX_SHADER, R::shader::box_vert);
        auto gshader = glo::shader::create(GL_GEOMETRY_SHADER, R::shader::water_geom);
        auto fshader = glo::shader::create(GL_FRAGMENT_SHADER, R::shader::default_frag);
        glo::shader::lock({ vshader, gshader, fshader });

        program = glo::program::create({ vshader, gshader, fshader });

        u_pMat  = program->getUniform<glm::mat4>("p_mat");
        u_color = program->getUniform<glm::vec4>("color");

        vao = glo::vertex_array::create();
        vao->bind();

        auto buffer = vao->hold(glo::buffer::create());

        buffer->bind(GL_ARRAY_BUFFER)
                .bufferData(sizeof(R::map::water), R::map::water, GL_STATIC_DRAW)
                .vertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(R::map::water[0]), 0)
                .vertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(R::map::water[0]),
                                     (void*)sizeof(b2Vec2));

        b2BodyDef bodyDef;
        auto      body = world.CreateBody(&bodyDef);

        for (auto [scale, position] : R::map::water) {
            b2PolygonShape shape;
            shape.SetAsBox(scale.x / 2, scale.y / 2, position, 0);

            b2FixtureDef fixDef;
            fixDef.shape    = &shape;
            fixDef.density  = density;
            fixDef.isSensor = true;
            body->CreateFixture(&fixDef)->SetUserData((void*)0x10000); // water = 0x10000
        }

        auto waterAndOthers = [](b2Fixture*& a, b2Fixture*& b) {
            if (0x10000 != (size_t)a->GetUserData())
                std::swap(a, b);
            return 0x10000 == (size_t)a->GetUserData();
        };

        cl.beginContactCallback.push_back([&](b2Contact* contact) {
            auto water  = contact->GetFixtureA();
            auto target = contact->GetFixtureB();
            if (waterAndOthers(water, target))
                contacts.insert(std::make_tuple(water, target));
        });

        cl.endContactCallback.push_back([&](b2Contact* contact) {
            auto water  = contact->GetFixtureA();
            auto target = contact->GetFixtureB();
            if (waterAndOthers(water, target))
                contacts.erase(std::make_tuple(water, target));
        });

        cl.resetCallback.push_back([&]() {
            for (auto it = contacts.begin(); it != contacts.end();)
                if (auto [water, target] = *it; 1 == (size_t)target->GetUserData())
                    it = contacts.erase(it);
                else
                    ++it;
        });
    }

    void update() override {
        for (auto [fixWater, fixTarget] : contacts) {
            switch (fixTarget->GetShape()->GetType()) {
            case b2Shape::e_polygon: {
                auto [area, centroid] = calculatePolygonAreaAndCentroid(fixWater, fixTarget);

                // apply buoyancy
                float  displacedMass = fixWater->GetDensity() * area;
                b2Vec2 buoyancy      = displacedMass * -1.f * world.GetGravity();
                fixTarget->GetBody()->ApplyForce(buoyancy, centroid, true);

                // apply linear drag
                auto direction = fixTarget->GetBody()->GetLinearVelocityFromWorldPoint(centroid);
                auto speed     = direction.Normalize();

                auto dragMagnitude = fixWater->GetDensity() * speed * speed / 2.f;
                auto dragForce     = dragMagnitude * -direction;
                fixTarget->GetBody()->ApplyForce(dragForce, centroid, true);

                // apply angular drag
                auto angDirection = fixTarget->GetBody()->GetAngularVelocity();
                auto angSpeed     = glm::abs(angDirection);
                angDirection      = angDirection >= 0 ? 1.f : -1.f;

                auto angDragMagnitude = fixWater->GetDensity() * angSpeed * angSpeed / 2.f;
                auto angDragForce     = angDragMagnitude * -angDirection;
                fixTarget->GetBody()->ApplyTorque(angDragForce, true);
            } break;
            case b2Shape::e_circle: {
                auto totalArea = calculateCircleArea(fixWater, fixTarget);

                float scaler = 1 == (size_t)fixTarget->GetUserData() ? pl_coef : 1.f;

                // apply buoyancy
                float  displacedMass = fixWater->GetDensity() * scaler * totalArea;
                b2Vec2 buoyancy      = displacedMass * -1.f * world.GetGravity();
                fixTarget->GetBody()->ApplyForceToCenter(buoyancy, true);

                // apply linear drag
                auto direction = fixTarget->GetBody()->GetLinearVelocity();
                auto speed     = direction.Normalize();

                auto dragMagnitude = fixWater->GetDensity() * scaler * speed * speed / 2.f;
                auto dragForce     = dragMagnitude * -direction;
                fixTarget->GetBody()->ApplyForceToCenter(dragForce, true);

                // apply angular drag
                auto angDirection = fixTarget->GetBody()->GetAngularVelocity();
                auto angSpeed     = glm::abs(angDirection);
                angDirection      = angDirection >= 0 ? 1.f : -1.f;

                auto angDragMagnitude = fixWater->GetDensity() * scaler * angSpeed * angSpeed / 2.f;
                auto angDragForce     = angDragMagnitude * -angDirection;
                fixTarget->GetBody()->ApplyTorque(angDragForce, true);

                // player move
                if (1 == (size_t)fixTarget->GetUserData()) {
                    if (WindowManager::input().W)
                        fixTarget->GetBody()->ApplyForceToCenter(b2Vec2(0, pl_swim), true);
                    else if (WindowManager::input().S)
                        fixTarget->GetBody()->ApplyForceToCenter(b2Vec2(0, -pl_swim), true);
                }
            } break;
            }
        }
    }

    void draw() override {
        program->use();
        u_pMat.setData(pMat);
        u_color.setData(R::map::color.water);

        vao->bind().drawArrays(GL_POINTS, 0, (GLsizei)std::size(R::map::water));
    }
};
