#pragma once
#include "glo/program.hpp"
#include "glo/vertex_array.hpp"
#include <stdexcept>
#include <Box2D/Dynamics/b2World.h>

class IDrawable {
public:
    virtual ~IDrawable() {}

    virtual void update() {}
    virtual void draw() {}
};
