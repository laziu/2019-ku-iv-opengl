#pragma once
#include "IDrawable.hpp"
#include "R.h"
#include "WindowManager.hpp"
#include "glo/framebuffer.hpp"
#include "glo/program.hpp"
#include "glo/texture.hpp"
#include "glo/vertex_array.hpp"
#include <functional>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

class Shadow : public IDrawable {
    static constexpr glm::vec2 position[] = { { -1.f, 1.f },
                                              { -1.f, -1.f },
                                              { 1.f, 1.f },
                                              { 1.f, -1.f } };

    static constexpr glm::vec2 texCoord[] = { { 0.f, 1.f },
                                              { 0.f, 0.f },
                                              { 1.f, 1.f },
                                              { 1.f, 0.f } };

    glo::program::sptr_t      program;
    glo::vertex_array::sptr_t vao;
    glo::framebuffer::sptr_t  fbo;
    glo::texture::sptr_t      tex;

    int width, height;

public:
    Shadow() {
        auto vshader = glo::shader::create(GL_VERTEX_SHADER, R::shader::shadow_vert);
        auto fshader = glo::shader::create(GL_FRAGMENT_SHADER, R::shader::shadow_frag);
        glo::shader::lock({ vshader, fshader });

        program = glo::program::create({ vshader, fshader });

        vao = glo::vertex_array::create();
        vao->bind();

        auto vbo = vao->hold(glo::buffer::create<2>());

        vbo[0]->bind(GL_ARRAY_BUFFER)
                .bufferData(sizeof(position), position, GL_STATIC_DRAW)
                .vertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
        vbo[1]->bind(GL_ARRAY_BUFFER)
                .bufferData(sizeof(texCoord), texCoord, GL_STATIC_DRAW)
                .vertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

        auto [w, h] = WindowManager::size();
        createFBO(width = w, height = h);
    }

    void createFBO(int width, int height) {
        (tex = glo::texture::create())
                ->bind(GL_TEXTURE_2D)
                .image2D(0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr)
                .parameteri(GL_TEXTURE_MIN_FILTER, GL_LINEAR)
                .parameteri(GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        (fbo = glo::framebuffer::create())
                ->bind(GL_FRAMEBUFFER)
                .texture2D(GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0)
                .checkStatus()
                .end();
    }

    void update() override {
        auto [w, h] = WindowManager::size();
        if (width != w || height != h)
            createFBO(width = w, height = h);
    }

    void drawform(std::function<void()> f) { fbo->bind(GL_FRAMEBUFFER).use(f).end(); }

    void draw() override {
        program->use();
        tex->bind(GL_TEXTURE_2D);
        vao->bind().drawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }
};
