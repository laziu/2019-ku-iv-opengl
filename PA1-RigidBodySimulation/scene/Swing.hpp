#pragma once
#include "IDrawable.hpp"
#include "R.h"
#include "data.hpp"
#include <algorithm>
#include <iterator>
#include <vector>
#include <glm/trigonometric.hpp>
#include <Box2D/Box2D.h>

class Swing : public IDrawable {
    static constexpr auto lineWidth = .1f;
    static constexpr auto density   = 1.f;

    struct {
        std::shared_ptr<glo::program>      program;
        std::shared_ptr<glo::vertex_array> vao;
        std::shared_ptr<glo::buffer>       b_position, b_angle;
        glo::program::uniform<glm::mat4>   u_pMat;
        glo::program::uniform<glm::vec4>   u_color;
    } box;

    struct {
        std::shared_ptr<glo::program>      program;
        std::shared_ptr<glo::vertex_array> vao;
        std::shared_ptr<glo::buffer>       b_position, b_normal;
        glo::program::uniform<glm::mat4>   u_pMat;
        glo::program::uniform<GLfloat>     u_lineWidth;
        glo::program::uniform<glm::vec4>   u_color;
    } rope;

    glm::mat4& pMat;

    std::vector<b2Body*>      body;
    std::vector<b2RopeJoint*> joint;

public:
    Swing(glm::mat4& pMat, b2World& world): pMat(pMat) {
        using R::map::swing;
        {
            auto vshader = glo::shader::create(GL_VERTEX_SHADER, R::shader::box_vert);
            auto gshader = glo::shader::create(GL_GEOMETRY_SHADER, R::shader::box_geom);
            auto fshader = glo::shader::create(GL_FRAGMENT_SHADER, R::shader::default_frag);
            glo::shader::lock({ vshader, gshader, fshader });

            box.program = glo::program::create({ vshader, gshader, fshader });

            box.u_pMat  = box.program->getUniform<glm::mat4>("p_mat");
            box.u_color = box.program->getUniform<glm::vec4>("color");

            box.vao = glo::vertex_array::create();
            box.vao->bind();

            box.vao->hold(glo::buffer::create())
                    ->bind(GL_ARRAY_BUFFER)
                    .bufferData(sizeof(swing), swing, GL_STATIC_DRAW)
                    .vertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(swing[0]),
                                         (void*)(sizeof(b2Vec2) + sizeof(float)));

            auto buffers = box.vao->hold(glo::buffer::create<2>());

            (box.b_position = buffers[0])
                    ->bind(GL_ARRAY_BUFFER)
                    .bufferData(std::size(swing) * sizeof(b2Vec2), nullptr, GL_STREAM_DRAW)
                    .vertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

            (box.b_angle = buffers[1])
                    ->bind(GL_ARRAY_BUFFER)
                    .bufferData(std::size(swing) * sizeof(GLfloat), nullptr, GL_STREAM_DRAW)
                    .vertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 0, 0);
        }
        {
            auto vshader = glo::shader::create(GL_VERTEX_SHADER, R::shader::chain_vert);
            auto gshader = glo::shader::create(GL_GEOMETRY_SHADER, R::shader::rope_geom);
            auto fshader = glo::shader::create(GL_FRAGMENT_SHADER, R::shader::default_frag);
            glo::shader::lock({ vshader, gshader, fshader });

            rope.program = glo::program::create({ vshader, gshader, fshader });

            rope.u_pMat      = rope.program->getUniform<glm::mat4>("p_mat");
            rope.u_lineWidth = rope.program->getUniform<GLfloat>("line_width");
            rope.u_color     = rope.program->getUniform<glm::vec4>("color");

            rope.vao = glo::vertex_array::create();
            rope.vao->bind();

            auto buffers = rope.vao->hold(glo::buffer::create<2>());

            (rope.b_position = buffers[0])
                    ->bind(GL_ARRAY_BUFFER)
                    .bufferData(std::size(swing) * sizeof(b2Vec2) * 4, nullptr, GL_STREAM_DRAW)
                    .vertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

            (rope.b_normal = buffers[1])
                    ->bind(GL_ARRAY_BUFFER)
                    .bufferData(std::size(swing) * sizeof(b2Vec2) * 4, nullptr, GL_STREAM_DRAW)
                    .vertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
        }

        for (auto [scale, position, jointHeight] : R::map::swing) {
            b2PolygonShape shape;
            b2BodyDef      bodyDef;
            bodyDef.type = b2_dynamicBody;

            shape.SetAsBox(scale.x / 2.f, scale.y / 2.f);
            bodyDef.position = position;

            auto boxBody = world.CreateBody(&bodyDef);
            boxBody->CreateFixture(&shape, density);
            body.push_back(boxBody);

            b2BodyDef ceilBodyDef;
            ceilBodyDef.position.Set(position.x, jointHeight);
            auto ceilBody = world.CreateBody(&ceilBodyDef);

            b2RopeJointDef ropeDef[2];
            ropeDef[0].bodyA = ropeDef[1].bodyA = ceilBody;
            ropeDef[0].bodyB = ropeDef[1].bodyB = boxBody;
            ropeDef[0].localAnchorA = ropeDef[1].localAnchorA = b2Vec2_zero;
            ropeDef[0].localAnchorB.Set(-(scale.x - lineWidth) / 2.f, 0);
            ropeDef[1].localAnchorB.Set(+(scale.x - lineWidth) / 2.f, 0);
            ropeDef[0].maxLength = ropeDef[1].maxLength =
                    glm::sqrt(glm::pow(scale.x / 2.f, 2) + glm::pow(jointHeight - position.y, 2));

            joint.push_back(static_cast<b2RopeJoint*>(world.CreateJoint(&ropeDef[0])));
            joint.push_back(static_cast<b2RopeJoint*>(world.CreateJoint(&ropeDef[1])));
        }
    }

    void update() override {
        box.b_position->bind(GL_ARRAY_BUFFER).mapBuffer<b2Vec2>(GL_WRITE_ONLY, [&](b2Vec2* buf) {
            std::transform(body.begin(), body.end(), buf,
                           [](auto bd) { return bd->GetPosition(); });
        });

        box.b_angle->bind(GL_ARRAY_BUFFER).mapBuffer<GLfloat>(GL_WRITE_ONLY, [&](GLfloat* buf) {
            std::transform(body.begin(), body.end(), buf, [](auto bd) { return bd->GetAngle(); });
        });

        rope.b_position->bind(GL_ARRAY_BUFFER)
                .mapBuffer<b2Vec2>(GL_WRITE_ONLY, [&](b2Vec2* buffer) {
                    for (size_t i = 0; i < joint.size(); ++i) {
                        buffer[2 * i]     = joint[i]->GetAnchorA();
                        buffer[2 * i + 1] = joint[i]->GetAnchorB();
                    }
                });

        rope.b_normal->bind(GL_ARRAY_BUFFER).mapBuffer<b2Vec2>(GL_WRITE_ONLY, [&](b2Vec2* buffer) {
            for (size_t i = 0; i < joint.size(); ++i) {
                b2Vec2 direction = joint[i]->GetAnchorB() - joint[i]->GetAnchorA();
                direction.Normalize();
                b2Vec2 normal { -direction.y, direction.x };
                buffer[2 * i] = buffer[2 * i + 1] = normal;
            }
        });
    }

    void draw() override {
        rope.program->use();
        rope.u_pMat.setData(pMat);
        rope.u_lineWidth.setData(lineWidth);
        rope.u_color.setData(R::map::color.rope);
        rope.vao->bind().drawArrays(GL_LINES, 0, (GLsizei)joint.size() * 2);

        box.program->use();
        box.u_pMat.setData(pMat);
        box.u_color.setData(R::map::color.entity);
        box.vao->bind().drawArrays(GL_POINTS, 0, (GLsizei)body.size());
    }
};
