#pragma once
#include <memory>
#include <vector>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/mat4x4.hpp>
#include <Box2D/Dynamics/b2World.h>

#include "ContactListener.hpp"
#include "IDrawable.hpp"
#include "Player.hpp"
#include "Shadow.hpp"
#include "data.hpp"

class Scene {
    static constexpr auto bg_color  = R::map::color.background;
    static constexpr auto viewScale = 10.f;
    static constexpr auto timeScale = 1.6f;
    static constexpr auto posLimit  = .2f;
    static constexpr auto gravity   = b2Vec2(0, -9.8f);

    b2World   world = { gravity };
    glm::mat4 pMat;

    ContactListener contactListener;

    // order is important
    Shadow shadow;
    Player player = { world, pMat, contactListener };

    std::vector<std::unique_ptr<IDrawable>> entities;

    b2Vec2 camera;

public:
    Scene();

    void resize(GLFWwindow* window, int width, int height) { glViewport(0, 0, width, height); }
    void update(GLFWwindow* window, double time, double deltaTime);
    void render(GLFWwindow* window, double time, double deltaTime);

    void moveStage(int level);

    bool pause = false;
};
