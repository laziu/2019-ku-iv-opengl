#pragma once
#include <cstdio>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

struct WindowManager {
    struct InputStatus {
        bool W, A, S, D, F;
        int  axisX, axisY;
    };

    struct SizeStatus {
        int width, height;
    };

    static void initialize(GLFWwindow* window) {
        WindowManager::window = window;
        printf("~~~~~~~~~~~~~~~~~~~~ Input Key ~~~~~~~~~~~~~~~~~~~~\n");
        printf("     W             ^         W: Add jump force     \n");
        printf("   A S D    =    < v >       S: Try to stop        \n");
        printf("                             F: Ignore Gravity     \n");
        printf("  1~7: Move to checkpoint    R: Restart            \n");
        printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    }

    static void update(GLFWwindow* window) {
        WindowManager::window = window;

        stat.W = isPressed(GLFW_KEY_W) || isPressed(GLFW_KEY_UP);
        stat.S = isPressed(GLFW_KEY_S) || isPressed(GLFW_KEY_DOWN);
        stat.A = isPressed(GLFW_KEY_A) || isPressed(GLFW_KEY_LEFT);
        stat.D = isPressed(GLFW_KEY_D) || isPressed(GLFW_KEY_RIGHT);
        stat.F = isPressed(GLFW_KEY_F);

        stat.axisX = stat.D - stat.A;
        stat.axisY = stat.W - stat.S;

        glfwGetFramebufferSize(window, &sz.width, &sz.height);
    }

    static InputStatus& input() { return stat; }
    static SizeStatus&  size() { return sz; }

private:
    WindowManager() = delete;

    static GLFWwindow* window;

    static InputStatus stat;
    static SizeStatus  sz;

    static bool isPressed(int key) { return glfwGetKey(window, key) == GLFW_PRESS; }
};
