#version 330
layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

uniform mat4 p_mat;

vec2 rotate(vec2 v, float a) {
    float s = sin(a);
    float c = cos(a);
    mat2  m = mat2(c, s, -s, c);
    return m * v;
}

in VERT_OUT {
    vec2  scale;
    float rotation;
} g_in[];

void gen_vertex(vec2 rp) {
    vec4 pos    = vec4(gl_in[0].gl_Position.xy + rotate(rp, g_in[0].rotation), 1.0, 1.0);
    gl_Position = p_mat * pos;
    EmitVertex();
}

void main() {
    vec2 offset = g_in[0].scale / 2.0;
    gen_vertex(vec2(+offset.x, +offset.y));
    gen_vertex(vec2(-offset.x, +offset.y));
    gen_vertex(vec2(+offset.x, -offset.y));
    gen_vertex(vec2(-offset.x, -offset.y));
    EndPrimitive();
}
