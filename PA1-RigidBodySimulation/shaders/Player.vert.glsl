#version 330
layout(location = 0) in vec2 pos;

uniform mat4 mMat, pMat;

void main() {
    gl_Position = pMat * mMat * vec4(pos, 0.9, 1.0);
}
