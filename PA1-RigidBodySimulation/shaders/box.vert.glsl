#version 330
layout(location = 0) in vec2 position;
layout(location = 1) in vec2 scale;
layout(location = 2) in float rotation;

out VERT_OUT {
    vec2  scale;
    float rotation;
} v_out;

void main() {
    gl_Position    = vec4(position, 0.0, 1.0);
    v_out.scale    = scale;
    v_out.rotation = rotation;
}
