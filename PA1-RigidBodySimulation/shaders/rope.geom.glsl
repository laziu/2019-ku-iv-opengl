#version 330
layout(lines) in;
layout(triangle_strip, max_vertices = 6) out;

in VERT_OUT {
    vec2 normal;
} g_in[];

uniform mat4  p_mat;
uniform float line_width;

void gen_vertex(vec2 p) {
    gl_Position = p_mat * vec4(p, 0.6, 1.0);
    EmitVertex();
}

void main() {
    gen_vertex(gl_in[0].gl_Position.xy + g_in[0].normal * line_width / 2.0);
    gen_vertex(gl_in[0].gl_Position.xy - g_in[0].normal * line_width / 2.0);
    gen_vertex(gl_in[1].gl_Position.xy + g_in[0].normal * line_width / 2.0);
    gen_vertex(gl_in[1].gl_Position.xy - g_in[0].normal * line_width / 2.0);
    gen_vertex(gl_in[1].gl_Position.xy + g_in[1].normal * line_width / 2.0);
    gen_vertex(gl_in[1].gl_Position.xy - g_in[1].normal * line_width / 2.0);
    EndPrimitive();
}
