#version 330
out vec4 FragColor;

in vec2 TexCoord;

uniform sampler2D screen_texture;

void main() {
    float alpha = texture(screen_texture, TexCoord).a;
    if (alpha < .51)
        alpha = 0;
    FragColor = vec4(0, 0, 0, alpha * .2);
}
