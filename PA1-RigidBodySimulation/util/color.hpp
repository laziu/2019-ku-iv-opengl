#pragma once
#ifndef _UTIL_COLOR_
#define _UTIL_COLOR_
#include <stdexcept>
#include <string_view>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

namespace util {
constexpr uint8_t hex(char c) {
    if ('0' <= c && c <= '9')
        return c - '0';
    else if ('a' <= c && c <= 'f')
        return c - 'a' + 0xa;
    else if ('A' <= c && c <= 'F')
        return c - 'A' + 0xA;
    else
        throw std::logic_error("c must be valid hex character");
}

constexpr glm::vec3 rgb_code(const std::string_view& s) {
    if (s.size() != 6)
        throw std::logic_error("rgb code length must be 6");
    auto r = hex(s[0]) * 0x10 + hex(s[1]);
    auto g = hex(s[2]) * 0x10 + hex(s[3]);
    auto b = hex(s[4]) * 0x10 + hex(s[5]);
    return glm::vec3(r / 255.f, g / 255.f, b / 255.f);
}

constexpr glm::vec4 rgba_code(const std::string_view& s, float alpha) {
    return glm::vec4(rgb_code(s), alpha);
}
} // namespace util
#endif // _UTIL_COLOR_
