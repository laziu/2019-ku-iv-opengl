#include "program.hpp"
#include "shader.hpp"

namespace glo {
std::unordered_map<std::size_t, std::weak_ptr<program>> program::instances;
std::unordered_map<std::size_t, std::weak_ptr<shader>>  shader::instances;
std::unordered_map<std::size_t, shader::sptr_t>         shader::locks;
} // namespace glo
