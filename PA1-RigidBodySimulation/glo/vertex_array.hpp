#pragma once
#include "buffer.hpp"
#include <algorithm>
#include <array>
#include <memory>
#include <vector>
#include <glad/glad.h>

#ifdef DEBUG_GL_OBJECTS
#include <cstdio>
#define DEBUG_GL_LOG(...) printf(__VA_ARGS__)
#else
#define DEBUG_GL_LOG(...)
#endif

namespace glo {
struct vertex_array {
    using sptr_t = std::shared_ptr<vertex_array>;

    const GLuint id;

    template <GLsizei N>
    static std::array<sptr_t, N> create() {
        std::array<GLuint, N> ids;
        std::array<sptr_t, N> vas;

        glGenVertexArrays(N, ids.data());
        std::transform(ids.begin(), ids.end(), vas.begin(), [](auto id) {
            DEBUG_GL_LOG("[Debug] ++   vertex_array/%d\n", id);
            return sptr_t(new vertex_array(id));
        });
        return vas;
    }

    static sptr_t create() {
        GLuint id;
        glGenVertexArrays(1, &id);
        DEBUG_GL_LOG("[Debug] ++   vertex_array/%d\n", id);
        return std::shared_ptr<vertex_array>(new vertex_array(id));
    }

    ~vertex_array() {
        DEBUG_GL_LOG("[Debug]   -- vertex_array/%d\n", id);
        glDeleteVertexArrays(1, &id);
    }

    template <GLsizei N>
    auto hold(std::array<buffer::sptr_t, N> buffer_list) {
        buffers.insert(buffers.end(), buffer_list.begin(), buffer_list.end());
        return buffer_list;
    }

    auto hold(buffer::sptr_t buffer) {
        buffers.push_back(buffer);
        return buffer;
    }

    struct binded {
        binded&& drawArrays(GLenum mode, GLint first, GLsizei count) {
            glDrawArrays(mode, first, count);
            return std::move(*this);
        }

        binded&& drawElements(GLenum mode, GLsizei count, GLenum type, const void* indices) {
            glDrawElements(mode, count, type, indices);
            return std::move(*this);
        }

    private:
        const vertex_array& o;
        binded(vertex_array& o): o(o) { glBindVertexArray(o.id); }

        friend vertex_array;
    };

    binded bind() { return binded(*this); }

private:
    vertex_array(GLuint id): id(id) {}

    std::vector<buffer::sptr_t> buffers;
};
} // namespace glo

#undef DEBUG_GL_LOG
