#pragma once
#include <algorithm>
#include <array>
#include <functional>
#include <memory>
#include <glad/glad.h>

#ifdef DEBUG_GL_OBJECTS
#include <cstdio>
#define DEBUG_GL_LOG(...) printf(__VA_ARGS__)
#else
#define DEBUG_GL_LOG(...)
#endif

namespace glo {
struct buffer {
    using sptr_t = std::shared_ptr<buffer>;

    const GLuint id;

    template <GLsizei N>
    static std::array<sptr_t, N> create() {
        std::array<GLuint, N> ids;
        std::array<sptr_t, N> bfs;

        glGenBuffers(N, ids.data());
        std::transform(ids.begin(), ids.end(), bfs.begin(), [](auto id) {
            DEBUG_GL_LOG("[Debug] ++   buffer/%d\n", id);
            return sptr_t(new buffer(id));
        });
        return bfs;
    }

    static sptr_t create() {
        GLuint id;
        glGenBuffers(1, &id);
        DEBUG_GL_LOG("[Debug] ++   buffer/%d\n", id);
        return sptr_t(new buffer(id));
    }

    ~buffer() {
        DEBUG_GL_LOG("[Debug]   -- buffer/%d\n", id);
        glDeleteBuffers(1, &id);
    }

    struct binded {
        binded&& bufferData(GLsizeiptr size, const void* data, GLenum usage) {
            glBufferData(target, size, data, usage);
            return std::move(*this);
        }

        binded&& vertexAttribPointer(GLuint      index,
                                     GLint       size,
                                     GLenum      type,
                                     GLboolean   normalized,
                                     GLsizei     stride,
                                     const void* pointer) {
            glVertexAttribPointer(index, size, type, normalized, stride, pointer);
            glEnableVertexAttribArray(index);
            return std::move(*this);
        }

        template <typename T>
        binded&& mapBuffer(GLenum access, std::function<void(T*)> withBuffer) {
            if (T* buf = (T*)glMapBuffer(target, access)) {
                withBuffer(buf);
                glUnmapBuffer(target);
            }
            else
                fprintf(stderr, "buffer %d mapBuffer failed\n", o.id);
            return std::move(*this);
        }

    private:
        const buffer& o;
        const GLenum  target;
        binded(buffer& o, GLenum target): o(o), target(target) { glBindBuffer(target, o.id); }

        friend buffer;
    };

    binded bind(GLenum target) { return binded(*this, target); }

private:
    buffer(GLuint id): id(id) {}
};
} // namespace glo

#undef DEBUG_GL_LOG
